#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

#define MASK_WIDTH 5

#define TILE 16

#define COMMENT "Histogram_GPU"
#define RGB_COMPONENT_COLOR 255

typedef struct {
    unsigned char red, green, blue;
} PPMPixel;

typedef struct {
    int x, y;
    PPMPixel *data;
} PPMImage;

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}


static PPMImage *readPPM(const char *filename) {
    char buff[16];
    PPMImage *img;
    FILE *fp;
    int c, rgb_comp_color;
    fp = fopen(filename, "rb");
    if (!fp) {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    if (!fgets(buff, sizeof(buff), fp)) {
        perror(filename);
        exit(1);
    }

    if (buff[0] != 'P' || buff[1] != '6') {
        fprintf(stderr, "Invalid image format (must be 'P6')\n");
        exit(1);
    }

    img = (PPMImage *) malloc(sizeof(PPMImage));
    if (!img) {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    c = getc(fp);
    while (c == '#') {
        while (getc(fp) != '\n')
            ;
        c = getc(fp);
    }

    ungetc(c, fp);
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {
        fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
        exit(1);
    }

    if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
        fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
                filename);
        exit(1);
    }

    if (rgb_comp_color != RGB_COMPONENT_COLOR) {
        fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
        exit(1);
    }

    while (fgetc(fp) != '\n')
        ;
    img->data = (PPMPixel*) malloc(img->x * img->y * sizeof(PPMPixel));

    if (!img) {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    if (fread(img->data, 3 * img->x, img->y, fp) != img->y) {
        fprintf(stderr, "Error loading image '%s'\n", filename);
        exit(1);
    }

    fclose(fp);
    return img;
}

void writePPM(PPMImage *img) {

    fprintf(stdout, "P6\n");
    fprintf(stdout, "# %s\n", COMMENT);
    fprintf(stdout, "%d %d\n", img->x, img->y);
    fprintf(stdout, "%d\n", RGB_COMPONENT_COLOR);

    fwrite(img->data, 3 * img->x, img->y, stdout);
    fclose(stdout);
}

__global__ void kernel(PPMPixel *imgIn, PPMPixel *imgOut,  int *imgX, int *imgY){

  int border = (int)((MASK_WIDTH - 1) / 2);
  int dimension = TILE + (MASK_WIDTH - 1);
  __shared__ PPMPixel shared[TILE + (MASK_WIDTH - 1)][TILE + (MASK_WIDTH - 1)];//slide 11 do arquivo Introduction to CUDA 2.pdf

  int k, index, x, y, sharedLinha, sharedCol;

  int copyToSharedMemPerThread = ceil((float)(dimension * dimension) / (blockDim.x * blockDim.y));//qntd de elementos que cada tread deve copiar para a shared memory

  int blockIniLinha = blockIdx.y * blockDim.y;//linha de inicio do bloco
	int blockIniCol = blockIdx.x * blockDim.x;//coluna de inicio do bloco

  //copiar para a shared memory
  for(k = 0; k < copyToSharedMemPerThread; k++){
    index = (threadIdx.y * blockDim.x) + threadIdx.x + (k * blockDim.x * blockDim.y);//slide 11
    x = (int)(index / dimension);//calculo do limite de x. Depois é necessário verificar se os limites estão dentro do intervalo da shared memory
    y = index % dimension;//calculo do limite de y. Depois é necessário verificar se os limites estão dentro do intervalo da shared memory

    //verificar se os limites x e y estão dentro do intervalo da shared memory
    if(x < dimension && y < dimension){

      //Calculo index no vetor de entrada. "Equivalente" ao gindex e lindex do slide 11
      sharedLinha = blockIniLinha + x - border;
      sharedCol = blockIniCol + y - border;

      //verificar se os indices  são validos no input vector. Se for, só copia os valores, senão coloca todos como zero
      if(sharedLinha >= 0 && sharedCol >= 0 && sharedCol < *imgX && sharedLinha < *imgY)
        shared[x][y] = imgIn[sharedLinha * *imgX + sharedCol];
      else
        shared[x][y].red = shared[x][y].green = shared[x][y].blue = 0;
    }//fim if intervalo shared memory
  }//fim for k

  __syncthreads();//Synchronize (ensure all the data is available) - slide 11

	int linhaOutput = blockIdx.y * blockDim.y + threadIdx.y;//linha do vetor de output
	int colOutput = blockIdx.x * blockDim.x + threadIdx.x;//coluna do vetor de output

  if((linhaOutput * colOutput) < (*imgX * *imgY)){//verifica a linha e coluna são "validos"
    int i, j, total_red, total_blue, total_green;
    total_red = total_blue = total_green = 0;

    //igual da versão serial, mas usando a variavel shared e os limites são calculados um pouco diferente, pois já foram acertados acima (cada bloco calculo o seu de forma a não precisar calcular aqui embaixo de novo)
    for(i = threadIdx.y; i < (threadIdx.y + MASK_WIDTH); i++){
      for(j = threadIdx.x; j < (threadIdx.x + MASK_WIDTH); j++){
        total_red += shared[i][j].red;
        total_blue += shared[i][j].blue;
        total_green += shared[i][j].green;
      }
    }
    //igual da versão serial
    imgOut[linhaOutput * *imgX + colOutput].red = total_red / (MASK_WIDTH * MASK_WIDTH);
    imgOut[linhaOutput * *imgX + colOutput].blue = total_blue / (MASK_WIDTH * MASK_WIDTH);
    imgOut[linhaOutput * *imgX + colOutput].green = total_green / (MASK_WIDTH * MASK_WIDTH);
  }//fim if

}

void Smoothing_CPU_Serial(PPMImage *image, PPMImage *image_copy) {
    int i, j, y, x;
    int total_red, total_blue, total_green;

    for (i = 0; i < image->y; i++) {
        for (j = 0; j < image->x; j++) {
            total_red = total_blue = total_green = 0;
            for (y = i - ((MASK_WIDTH-1)/2); y <= (i + ((MASK_WIDTH-1)/2)); y++) {
                for (x = j - ((MASK_WIDTH-1)/2); x <= (j + ((MASK_WIDTH-1)/2)); x++) {
                    if (x >= 0 && y >= 0 && y < image->y && x < image->x) {
                        total_red += image_copy->data[(y * image->x) + x].red;
                        total_blue += image_copy->data[(y * image->x) + x].blue;
                        total_green += image_copy->data[(y * image->x) + x].green;
                    } //if
                } //for z
            } //for y
            image->data[(i * image->x) + j].red = total_red / (MASK_WIDTH*MASK_WIDTH);
            image->data[(i * image->x) + j].blue = total_blue / (MASK_WIDTH*MASK_WIDTH);
            image->data[(i * image->x) + j].green = total_green / (MASK_WIDTH*MASK_WIDTH);
        }
    }
}

int main(int argc, char *argv[]) {

    if( argc != 2 ) {
        printf("Too many or no one arguments supplied.\n");
    }

    double t_start, t_end;
    int i;
    char *filename = argv[1]; //Recebendo o arquivo!;

    PPMImage *image = readPPM(filename);
    PPMImage *image_output = readPPM(filename);

/****************************ALTERAÇÕES PARA A GPU*************************/

    //variaveis para usar na GPU
    PPMPixel *imageIn_d, *imageOut_d;
    int *imageX_d, *imageY_d;
    int xy = image->y * image->x;

    cudaMalloc((void**)&imageIn_d, xy * sizeof(PPMPixel));
    cudaMalloc((void**)&imageOut_d, xy * sizeof(PPMPixel));
    cudaMalloc((void**)&imageX_d, sizeof(int));
    cudaMalloc((void**)&imageY_d, sizeof(int));

    cudaMemcpy(imageIn_d, image->data, xy * sizeof(PPMPixel), cudaMemcpyHostToDevice);
    cudaMemcpy(imageX_d, &(image->x), sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(imageY_d, &(image->y), sizeof(int), cudaMemcpyHostToDevice);

    dim3 GRID(TILE, TILE);//threads por bloco
    dim3 BLOCKS(ceil((float)(image->x) / TILE), ceil((float)(image->y) / TILE), 1);

    //Timing Kernel Execution
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    float milliseconds = 0;

    cudaEventRecord(start);//Timing Kernel Execution

    kernel<<<BLOCKS, GRID>>>(imageIn_d, imageOut_d, imageX_d, imageY_d);

    cudaEventRecord(stop);//Timing Kernel Execution
  	//Timing Kernel Execution
  	cudaEventSynchronize(stop);
  	cudaEventElapsedTime(&milliseconds, start, stop);
  	printf("\n");
  	//fprintf(stdout, "Tempo Kernel: %0.6lfms\n\n", milliseconds);

    //t_start = rtclock();
    //Smoothing_CPU_Serial(image_output, image);
    //t_end = rtclock();

    cudaMemcpy(image_output->data, imageOut_d, xy * sizeof(PPMPixel), cudaMemcpyDeviceToHost);

    writePPM(image_output);

    cudaFree(imageIn_d);
    cudaFree(imageOut_d);
    cudaFree(imageX_d);
    cudaFree(imageY_d);

    //fprintf(stdout, "\n%0.6lfs\n", t_end - t_start);
    free(image);
    free(image_output);
}
