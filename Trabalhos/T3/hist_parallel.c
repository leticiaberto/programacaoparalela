#include <stdio.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <pthread.h>

//variaveis globais
double h, *val, max, min;
int n, nval, *vet, size;


/* funcao que calcula o minimo valor em um vetor */
double min_val(double * vet,int nval) {
	int i;
	double min;

	min = FLT_MAX;

	for(i=0;i<nval;i++) {
		if(vet[i] < min)
			min =  vet[i];
	}

	return min;
}

/* funcao que calcula o maximo valor em um vetor */
double max_val(double * vet, int nval) {
	int i;
	double max;

	max = FLT_MIN;

	for(i=0;i<nval;i++) {
		if(vet[i] > max)
			max =  vet[i];
	}

	return max;
}

/* conta quantos valores no vetor estao entre o minimo e o maximo passados como parametros */
void * count(void* rank) {
	long my_rank = (long) rank;
	int qntdBin = n/size;//qnts bins cada thread vai usar
	int firstBin = my_rank * qntdBin; //calcula o primeiro bin da thread através do seu rank
	int lastBin = firstBin + qntdBin; //calcula o ultimo bin da thread

	if (my_rank == size - 1)//caso o numero de bins nao seja divisivel, aloca o q restou para a ultima thread
		lastBin = n;
	int i, j, count;
	double min_t, max_t;

	for(j = firstBin; j < lastBin; j++) { //percorre só os bins referentes a thread
		count = 0;
		min_t = min + j*h; //valor minimo do bin (extremo esquerdo)
		max_t = min + (j+1)*h; //valor maximo do bin (extremo direito)
		for(i = 0; i < nval; i++) {
			if( (val[i] <= max_t && val[i] > min_t) || (j == 0 && val[i] <= min_t) ) {
				count++;
			}
		}

		vet[j] = count;//nao tem problema de corrida pq cada thread acessa um indice de vet diferente ao mesmo tempo (garantido através do firstBin e lastBin)
	}

	return NULL;//vet é global, logo é alterado na função, não precisando retorna-lo
}

int main(int argc, char * argv[]) {
	int i;
	long unsigned int duracao;
	struct timeval start, end;

	//para usar threads
	long thread;
	pthread_t* thread_handles;

	scanf("%d",&size);

	/* entrada do numero de dados */
	scanf("%d",&nval);
	/* numero de barras do histograma a serem calculadas */
	scanf("%d",&n);

	/* vetor com os dados */
	val = (double *)malloc(nval*sizeof(double));
	vet = (int *)malloc(n*sizeof(int));

	/* entrada dos dados */
	for(i=0;i<nval;i++) {
		scanf("%lf",&val[i]);
	}

	/* calcula o minimo e o maximo valores inteiros */
	min = floor(min_val(val,nval));
	max = ceil(max_val(val,nval));

	/* calcula o tamanho de cada barra */
	h = (max - min)/n;

	thread_handles = malloc(size*sizeof(pthread_t));

	gettimeofday(&start, NULL);

	for(thread = 0; thread < size; thread++)
		pthread_create(&thread_handles[thread], NULL, count, (void*) thread);

	for(thread = 0; thread < size; thread++)
		pthread_join(thread_handles[thread], NULL);

	free (thread_handles);

	/* chama a funcao */
	//vet = count(min, max, vet, n, h, val, nval);

	gettimeofday(&end, NULL);

	duracao = ((end.tv_sec * 1000000 + end.tv_usec) - \
	(start.tv_sec * 1000000 + start.tv_usec));

	printf("%.2lf",min);
	for(i=1;i<=n;i++) {
		printf(" %.2lf",min + h*i);
	}
	printf("\n");

	/* imprime o histograma calculado */
	printf("%d",vet[0]);
	for(i=1;i<n;i++) {
		printf(" %d",vet[i]);
	}
	printf("\n");


	/* imprime o tempo de duracao do calculo */
	printf("%lu\n",duracao);

	free(vet);
	free(val);

	return 0;
}

/*-------------------------Tabela de SpeedUp e Eficiência-------------------------*/
/*
						Threads    1			2				4				8				16
-----------------------------------------------------------------------------------
arq1.in		SpeedUp    	1			9.41			2.88		2.93		4.56
					Eficiência  1			2.35			0.72		0.73		1.14
-----------------------------------------------------------------------------------
arq2.in		SpeedUp    	1			2.03		2.44		2.60		2.56
					Eficiência  1			0.50		0.61		0.65		0.64
-----------------------------------------------------------------------------------
arq3.in		SpeedUp    	1			1.91		2.58		2.71		2.93
					Eficiência  1			0.47		0.64		0.67		0.73
-----------------------------------------------------------------------------------

Como pode-se observar, o SpeedUp do arq1.in decresce com o aumento do numero de threads entre 4 e 8,
mas sobe novamente quando thread = 16. Mesmo com esse acrescimo no final, não supera o SpeedUp com apenas 2 threads.
Isso pode ser explicado devido ao fato de o gerenciamento de threads para entradas pequenas não supera o valor ganho com o seu uso
Além disso, a eficiência tambpem decresce com o aumento de threads, o que nos leva a conclusão que após determinado
numero de threads é melhor não aumenta-las (nesse caso 4)

O SpeedUp do arq2.in aumenta conforme aumentamos o numero de threads. Para threads = 16 sofre um pequeno decrescimo,
mas isso pode ser justificado ao tempo necessário para gerenciar essa quantidade a mais. Como essa diferença é muito pequena,
compensa usar o maior numero de threads
A eficiência também aumentou para o aumento de threads, porém não foi tão significativa quanto 2 threads no arq1.in

O SpeedUp do arq3.in aumenta com o numero de threads e não sofre decrescimos. A eficiência também melhora
Como o arquivo é muito grande, a paralelização das operações de mais custo (função count) compensa o gasto com gerenciamento de threads
*/

/*-------------------------------Gprof---------------------------------------------*/
/*
arq1.in
Each sample counts as 0.01 seconds.
 no time accumulated

  %   cumulative   self              self     total
 time   seconds   seconds    calls  Ts/call  Ts/call  name
  0.00      0.00     0.00        1     0.00     0.00  count
  0.00      0.00     0.00        1     0.00     0.00  max_val
  0.00      0.00     0.00        1     0.00     0.00  min_val


arq2.in
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  ms/call  ms/call  name
100.46      0.07     0.07        1    70.32    70.32  count
  0.00      0.07     0.00        1     0.00     0.00  max_val
  0.00      0.07     0.00        1     0.00     0.00  min_val

arq3.in
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  ms/call  ms/call  name
100.46      0.40     0.40        1   401.83   401.83  count
  0.00      0.40     0.00        1     0.00     0.00  max_val
  0.00      0.40     0.00        1     0.00     0.00  min_val

*/
