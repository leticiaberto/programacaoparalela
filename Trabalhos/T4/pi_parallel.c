#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <semaphore.h>

unsigned int n;
long long unsigned int in = 0; //numero de pontos dentro do circulo
int size;
sem_t somaGlobal;

void * monte_carlo_pi(void* rank) {
	long my_rank = (long) rank;
	long long unsigned int my_in = 0, i;
	double x, y, d;
	unsigned int estFunc;//ponteiro para o estado da função em cada thread
	long long int qntdPoints = n/size;//qntd de pontos por thread
	long long int firstPoint = my_rank * qntdPoints; //primeiro ponto da thread
	long long int lastPoint = firstPoint + qntdPoints; //ultimo ponto da thread

	//gera os pontos, ve se esta no circulo e contabiliza
	for (i = firstPoint; i < lastPoint; i++) {
		x = ((rand_r(&estFunc) % 1000000)/500000.0)-1;//gera numeros aleatorios entre -1 e 1 para a coordenada x
		y = ((rand_r(&estFunc) % 1000000)/500000.0)-1;//gera numeros aleatorios entre -1 e 1 para a coordenada y
		d = ((x*x) + (y*y));//calcula onde vai estar o ponto x e y - teorema de Pitagoras, para depois calcular se o ponto esta dentro ou fora do circulo (não temos a coordenada do circulo, só o raio)
		if (d <= 1) my_in+=1;//se caiu dentro do circulo adiciona ao numero de ponrtos dentro do circulo (variavel local)
	}

	//adicionar qntd de pontos calculados localmente (my_in) na variavel global in (numero de pontos dentro do circulo) utilizando semaforo para proteger a var global
	sem_wait(&somaGlobal);
	in+=my_in;
	sem_post(&somaGlobal);
	return NULL;
}

int main(void) {
	double pi;
	long unsigned int duracao;
	struct timeval start, end;

	//para usar threads
	long thread;
	pthread_t* thread_handles;

	//size = numero de threads
	//n = numero de total de pontos que serão gerados
	scanf("%d %u",&size, &n);

	srand (time(NULL));

	thread_handles = malloc(size*sizeof(pthread_t));

	sem_init(&somaGlobal, 0, 1);

	gettimeofday(&start, NULL);

	for(thread = 0; thread < size; thread++)
		pthread_create(&thread_handles[thread], NULL, monte_carlo_pi, (void*) thread);

	for(thread = 0; thread < size; thread++)
		pthread_join(thread_handles[thread], NULL);

	free (thread_handles);

	//in = monte_carlo_pi(n);
	gettimeofday(&end, NULL);

	duracao = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));

	pi = 4*in/((double)n);
	printf("%lf\n%lu\n",pi,duracao);

	return 0;
}
