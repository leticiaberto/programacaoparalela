; ModuleID = 'CMakeFiles/cutcp.dir/readatom-inst.c'
source_filename = "CMakeFiles/cutcp.dir/readatom-inst.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.Atoms_t = type { %struct.Atom_t*, i32 }
%struct.Atom_t = type { float, float, float, float }
%struct.Vec3_t = type { float, float, float }

@.str = private unnamed_addr constant [2 x i8] c"r\00", align 1
@stderr = external global %struct._IO_FILE*, align 8
@.str.1 = private unnamed_addr constant [34 x i8] c"can't open file \22%s\22 for reading\0A\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c"can't allocate memory\0A\00", align 1
@.str.3 = private unnamed_addr constant [7 x i8] c"ATOM  \00", align 1
@.str.4 = private unnamed_addr constant [7 x i8] c"HETATM\00", align 1
@.str.5 = private unnamed_addr constant [28 x i8] c"can't allocate more memory\0A\00", align 1
@.str.6 = private unnamed_addr constant [32 x i8] c"%*s %*d %*s %*s %*d %f %f %f %f\00", align 1
@.str.7 = private unnamed_addr constant [46 x i8] c"atom record %d does not have expected format\0A\00", align 1
@.str.8 = private unnamed_addr constant [18 x i8] c"did not find EOF\0A\00", align 1
@.str.9 = private unnamed_addr constant [18 x i8] c"can't close file\0A\00", align 1
@0 = internal constant [37 x i8] c"CMakeFiles/cutcp.dir/readatom-inst.c\00"

; Function Attrs: nounwind uwtable
define %struct.Atoms_t* @read_atom_file(i8*) #0 !dbg !25 {
  %2 = alloca %struct.Atoms_t*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca %struct._IO_FILE*, align 8
  %5 = alloca [96 x i8], align 16
  %6 = alloca %struct.Atom_t*, align 8
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca i8*, align 8
  %10 = alloca %struct.Atoms_t*, align 8
  store i8* %0, i8** %3, align 8
  call void @llvm.dbg.declare(metadata i8** %3, metadata !31, metadata !32), !dbg !33
  call void @llvm.dbg.declare(metadata %struct._IO_FILE** %4, metadata !34, metadata !32), !dbg !95
  call void @llvm.dbg.declare(metadata [96 x i8]* %5, metadata !96, metadata !32), !dbg !100
  call void @llvm.dbg.declare(metadata %struct.Atom_t** %6, metadata !101, metadata !32), !dbg !102
  call void @llvm.dbg.declare(metadata i32* %7, metadata !103, metadata !32), !dbg !104
  store i32 20, i32* %7, align 4, !dbg !104
  call void @llvm.dbg.declare(metadata i32* %8, metadata !105, metadata !32), !dbg !106
  store i32 0, i32* %8, align 4, !dbg !106
  %11 = load i8*, i8** %3, align 8, !dbg !107
  %12 = call %struct._IO_FILE* @fopen(i8* %11, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0)), !dbg !108
  store %struct._IO_FILE* %12, %struct._IO_FILE** %4, align 8, !dbg !109
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** %4, align 8, !dbg !110
  %14 = icmp eq %struct._IO_FILE* null, %13, !dbg !112
  br i1 %14, label %15, label %19, !dbg !113

; <label>:15:                                     ; preds = %1
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !114
  %17 = load i8*, i8** %3, align 8, !dbg !116
  %18 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.1, i32 0, i32 0), i8* %17), !dbg !117
  store %struct.Atoms_t* null, %struct.Atoms_t** %2, align 8, !dbg !118
  br label %129, !dbg !118

; <label>:19:                                     ; preds = %1
  %20 = load i32, i32* %7, align 4, !dbg !119
  %21 = sext i32 %20 to i64, !dbg !119
  %22 = mul i64 %21, 16, !dbg !120
  %23 = call noalias i8* @malloc(i64 %22) #8, !dbg !121
  %24 = bitcast i8* %23 to %struct.Atom_t*, !dbg !122
  store %struct.Atom_t* %24, %struct.Atom_t** %6, align 8, !dbg !123
  %25 = load %struct.Atom_t*, %struct.Atom_t** %6, align 8, !dbg !124
  %26 = icmp eq %struct.Atom_t* null, %25, !dbg !126
  br i1 %26, label %27, label %30, !dbg !127

; <label>:27:                                     ; preds = %19
  %28 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !128
  %29 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %28, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0)), !dbg !130
  store %struct.Atoms_t* null, %struct.Atoms_t** %2, align 8, !dbg !131
  br label %129, !dbg !131

; <label>:30:                                     ; preds = %19
  br label %31, !dbg !132

; <label>:31:                                     ; preds = %96, %44, %30
  %32 = getelementptr inbounds [96 x i8], [96 x i8]* %5, i32 0, i32 0, !dbg !133
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** %4, align 8, !dbg !135
  %34 = call i8* @fgets(i8* %32, i32 96, %struct._IO_FILE* %33), !dbg !136
  %35 = icmp ne i8* %34, null, !dbg !137
  br i1 %35, label %36, label %99, !dbg !138

; <label>:36:                                     ; preds = %31
  %37 = getelementptr inbounds [96 x i8], [96 x i8]* %5, i32 0, i32 0, !dbg !139
  %38 = call i32 @strncmp(i8* %37, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i64 6) #9, !dbg !142
  %39 = icmp ne i32 %38, 0, !dbg !143
  br i1 %39, label %40, label %45, !dbg !144

; <label>:40:                                     ; preds = %36
  %41 = getelementptr inbounds [96 x i8], [96 x i8]* %5, i32 0, i32 0, !dbg !145
  %42 = call i32 @strncmp(i8* %41, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.4, i32 0, i32 0), i64 6) #9, !dbg !147
  %43 = icmp ne i32 %42, 0, !dbg !148
  br i1 %43, label %44, label %45, !dbg !149

; <label>:44:                                     ; preds = %40
  br label %31, !dbg !150, !llvm.loop !152

; <label>:45:                                     ; preds = %40, %36
  %46 = load i32, i32* %8, align 4, !dbg !153
  %47 = load i32, i32* %7, align 4, !dbg !155
  %48 = icmp eq i32 %46, %47, !dbg !156
  br i1 %48, label %49, label %67, !dbg !157

; <label>:49:                                     ; preds = %45
  call void @llvm.dbg.declare(metadata i8** %9, metadata !158, metadata !32), !dbg !160
  %50 = load %struct.Atom_t*, %struct.Atom_t** %6, align 8, !dbg !161
  %51 = bitcast %struct.Atom_t* %50 to i8*, !dbg !161
  %52 = load i32, i32* %7, align 4, !dbg !162
  %53 = mul nsw i32 2, %52, !dbg !163
  %54 = sext i32 %53 to i64, !dbg !164
  %55 = mul i64 %54, 16, !dbg !165
  %56 = call i8* @realloc(i8* %51, i64 %55) #8, !dbg !166
  store i8* %56, i8** %9, align 8, !dbg !160
  %57 = load i8*, i8** %9, align 8, !dbg !167
  %58 = icmp eq i8* null, %57, !dbg !169
  br i1 %58, label %59, label %62, !dbg !170

; <label>:59:                                     ; preds = %49
  %60 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !171
  %61 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %60, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.5, i32 0, i32 0)), !dbg !173
  store %struct.Atoms_t* null, %struct.Atoms_t** %2, align 8, !dbg !174
  br label %129, !dbg !174

; <label>:62:                                     ; preds = %49
  %63 = load i8*, i8** %9, align 8, !dbg !175
  %64 = bitcast i8* %63 to %struct.Atom_t*, !dbg !176
  store %struct.Atom_t* %64, %struct.Atom_t** %6, align 8, !dbg !177
  %65 = load i32, i32* %7, align 4, !dbg !178
  %66 = mul nsw i32 %65, 2, !dbg !178
  store i32 %66, i32* %7, align 4, !dbg !178
  br label %67, !dbg !179

; <label>:67:                                     ; preds = %62, %45
  %68 = getelementptr inbounds [96 x i8], [96 x i8]* %5, i32 0, i32 0, !dbg !180
  %69 = load i32, i32* %8, align 4, !dbg !182
  %70 = sext i32 %69 to i64, !dbg !183
  %71 = load %struct.Atom_t*, %struct.Atom_t** %6, align 8, !dbg !183
  %72 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %71, i64 %70, !dbg !183
  %73 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %72, i32 0, i32 0, !dbg !184
  %74 = load i32, i32* %8, align 4, !dbg !185
  %75 = sext i32 %74 to i64, !dbg !186
  %76 = load %struct.Atom_t*, %struct.Atom_t** %6, align 8, !dbg !186
  %77 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %76, i64 %75, !dbg !186
  %78 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %77, i32 0, i32 1, !dbg !187
  %79 = load i32, i32* %8, align 4, !dbg !188
  %80 = sext i32 %79 to i64, !dbg !189
  %81 = load %struct.Atom_t*, %struct.Atom_t** %6, align 8, !dbg !189
  %82 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %81, i64 %80, !dbg !189
  %83 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %82, i32 0, i32 2, !dbg !190
  %84 = load i32, i32* %8, align 4, !dbg !191
  %85 = sext i32 %84 to i64, !dbg !192
  %86 = load %struct.Atom_t*, %struct.Atom_t** %6, align 8, !dbg !192
  %87 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %86, i64 %85, !dbg !192
  %88 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %87, i32 0, i32 3, !dbg !193
  %89 = call i32 (i8*, i8*, ...) @__isoc99_sscanf(i8* %68, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.6, i32 0, i32 0), float* %73, float* %78, float* %83, float* %88) #8, !dbg !194
  %90 = icmp ne i32 %89, 4, !dbg !195
  br i1 %90, label %91, label %96, !dbg !196

; <label>:91:                                     ; preds = %67
  %92 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !197
  %93 = load i32, i32* %8, align 4, !dbg !199
  %94 = add nsw i32 %93, 1, !dbg !200
  %95 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %92, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.7, i32 0, i32 0), i32 %94), !dbg !201
  store %struct.Atoms_t* null, %struct.Atoms_t** %2, align 8, !dbg !202
  br label %129, !dbg !202

; <label>:96:                                     ; preds = %67
  %97 = load i32, i32* %8, align 4, !dbg !203
  %98 = add nsw i32 %97, 1, !dbg !203
  store i32 %98, i32* %8, align 4, !dbg !203
  br label %31, !dbg !204, !llvm.loop !152

; <label>:99:                                     ; preds = %31
  %100 = load %struct._IO_FILE*, %struct._IO_FILE** %4, align 8, !dbg !206
  %101 = call i32 @feof(%struct._IO_FILE* %100) #8, !dbg !208
  %102 = icmp ne i32 %101, 0, !dbg !208
  br i1 %102, label %106, label %103, !dbg !209

; <label>:103:                                    ; preds = %99
  %104 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !210
  %105 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %104, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.8, i32 0, i32 0)), !dbg !212
  store %struct.Atoms_t* null, %struct.Atoms_t** %2, align 8, !dbg !213
  br label %129, !dbg !213

; <label>:106:                                    ; preds = %99
  %107 = load %struct._IO_FILE*, %struct._IO_FILE** %4, align 8, !dbg !214
  %108 = call i32 @fclose(%struct._IO_FILE* %107), !dbg !216
  %109 = icmp ne i32 %108, 0, !dbg !216
  br i1 %109, label %110, label %113, !dbg !217

; <label>:110:                                    ; preds = %106
  %111 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !218
  %112 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %111, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.9, i32 0, i32 0)), !dbg !220
  store %struct.Atoms_t* null, %struct.Atoms_t** %2, align 8, !dbg !221
  br label %129, !dbg !221

; <label>:113:                                    ; preds = %106
  call void @llvm.dbg.declare(metadata %struct.Atoms_t** %10, metadata !222, metadata !32), !dbg !224
  %114 = call noalias i8* @malloc(i64 16) #8, !dbg !225
  %115 = bitcast i8* %114 to %struct.Atoms_t*, !dbg !226
  store %struct.Atoms_t* %115, %struct.Atoms_t** %10, align 8, !dbg !224
  %116 = load %struct.Atoms_t*, %struct.Atoms_t** %10, align 8, !dbg !227
  %117 = icmp eq %struct.Atoms_t* null, %116, !dbg !229
  br i1 %117, label %118, label %121, !dbg !230

; <label>:118:                                    ; preds = %113
  %119 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !231
  %120 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %119, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0)), !dbg !233
  store %struct.Atoms_t* null, %struct.Atoms_t** %2, align 8, !dbg !234
  br label %129, !dbg !234

; <label>:121:                                    ; preds = %113
  %122 = load i32, i32* %8, align 4, !dbg !235
  %123 = load %struct.Atoms_t*, %struct.Atoms_t** %10, align 8, !dbg !236
  %124 = getelementptr inbounds %struct.Atoms_t, %struct.Atoms_t* %123, i32 0, i32 1, !dbg !237
  store i32 %122, i32* %124, align 8, !dbg !238
  %125 = load %struct.Atom_t*, %struct.Atom_t** %6, align 8, !dbg !239
  %126 = load %struct.Atoms_t*, %struct.Atoms_t** %10, align 8, !dbg !240
  %127 = getelementptr inbounds %struct.Atoms_t, %struct.Atoms_t* %126, i32 0, i32 0, !dbg !241
  store %struct.Atom_t* %125, %struct.Atom_t** %127, align 8, !dbg !242
  %128 = load %struct.Atoms_t*, %struct.Atoms_t** %10, align 8, !dbg !243
  store %struct.Atoms_t* %128, %struct.Atoms_t** %2, align 8, !dbg !244
  br label %129, !dbg !244

; <label>:129:                                    ; preds = %121, %118, %110, %103, %91, %59, %27, %15
  %130 = load %struct.Atoms_t*, %struct.Atoms_t** %2, align 8, !dbg !245
  ret %struct.Atoms_t* %130, !dbg !245
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

declare %struct._IO_FILE* @fopen(i8*, i8*) #2

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #3

declare i8* @fgets(i8*, i32, %struct._IO_FILE*) #2

; Function Attrs: nounwind readonly
declare i32 @strncmp(i8*, i8*, i64) #4

; Function Attrs: nounwind
declare i8* @realloc(i8*, i64) #3

; Function Attrs: nounwind
declare i32 @__isoc99_sscanf(i8*, i8*, ...) #3

; Function Attrs: nounwind
declare i32 @feof(%struct._IO_FILE*) #3

declare i32 @fclose(%struct._IO_FILE*) #2

; Function Attrs: nounwind uwtable
define void @free_atom(%struct.Atoms_t*) #0 !dbg !246 {
  %2 = alloca %struct.Atoms_t*, align 8
  store %struct.Atoms_t* %0, %struct.Atoms_t** %2, align 8
  call void @llvm.dbg.declare(metadata %struct.Atoms_t** %2, metadata !249, metadata !32), !dbg !250
  %3 = load %struct.Atoms_t*, %struct.Atoms_t** %2, align 8, !dbg !251
  %4 = icmp ne %struct.Atoms_t* %3, null, !dbg !251
  br i1 %4, label %5, label %12, !dbg !253

; <label>:5:                                      ; preds = %1
  %6 = load %struct.Atoms_t*, %struct.Atoms_t** %2, align 8, !dbg !254
  %7 = getelementptr inbounds %struct.Atoms_t, %struct.Atoms_t* %6, i32 0, i32 0, !dbg !256
  %8 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !256
  %9 = bitcast %struct.Atom_t* %8 to i8*, !dbg !254
  call void @free(i8* %9) #8, !dbg !257
  %10 = load %struct.Atoms_t*, %struct.Atoms_t** %2, align 8, !dbg !258
  %11 = bitcast %struct.Atoms_t* %10 to i8*, !dbg !258
  call void @free(i8* %11) #8, !dbg !259
  br label %12, !dbg !260

; <label>:12:                                     ; preds = %5, %1
  ret void, !dbg !261
}

; Function Attrs: nounwind
declare void @free(i8*) #3

; Function Attrs: nounwind uwtable
define void @get_atom_extent(%struct.Vec3_t*, %struct.Vec3_t*, %struct.Atoms_t*) #0 !dbg !262 {
  %4 = alloca %struct.Vec3_t*, align 8
  %5 = alloca %struct.Vec3_t*, align 8
  %6 = alloca %struct.Atoms_t*, align 8
  %7 = alloca %struct.Atom_t*, align 8
  %8 = alloca i32, align 4
  %9 = alloca %struct.Vec3_t, align 4
  %10 = alloca %struct.Vec3_t, align 4
  %11 = alloca i32, align 4
  store %struct.Vec3_t* %0, %struct.Vec3_t** %4, align 8
  call void @llvm.dbg.declare(metadata %struct.Vec3_t** %4, metadata !272, metadata !32), !dbg !273
  store %struct.Vec3_t* %1, %struct.Vec3_t** %5, align 8
  call void @llvm.dbg.declare(metadata %struct.Vec3_t** %5, metadata !274, metadata !32), !dbg !275
  store %struct.Atoms_t* %2, %struct.Atoms_t** %6, align 8
  call void @llvm.dbg.declare(metadata %struct.Atoms_t** %6, metadata !276, metadata !32), !dbg !277
  call void @llvm.dbg.declare(metadata %struct.Atom_t** %7, metadata !278, metadata !32), !dbg !279
  %12 = load %struct.Atoms_t*, %struct.Atoms_t** %6, align 8, !dbg !280
  %13 = getelementptr inbounds %struct.Atoms_t, %struct.Atoms_t* %12, i32 0, i32 0, !dbg !281
  %14 = load %struct.Atom_t*, %struct.Atom_t** %13, align 8, !dbg !281
  store %struct.Atom_t* %14, %struct.Atom_t** %7, align 8, !dbg !279
  call void @llvm.dbg.declare(metadata i32* %8, metadata !282, metadata !32), !dbg !283
  %15 = load %struct.Atoms_t*, %struct.Atoms_t** %6, align 8, !dbg !284
  %16 = getelementptr inbounds %struct.Atoms_t, %struct.Atoms_t* %15, i32 0, i32 1, !dbg !285
  %17 = load i32, i32* %16, align 8, !dbg !285
  store i32 %17, i32* %8, align 4, !dbg !283
  call void @llvm.dbg.declare(metadata %struct.Vec3_t* %9, metadata !286, metadata !32), !dbg !287
  call void @llvm.dbg.declare(metadata %struct.Vec3_t* %10, metadata !288, metadata !32), !dbg !289
  call void @llvm.dbg.declare(metadata i32* %11, metadata !290, metadata !32), !dbg !291
  %18 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !292
  %19 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %18, i64 0, !dbg !292
  %20 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %19, i32 0, i32 0, !dbg !293
  %21 = load float, float* %20, align 4, !dbg !293
  %22 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 0, !dbg !294
  store float %21, float* %22, align 4, !dbg !295
  %23 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 0, !dbg !296
  store float %21, float* %23, align 4, !dbg !297
  %24 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !298
  %25 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %24, i64 0, !dbg !298
  %26 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %25, i32 0, i32 1, !dbg !299
  %27 = load float, float* %26, align 4, !dbg !299
  %28 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 1, !dbg !300
  store float %27, float* %28, align 4, !dbg !301
  %29 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 1, !dbg !302
  store float %27, float* %29, align 4, !dbg !303
  %30 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !304
  %31 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %30, i64 0, !dbg !304
  %32 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %31, i32 0, i32 2, !dbg !305
  %33 = load float, float* %32, align 4, !dbg !305
  %34 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 2, !dbg !306
  store float %33, float* %34, align 4, !dbg !307
  %35 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 2, !dbg !308
  store float %33, float* %35, align 4, !dbg !309
  store i32 1, i32* %11, align 4, !dbg !310
  br label %36, !dbg !312

; <label>:36:                                     ; preds = %101, %3
  %37 = load i32, i32* %11, align 4, !dbg !313
  %38 = load i32, i32* %8, align 4, !dbg !316
  %39 = icmp slt i32 %37, %38, !dbg !317
  br i1 %39, label %40, label %104, !dbg !318

; <label>:40:                                     ; preds = %36
  %41 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 0, !dbg !319
  %42 = load float, float* %41, align 4, !dbg !319
  %43 = load i32, i32* %11, align 4, !dbg !321
  %44 = sext i32 %43 to i64, !dbg !322
  %45 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !322
  %46 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %45, i64 %44, !dbg !322
  %47 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %46, i32 0, i32 0, !dbg !323
  %48 = load float, float* %47, align 4, !dbg !323
  %49 = call float @fminf(float %42, float %48) #1, !dbg !324
  %50 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 0, !dbg !325
  store float %49, float* %50, align 4, !dbg !326
  %51 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 0, !dbg !327
  %52 = load float, float* %51, align 4, !dbg !327
  %53 = load i32, i32* %11, align 4, !dbg !328
  %54 = sext i32 %53 to i64, !dbg !329
  %55 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !329
  %56 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %55, i64 %54, !dbg !329
  %57 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %56, i32 0, i32 0, !dbg !330
  %58 = load float, float* %57, align 4, !dbg !330
  %59 = call float @fmaxf(float %52, float %58) #1, !dbg !331
  %60 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 0, !dbg !332
  store float %59, float* %60, align 4, !dbg !333
  %61 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 1, !dbg !334
  %62 = load float, float* %61, align 4, !dbg !334
  %63 = load i32, i32* %11, align 4, !dbg !335
  %64 = sext i32 %63 to i64, !dbg !336
  %65 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !336
  %66 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %65, i64 %64, !dbg !336
  %67 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %66, i32 0, i32 1, !dbg !337
  %68 = load float, float* %67, align 4, !dbg !337
  %69 = call float @fminf(float %62, float %68) #1, !dbg !338
  %70 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 1, !dbg !339
  store float %69, float* %70, align 4, !dbg !340
  %71 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 1, !dbg !341
  %72 = load float, float* %71, align 4, !dbg !341
  %73 = load i32, i32* %11, align 4, !dbg !342
  %74 = sext i32 %73 to i64, !dbg !343
  %75 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !343
  %76 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %75, i64 %74, !dbg !343
  %77 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %76, i32 0, i32 1, !dbg !344
  %78 = load float, float* %77, align 4, !dbg !344
  %79 = call float @fmaxf(float %72, float %78) #1, !dbg !345
  %80 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 1, !dbg !346
  store float %79, float* %80, align 4, !dbg !347
  %81 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 2, !dbg !348
  %82 = load float, float* %81, align 4, !dbg !348
  %83 = load i32, i32* %11, align 4, !dbg !349
  %84 = sext i32 %83 to i64, !dbg !350
  %85 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !350
  %86 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %85, i64 %84, !dbg !350
  %87 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %86, i32 0, i32 2, !dbg !351
  %88 = load float, float* %87, align 4, !dbg !351
  %89 = call float @fminf(float %82, float %88) #1, !dbg !352
  %90 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 2, !dbg !353
  store float %89, float* %90, align 4, !dbg !354
  %91 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 2, !dbg !355
  %92 = load float, float* %91, align 4, !dbg !355
  %93 = load i32, i32* %11, align 4, !dbg !356
  %94 = sext i32 %93 to i64, !dbg !357
  %95 = load %struct.Atom_t*, %struct.Atom_t** %7, align 8, !dbg !357
  %96 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %95, i64 %94, !dbg !357
  %97 = getelementptr inbounds %struct.Atom_t, %struct.Atom_t* %96, i32 0, i32 2, !dbg !358
  %98 = load float, float* %97, align 4, !dbg !358
  %99 = call float @fmaxf(float %92, float %98) #1, !dbg !359
  %100 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 2, !dbg !360
  store float %99, float* %100, align 4, !dbg !361
  br label %101, !dbg !362

; <label>:101:                                    ; preds = %40
  %102 = load i32, i32* %11, align 4, !dbg !363
  %103 = add nsw i32 %102, 1, !dbg !363
  store i32 %103, i32* %11, align 4, !dbg !363
  br label %36, !dbg !365, !llvm.loop !366

; <label>:104:                                    ; preds = %36
  %105 = load %struct.Vec3_t*, %struct.Vec3_t** %4, align 8, !dbg !368
  %106 = bitcast %struct.Vec3_t* %105 to i8*, !dbg !369
  %107 = bitcast %struct.Vec3_t* %9 to i8*, !dbg !369
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %106, i8* %107, i64 12, i32 4, i1 false), !dbg !369
  %108 = load %struct.Vec3_t*, %struct.Vec3_t** %5, align 8, !dbg !370
  %109 = bitcast %struct.Vec3_t* %108 to i8*, !dbg !371
  %110 = bitcast %struct.Vec3_t* %10 to i8*, !dbg !371
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %109, i8* %110, i64 12, i32 4, i1 false), !dbg !371
  ret void, !dbg !372
}

; Function Attrs: nounwind readnone
declare float @fminf(float, float) #5

; Function Attrs: nounwind readnone
declare float @fmaxf(float, float) #5

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #6

; Function Attrs: noinline
declare void @__check_dependence(i8*, i32, i8, i8*) #7

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readonly "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind }
attributes #7 = { noinline }
attributes #8 = { nounwind }
attributes #9 = { nounwind readonly }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!22, !23}
!llvm.ident = !{!24}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 4.0.0 (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause-clang.git 587724e9c6678af22b0b9d7649d8b84fb39aaaee) (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause.git 6f76a77790273ec9529638e6dc9391b206557c59)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2, retainedTypes: !3)
!1 = !DIFile(filename: "CMakeFiles/cutcp.dir/readatom-inst.c", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!2 = !{}
!3 = !{!4, !5, !15}
!4 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64, align: 64)
!5 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !6, size: 64, align: 64)
!6 = !DIDerivedType(tag: DW_TAG_typedef, name: "Atom", file: !7, line: 16, baseType: !8)
!7 = !DIFile(filename: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/atom.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!8 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Atom_t", file: !7, line: 16, size: 128, align: 32, elements: !9)
!9 = !{!10, !12, !13, !14}
!10 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !8, file: !7, line: 16, baseType: !11, size: 32, align: 32)
!11 = !DIBasicType(name: "float", size: 32, align: 32, encoding: DW_ATE_float)
!12 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !8, file: !7, line: 16, baseType: !11, size: 32, align: 32, offset: 32)
!13 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !8, file: !7, line: 16, baseType: !11, size: 32, align: 32, offset: 64)
!14 = !DIDerivedType(tag: DW_TAG_member, name: "q", scope: !8, file: !7, line: 16, baseType: !11, size: 32, align: 32, offset: 96)
!15 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !16, size: 64, align: 64)
!16 = !DIDerivedType(tag: DW_TAG_typedef, name: "Atoms", file: !7, line: 21, baseType: !17)
!17 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Atoms_t", file: !7, line: 18, size: 128, align: 64, elements: !18)
!18 = !{!19, !20}
!19 = !DIDerivedType(tag: DW_TAG_member, name: "atoms", scope: !17, file: !7, line: 19, baseType: !5, size: 64, align: 64)
!20 = !DIDerivedType(tag: DW_TAG_member, name: "size", scope: !17, file: !7, line: 20, baseType: !21, size: 32, align: 32, offset: 64)
!21 = !DIBasicType(name: "int", size: 32, align: 32, encoding: DW_ATE_signed)
!22 = !{i32 2, !"Dwarf Version", i32 4}
!23 = !{i32 2, !"Debug Info Version", i32 3}
!24 = !{!"clang version 4.0.0 (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause-clang.git 587724e9c6678af22b0b9d7649d8b84fb39aaaee) (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause.git 6f76a77790273ec9529638e6dc9391b206557c59)"}
!25 = distinct !DISubprogram(name: "read_atom_file", scope: !1, file: !1, line: 18, type: !26, isLocal: false, isDefinition: true, scopeLine: 18, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !2)
!26 = !DISubroutineType(types: !27)
!27 = !{!15, !28}
!28 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !29, size: 64, align: 64)
!29 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !30)
!30 = !DIBasicType(name: "char", size: 8, align: 8, encoding: DW_ATE_signed_char)
!31 = !DILocalVariable(name: "fname", arg: 1, scope: !25, file: !1, line: 18, type: !28)
!32 = !DIExpression()
!33 = !DILocation(line: 18, column: 35, scope: !25)
!34 = !DILocalVariable(name: "file", scope: !25, file: !1, line: 19, type: !35)
!35 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !36, size: 64, align: 64)
!36 = !DIDerivedType(tag: DW_TAG_typedef, name: "FILE", file: !37, line: 48, baseType: !38)
!37 = !DIFile(filename: "/usr/include/stdio.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!38 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_FILE", file: !39, line: 245, size: 1728, align: 64, elements: !40)
!39 = !DIFile(filename: "/usr/include/libio.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!40 = !{!41, !42, !44, !45, !46, !47, !48, !49, !50, !51, !52, !53, !54, !62, !63, !64, !65, !69, !71, !73, !77, !80, !82, !83, !84, !85, !86, !90, !91}
!41 = !DIDerivedType(tag: DW_TAG_member, name: "_flags", scope: !38, file: !39, line: 246, baseType: !21, size: 32, align: 32)
!42 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_ptr", scope: !38, file: !39, line: 251, baseType: !43, size: 64, align: 64, offset: 64)
!43 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !30, size: 64, align: 64)
!44 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_end", scope: !38, file: !39, line: 252, baseType: !43, size: 64, align: 64, offset: 128)
!45 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_base", scope: !38, file: !39, line: 253, baseType: !43, size: 64, align: 64, offset: 192)
!46 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_base", scope: !38, file: !39, line: 254, baseType: !43, size: 64, align: 64, offset: 256)
!47 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_ptr", scope: !38, file: !39, line: 255, baseType: !43, size: 64, align: 64, offset: 320)
!48 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_end", scope: !38, file: !39, line: 256, baseType: !43, size: 64, align: 64, offset: 384)
!49 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_buf_base", scope: !38, file: !39, line: 257, baseType: !43, size: 64, align: 64, offset: 448)
!50 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_buf_end", scope: !38, file: !39, line: 258, baseType: !43, size: 64, align: 64, offset: 512)
!51 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_save_base", scope: !38, file: !39, line: 260, baseType: !43, size: 64, align: 64, offset: 576)
!52 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_backup_base", scope: !38, file: !39, line: 261, baseType: !43, size: 64, align: 64, offset: 640)
!53 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_save_end", scope: !38, file: !39, line: 262, baseType: !43, size: 64, align: 64, offset: 704)
!54 = !DIDerivedType(tag: DW_TAG_member, name: "_markers", scope: !38, file: !39, line: 264, baseType: !55, size: 64, align: 64, offset: 768)
!55 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !56, size: 64, align: 64)
!56 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_marker", file: !39, line: 160, size: 192, align: 64, elements: !57)
!57 = !{!58, !59, !61}
!58 = !DIDerivedType(tag: DW_TAG_member, name: "_next", scope: !56, file: !39, line: 161, baseType: !55, size: 64, align: 64)
!59 = !DIDerivedType(tag: DW_TAG_member, name: "_sbuf", scope: !56, file: !39, line: 162, baseType: !60, size: 64, align: 64, offset: 64)
!60 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !38, size: 64, align: 64)
!61 = !DIDerivedType(tag: DW_TAG_member, name: "_pos", scope: !56, file: !39, line: 166, baseType: !21, size: 32, align: 32, offset: 128)
!62 = !DIDerivedType(tag: DW_TAG_member, name: "_chain", scope: !38, file: !39, line: 266, baseType: !60, size: 64, align: 64, offset: 832)
!63 = !DIDerivedType(tag: DW_TAG_member, name: "_fileno", scope: !38, file: !39, line: 268, baseType: !21, size: 32, align: 32, offset: 896)
!64 = !DIDerivedType(tag: DW_TAG_member, name: "_flags2", scope: !38, file: !39, line: 272, baseType: !21, size: 32, align: 32, offset: 928)
!65 = !DIDerivedType(tag: DW_TAG_member, name: "_old_offset", scope: !38, file: !39, line: 274, baseType: !66, size: 64, align: 64, offset: 960)
!66 = !DIDerivedType(tag: DW_TAG_typedef, name: "__off_t", file: !67, line: 131, baseType: !68)
!67 = !DIFile(filename: "/usr/include/bits/types.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!68 = !DIBasicType(name: "long int", size: 64, align: 64, encoding: DW_ATE_signed)
!69 = !DIDerivedType(tag: DW_TAG_member, name: "_cur_column", scope: !38, file: !39, line: 278, baseType: !70, size: 16, align: 16, offset: 1024)
!70 = !DIBasicType(name: "unsigned short", size: 16, align: 16, encoding: DW_ATE_unsigned)
!71 = !DIDerivedType(tag: DW_TAG_member, name: "_vtable_offset", scope: !38, file: !39, line: 279, baseType: !72, size: 8, align: 8, offset: 1040)
!72 = !DIBasicType(name: "signed char", size: 8, align: 8, encoding: DW_ATE_signed_char)
!73 = !DIDerivedType(tag: DW_TAG_member, name: "_shortbuf", scope: !38, file: !39, line: 280, baseType: !74, size: 8, align: 8, offset: 1048)
!74 = !DICompositeType(tag: DW_TAG_array_type, baseType: !30, size: 8, align: 8, elements: !75)
!75 = !{!76}
!76 = !DISubrange(count: 1)
!77 = !DIDerivedType(tag: DW_TAG_member, name: "_lock", scope: !38, file: !39, line: 284, baseType: !78, size: 64, align: 64, offset: 1088)
!78 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !79, size: 64, align: 64)
!79 = !DIDerivedType(tag: DW_TAG_typedef, name: "_IO_lock_t", file: !39, line: 154, baseType: null)
!80 = !DIDerivedType(tag: DW_TAG_member, name: "_offset", scope: !38, file: !39, line: 293, baseType: !81, size: 64, align: 64, offset: 1152)
!81 = !DIDerivedType(tag: DW_TAG_typedef, name: "__off64_t", file: !67, line: 132, baseType: !68)
!82 = !DIDerivedType(tag: DW_TAG_member, name: "__pad1", scope: !38, file: !39, line: 302, baseType: !4, size: 64, align: 64, offset: 1216)
!83 = !DIDerivedType(tag: DW_TAG_member, name: "__pad2", scope: !38, file: !39, line: 303, baseType: !4, size: 64, align: 64, offset: 1280)
!84 = !DIDerivedType(tag: DW_TAG_member, name: "__pad3", scope: !38, file: !39, line: 304, baseType: !4, size: 64, align: 64, offset: 1344)
!85 = !DIDerivedType(tag: DW_TAG_member, name: "__pad4", scope: !38, file: !39, line: 305, baseType: !4, size: 64, align: 64, offset: 1408)
!86 = !DIDerivedType(tag: DW_TAG_member, name: "__pad5", scope: !38, file: !39, line: 306, baseType: !87, size: 64, align: 64, offset: 1472)
!87 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !88, line: 62, baseType: !89)
!88 = !DIFile(filename: "/opt/aclang/install/bin/../lib/clang/4.0.0/include/stddef.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!89 = !DIBasicType(name: "long unsigned int", size: 64, align: 64, encoding: DW_ATE_unsigned)
!90 = !DIDerivedType(tag: DW_TAG_member, name: "_mode", scope: !38, file: !39, line: 308, baseType: !21, size: 32, align: 32, offset: 1536)
!91 = !DIDerivedType(tag: DW_TAG_member, name: "_unused2", scope: !38, file: !39, line: 310, baseType: !92, size: 160, align: 8, offset: 1568)
!92 = !DICompositeType(tag: DW_TAG_array_type, baseType: !30, size: 160, align: 8, elements: !93)
!93 = !{!94}
!94 = !DISubrange(count: 20)
!95 = !DILocation(line: 19, column: 9, scope: !25)
!96 = !DILocalVariable(name: "line", scope: !25, file: !1, line: 20, type: !97)
!97 = !DICompositeType(tag: DW_TAG_array_type, baseType: !30, size: 768, align: 8, elements: !98)
!98 = !{!99}
!99 = !DISubrange(count: 96)
!100 = !DILocation(line: 20, column: 8, scope: !25)
!101 = !DILocalVariable(name: "atom", scope: !25, file: !1, line: 22, type: !5)
!102 = !DILocation(line: 22, column: 9, scope: !25)
!103 = !DILocalVariable(name: "len", scope: !25, file: !1, line: 23, type: !21)
!104 = !DILocation(line: 23, column: 7, scope: !25)
!105 = !DILocalVariable(name: "cnt", scope: !25, file: !1, line: 24, type: !21)
!106 = !DILocation(line: 24, column: 7, scope: !25)
!107 = !DILocation(line: 27, column: 16, scope: !25)
!108 = !DILocation(line: 27, column: 10, scope: !25)
!109 = !DILocation(line: 27, column: 8, scope: !25)
!110 = !DILocation(line: 28, column: 15, scope: !111)
!111 = distinct !DILexicalBlock(scope: !25, file: !1, line: 28, column: 7)
!112 = !DILocation(line: 28, column: 12, scope: !111)
!113 = !DILocation(line: 28, column: 7, scope: !25)
!114 = !DILocation(line: 29, column: 13, scope: !115)
!115 = distinct !DILexicalBlock(scope: !111, file: !1, line: 28, column: 21)
!116 = !DILocation(line: 29, column: 61, scope: !115)
!117 = !DILocation(line: 29, column: 5, scope: !115)
!118 = !DILocation(line: 30, column: 5, scope: !115)
!119 = !DILocation(line: 34, column: 25, scope: !25)
!120 = !DILocation(line: 34, column: 29, scope: !25)
!121 = !DILocation(line: 34, column: 18, scope: !25)
!122 = !DILocation(line: 34, column: 10, scope: !25)
!123 = !DILocation(line: 34, column: 8, scope: !25)
!124 = !DILocation(line: 35, column: 15, scope: !125)
!125 = distinct !DILexicalBlock(scope: !25, file: !1, line: 35, column: 7)
!126 = !DILocation(line: 35, column: 12, scope: !125)
!127 = !DILocation(line: 35, column: 7, scope: !25)
!128 = !DILocation(line: 36, column: 13, scope: !129)
!129 = distinct !DILexicalBlock(scope: !125, file: !1, line: 35, column: 21)
!130 = !DILocation(line: 36, column: 5, scope: !129)
!131 = !DILocation(line: 37, column: 5, scope: !129)
!132 = !DILocation(line: 41, column: 3, scope: !25)
!133 = !DILocation(line: 41, column: 16, scope: !134)
!134 = !DILexicalBlockFile(scope: !25, file: !1, discriminator: 1)
!135 = !DILocation(line: 41, column: 31, scope: !134)
!136 = !DILocation(line: 41, column: 10, scope: !134)
!137 = !DILocation(line: 41, column: 37, scope: !134)
!138 = !DILocation(line: 41, column: 3, scope: !134)
!139 = !DILocation(line: 43, column: 17, scope: !140)
!140 = distinct !DILexicalBlock(scope: !141, file: !1, line: 43, column: 9)
!141 = distinct !DILexicalBlock(scope: !25, file: !1, line: 41, column: 46)
!142 = !DILocation(line: 43, column: 9, scope: !140)
!143 = !DILocation(line: 43, column: 36, scope: !140)
!144 = !DILocation(line: 43, column: 41, scope: !140)
!145 = !DILocation(line: 43, column: 52, scope: !146)
!146 = !DILexicalBlockFile(scope: !140, file: !1, discriminator: 1)
!147 = !DILocation(line: 43, column: 44, scope: !146)
!148 = !DILocation(line: 43, column: 71, scope: !146)
!149 = !DILocation(line: 43, column: 9, scope: !146)
!150 = !DILocation(line: 44, column: 7, scope: !151)
!151 = distinct !DILexicalBlock(scope: !140, file: !1, line: 43, column: 77)
!152 = distinct !{!152, !132}
!153 = !DILocation(line: 47, column: 9, scope: !154)
!154 = distinct !DILexicalBlock(scope: !141, file: !1, line: 47, column: 9)
!155 = !DILocation(line: 47, column: 16, scope: !154)
!156 = !DILocation(line: 47, column: 13, scope: !154)
!157 = !DILocation(line: 47, column: 9, scope: !141)
!158 = !DILocalVariable(name: "tmp", scope: !159, file: !1, line: 48, type: !4)
!159 = distinct !DILexicalBlock(scope: !154, file: !1, line: 47, column: 21)
!160 = !DILocation(line: 48, column: 13, scope: !159)
!161 = !DILocation(line: 48, column: 27, scope: !159)
!162 = !DILocation(line: 48, column: 37, scope: !159)
!163 = !DILocation(line: 48, column: 35, scope: !159)
!164 = !DILocation(line: 48, column: 33, scope: !159)
!165 = !DILocation(line: 48, column: 41, scope: !159)
!166 = !DILocation(line: 48, column: 19, scope: !159)
!167 = !DILocation(line: 49, column: 19, scope: !168)
!168 = distinct !DILexicalBlock(scope: !159, file: !1, line: 49, column: 11)
!169 = !DILocation(line: 49, column: 16, scope: !168)
!170 = !DILocation(line: 49, column: 11, scope: !159)
!171 = !DILocation(line: 50, column: 17, scope: !172)
!172 = distinct !DILexicalBlock(scope: !168, file: !1, line: 49, column: 24)
!173 = !DILocation(line: 50, column: 9, scope: !172)
!174 = !DILocation(line: 51, column: 9, scope: !172)
!175 = !DILocation(line: 53, column: 22, scope: !159)
!176 = !DILocation(line: 53, column: 14, scope: !159)
!177 = !DILocation(line: 53, column: 12, scope: !159)
!178 = !DILocation(line: 54, column: 11, scope: !159)
!179 = !DILocation(line: 55, column: 5, scope: !159)
!180 = !DILocation(line: 58, column: 16, scope: !181)
!181 = distinct !DILexicalBlock(scope: !141, file: !1, line: 58, column: 9)
!182 = !DILocation(line: 58, column: 64, scope: !181)
!183 = !DILocation(line: 58, column: 59, scope: !181)
!184 = !DILocation(line: 58, column: 69, scope: !181)
!185 = !DILocation(line: 59, column: 23, scope: !181)
!186 = !DILocation(line: 59, column: 18, scope: !181)
!187 = !DILocation(line: 59, column: 28, scope: !181)
!188 = !DILocation(line: 59, column: 39, scope: !181)
!189 = !DILocation(line: 59, column: 34, scope: !181)
!190 = !DILocation(line: 59, column: 44, scope: !181)
!191 = !DILocation(line: 59, column: 55, scope: !181)
!192 = !DILocation(line: 59, column: 50, scope: !181)
!193 = !DILocation(line: 59, column: 60, scope: !181)
!194 = !DILocation(line: 58, column: 9, scope: !181)
!195 = !DILocation(line: 59, column: 64, scope: !181)
!196 = !DILocation(line: 58, column: 9, scope: !141)
!197 = !DILocation(line: 60, column: 15, scope: !198)
!198 = distinct !DILexicalBlock(scope: !181, file: !1, line: 59, column: 70)
!199 = !DILocation(line: 61, column: 15, scope: !198)
!200 = !DILocation(line: 61, column: 19, scope: !198)
!201 = !DILocation(line: 60, column: 7, scope: !198)
!202 = !DILocation(line: 62, column: 7, scope: !198)
!203 = !DILocation(line: 65, column: 8, scope: !141)
!204 = !DILocation(line: 41, column: 3, scope: !205)
!205 = !DILexicalBlockFile(scope: !25, file: !1, discriminator: 2)
!206 = !DILocation(line: 69, column: 13, scope: !207)
!207 = distinct !DILexicalBlock(scope: !25, file: !1, line: 69, column: 7)
!208 = !DILocation(line: 69, column: 8, scope: !207)
!209 = !DILocation(line: 69, column: 7, scope: !25)
!210 = !DILocation(line: 70, column: 13, scope: !211)
!211 = distinct !DILexicalBlock(scope: !207, file: !1, line: 69, column: 20)
!212 = !DILocation(line: 70, column: 5, scope: !211)
!213 = !DILocation(line: 71, column: 5, scope: !211)
!214 = !DILocation(line: 73, column: 14, scope: !215)
!215 = distinct !DILexicalBlock(scope: !25, file: !1, line: 73, column: 7)
!216 = !DILocation(line: 73, column: 7, scope: !215)
!217 = !DILocation(line: 73, column: 7, scope: !25)
!218 = !DILocation(line: 74, column: 13, scope: !219)
!219 = distinct !DILexicalBlock(scope: !215, file: !1, line: 73, column: 21)
!220 = !DILocation(line: 74, column: 5, scope: !219)
!221 = !DILocation(line: 75, column: 5, scope: !219)
!222 = !DILocalVariable(name: "out", scope: !223, file: !1, line: 80, type: !15)
!223 = distinct !DILexicalBlock(scope: !25, file: !1, line: 79, column: 3)
!224 = !DILocation(line: 80, column: 12, scope: !223)
!225 = !DILocation(line: 80, column: 27, scope: !223)
!226 = !DILocation(line: 80, column: 18, scope: !223)
!227 = !DILocation(line: 82, column: 17, scope: !228)
!228 = distinct !DILexicalBlock(scope: !223, file: !1, line: 82, column: 9)
!229 = !DILocation(line: 82, column: 14, scope: !228)
!230 = !DILocation(line: 82, column: 9, scope: !223)
!231 = !DILocation(line: 83, column: 15, scope: !232)
!232 = distinct !DILexicalBlock(scope: !228, file: !1, line: 82, column: 22)
!233 = !DILocation(line: 83, column: 7, scope: !232)
!234 = !DILocation(line: 84, column: 7, scope: !232)
!235 = !DILocation(line: 87, column: 17, scope: !223)
!236 = !DILocation(line: 87, column: 5, scope: !223)
!237 = !DILocation(line: 87, column: 10, scope: !223)
!238 = !DILocation(line: 87, column: 15, scope: !223)
!239 = !DILocation(line: 88, column: 18, scope: !223)
!240 = !DILocation(line: 88, column: 5, scope: !223)
!241 = !DILocation(line: 88, column: 10, scope: !223)
!242 = !DILocation(line: 88, column: 16, scope: !223)
!243 = !DILocation(line: 90, column: 12, scope: !223)
!244 = !DILocation(line: 90, column: 5, scope: !223)
!245 = !DILocation(line: 92, column: 1, scope: !25)
!246 = distinct !DISubprogram(name: "free_atom", scope: !1, file: !1, line: 94, type: !247, isLocal: false, isDefinition: true, scopeLine: 94, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !2)
!247 = !DISubroutineType(types: !248)
!248 = !{null, !15}
!249 = !DILocalVariable(name: "atom", arg: 1, scope: !246, file: !1, line: 94, type: !15)
!250 = !DILocation(line: 94, column: 23, scope: !246)
!251 = !DILocation(line: 95, column: 7, scope: !252)
!252 = distinct !DILexicalBlock(scope: !246, file: !1, line: 95, column: 7)
!253 = !DILocation(line: 95, column: 7, scope: !246)
!254 = !DILocation(line: 96, column: 10, scope: !255)
!255 = distinct !DILexicalBlock(scope: !252, file: !1, line: 95, column: 13)
!256 = !DILocation(line: 96, column: 16, scope: !255)
!257 = !DILocation(line: 96, column: 5, scope: !255)
!258 = !DILocation(line: 97, column: 10, scope: !255)
!259 = !DILocation(line: 97, column: 5, scope: !255)
!260 = !DILocation(line: 98, column: 3, scope: !255)
!261 = !DILocation(line: 99, column: 1, scope: !246)
!262 = distinct !DISubprogram(name: "get_atom_extent", scope: !1, file: !1, line: 101, type: !263, isLocal: false, isDefinition: true, scopeLine: 101, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !2)
!263 = !DISubroutineType(types: !264)
!264 = !{null, !265, !265, !15}
!265 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !266, size: 64, align: 64)
!266 = !DIDerivedType(tag: DW_TAG_typedef, name: "Vec3", file: !7, line: 23, baseType: !267)
!267 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Vec3_t", file: !7, line: 23, size: 96, align: 32, elements: !268)
!268 = !{!269, !270, !271}
!269 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !267, file: !7, line: 23, baseType: !11, size: 32, align: 32)
!270 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !267, file: !7, line: 23, baseType: !11, size: 32, align: 32, offset: 32)
!271 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !267, file: !7, line: 23, baseType: !11, size: 32, align: 32, offset: 64)
!272 = !DILocalVariable(name: "out_lo", arg: 1, scope: !262, file: !1, line: 101, type: !265)
!273 = !DILocation(line: 101, column: 28, scope: !262)
!274 = !DILocalVariable(name: "out_hi", arg: 2, scope: !262, file: !1, line: 101, type: !265)
!275 = !DILocation(line: 101, column: 42, scope: !262)
!276 = !DILocalVariable(name: "atom", arg: 3, scope: !262, file: !1, line: 101, type: !15)
!277 = !DILocation(line: 101, column: 57, scope: !262)
!278 = !DILocalVariable(name: "atoms", scope: !262, file: !1, line: 102, type: !5)
!279 = !DILocation(line: 102, column: 9, scope: !262)
!280 = !DILocation(line: 102, column: 17, scope: !262)
!281 = !DILocation(line: 102, column: 23, scope: !262)
!282 = !DILocalVariable(name: "natoms", scope: !262, file: !1, line: 103, type: !21)
!283 = !DILocation(line: 103, column: 7, scope: !262)
!284 = !DILocation(line: 103, column: 16, scope: !262)
!285 = !DILocation(line: 103, column: 22, scope: !262)
!286 = !DILocalVariable(name: "lo", scope: !262, file: !1, line: 104, type: !266)
!287 = !DILocation(line: 104, column: 8, scope: !262)
!288 = !DILocalVariable(name: "hi", scope: !262, file: !1, line: 105, type: !266)
!289 = !DILocation(line: 105, column: 8, scope: !262)
!290 = !DILocalVariable(name: "n", scope: !262, file: !1, line: 106, type: !21)
!291 = !DILocation(line: 106, column: 7, scope: !262)
!292 = !DILocation(line: 108, column: 17, scope: !262)
!293 = !DILocation(line: 108, column: 26, scope: !262)
!294 = !DILocation(line: 108, column: 13, scope: !262)
!295 = !DILocation(line: 108, column: 15, scope: !262)
!296 = !DILocation(line: 108, column: 6, scope: !262)
!297 = !DILocation(line: 108, column: 8, scope: !262)
!298 = !DILocation(line: 109, column: 17, scope: !262)
!299 = !DILocation(line: 109, column: 26, scope: !262)
!300 = !DILocation(line: 109, column: 13, scope: !262)
!301 = !DILocation(line: 109, column: 15, scope: !262)
!302 = !DILocation(line: 109, column: 6, scope: !262)
!303 = !DILocation(line: 109, column: 8, scope: !262)
!304 = !DILocation(line: 110, column: 17, scope: !262)
!305 = !DILocation(line: 110, column: 26, scope: !262)
!306 = !DILocation(line: 110, column: 13, scope: !262)
!307 = !DILocation(line: 110, column: 15, scope: !262)
!308 = !DILocation(line: 110, column: 6, scope: !262)
!309 = !DILocation(line: 110, column: 8, scope: !262)
!310 = !DILocation(line: 112, column: 10, scope: !311)
!311 = distinct !DILexicalBlock(scope: !262, file: !1, line: 112, column: 3)
!312 = !DILocation(line: 112, column: 8, scope: !311)
!313 = !DILocation(line: 112, column: 15, scope: !314)
!314 = !DILexicalBlockFile(scope: !315, file: !1, discriminator: 1)
!315 = distinct !DILexicalBlock(scope: !311, file: !1, line: 112, column: 3)
!316 = !DILocation(line: 112, column: 19, scope: !314)
!317 = !DILocation(line: 112, column: 17, scope: !314)
!318 = !DILocation(line: 112, column: 3, scope: !314)
!319 = !DILocation(line: 113, column: 21, scope: !320)
!320 = distinct !DILexicalBlock(scope: !315, file: !1, line: 112, column: 32)
!321 = !DILocation(line: 113, column: 30, scope: !320)
!322 = !DILocation(line: 113, column: 24, scope: !320)
!323 = !DILocation(line: 113, column: 33, scope: !320)
!324 = !DILocation(line: 113, column: 12, scope: !320)
!325 = !DILocation(line: 113, column: 8, scope: !320)
!326 = !DILocation(line: 113, column: 10, scope: !320)
!327 = !DILocation(line: 114, column: 21, scope: !320)
!328 = !DILocation(line: 114, column: 30, scope: !320)
!329 = !DILocation(line: 114, column: 24, scope: !320)
!330 = !DILocation(line: 114, column: 33, scope: !320)
!331 = !DILocation(line: 114, column: 12, scope: !320)
!332 = !DILocation(line: 114, column: 8, scope: !320)
!333 = !DILocation(line: 114, column: 10, scope: !320)
!334 = !DILocation(line: 115, column: 21, scope: !320)
!335 = !DILocation(line: 115, column: 30, scope: !320)
!336 = !DILocation(line: 115, column: 24, scope: !320)
!337 = !DILocation(line: 115, column: 33, scope: !320)
!338 = !DILocation(line: 115, column: 12, scope: !320)
!339 = !DILocation(line: 115, column: 8, scope: !320)
!340 = !DILocation(line: 115, column: 10, scope: !320)
!341 = !DILocation(line: 116, column: 21, scope: !320)
!342 = !DILocation(line: 116, column: 30, scope: !320)
!343 = !DILocation(line: 116, column: 24, scope: !320)
!344 = !DILocation(line: 116, column: 33, scope: !320)
!345 = !DILocation(line: 116, column: 12, scope: !320)
!346 = !DILocation(line: 116, column: 8, scope: !320)
!347 = !DILocation(line: 116, column: 10, scope: !320)
!348 = !DILocation(line: 117, column: 21, scope: !320)
!349 = !DILocation(line: 117, column: 30, scope: !320)
!350 = !DILocation(line: 117, column: 24, scope: !320)
!351 = !DILocation(line: 117, column: 33, scope: !320)
!352 = !DILocation(line: 117, column: 12, scope: !320)
!353 = !DILocation(line: 117, column: 8, scope: !320)
!354 = !DILocation(line: 117, column: 10, scope: !320)
!355 = !DILocation(line: 118, column: 21, scope: !320)
!356 = !DILocation(line: 118, column: 30, scope: !320)
!357 = !DILocation(line: 118, column: 24, scope: !320)
!358 = !DILocation(line: 118, column: 33, scope: !320)
!359 = !DILocation(line: 118, column: 12, scope: !320)
!360 = !DILocation(line: 118, column: 8, scope: !320)
!361 = !DILocation(line: 118, column: 10, scope: !320)
!362 = !DILocation(line: 119, column: 3, scope: !320)
!363 = !DILocation(line: 112, column: 28, scope: !364)
!364 = !DILexicalBlockFile(scope: !315, file: !1, discriminator: 2)
!365 = !DILocation(line: 112, column: 3, scope: !364)
!366 = distinct !{!366, !367}
!367 = !DILocation(line: 112, column: 3, scope: !262)
!368 = !DILocation(line: 121, column: 4, scope: !262)
!369 = !DILocation(line: 121, column: 13, scope: !262)
!370 = !DILocation(line: 122, column: 4, scope: !262)
!371 = !DILocation(line: 122, column: 13, scope: !262)
!372 = !DILocation(line: 123, column: 1, scope: !262)
