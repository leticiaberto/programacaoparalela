; ModuleID = 'CMakeFiles/cutcp.dir/output-inst.c'
source_filename = "CMakeFiles/cutcp.dir/output-inst.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.Lattice_t = type { %struct.LatticeDim_t, float* }
%struct.LatticeDim_t = type { i32, i32, i32, %struct.Vec3_t, float }
%struct.Vec3_t = type { float, float, float }

@.str = private unnamed_addr constant [2 x i8] c"w\00", align 1
@stderr = external global %struct._IO_FILE*, align 8
@.str.1 = private unnamed_addr constant [25 x i8] c"Cannot open output file\0A\00", align 1
@0 = internal constant [35 x i8] c"CMakeFiles/cutcp.dir/output-inst.c\00"

; Function Attrs: nounwind uwtable
define void @write_lattice_summary(i8*, %struct.Lattice_t*) #0 !dbg !13 {
  %3 = alloca i8*, align 8
  %4 = alloca %struct.Lattice_t*, align 8
  %5 = alloca float*, align 8
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca %struct._IO_FILE*, align 8
  %10 = alloca double, align 8
  %11 = alloca float, align 4
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca i32, align 4
  store i8* %0, i8** %3, align 8
  call void @llvm.dbg.declare(metadata i8** %3, metadata !43, metadata !44), !dbg !45
  store %struct.Lattice_t* %1, %struct.Lattice_t** %4, align 8
  call void @llvm.dbg.declare(metadata %struct.Lattice_t** %4, metadata !46, metadata !44), !dbg !47
  call void @llvm.dbg.declare(metadata float** %5, metadata !48, metadata !44), !dbg !49
  %15 = load %struct.Lattice_t*, %struct.Lattice_t** %4, align 8, !dbg !50
  %16 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %15, i32 0, i32 1, !dbg !51
  %17 = load float*, float** %16, align 8, !dbg !51
  store float* %17, float** %5, align 8, !dbg !49
  call void @llvm.dbg.declare(metadata i32* %6, metadata !52, metadata !44), !dbg !53
  %18 = load %struct.Lattice_t*, %struct.Lattice_t** %4, align 8, !dbg !54
  %19 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %18, i32 0, i32 0, !dbg !55
  %20 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %19, i32 0, i32 0, !dbg !56
  %21 = load i32, i32* %20, align 8, !dbg !56
  store i32 %21, i32* %6, align 4, !dbg !53
  call void @llvm.dbg.declare(metadata i32* %7, metadata !57, metadata !44), !dbg !58
  %22 = load %struct.Lattice_t*, %struct.Lattice_t** %4, align 8, !dbg !59
  %23 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %22, i32 0, i32 0, !dbg !60
  %24 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %23, i32 0, i32 1, !dbg !61
  %25 = load i32, i32* %24, align 4, !dbg !61
  store i32 %25, i32* %7, align 4, !dbg !58
  call void @llvm.dbg.declare(metadata i32* %8, metadata !62, metadata !44), !dbg !63
  %26 = load %struct.Lattice_t*, %struct.Lattice_t** %4, align 8, !dbg !64
  %27 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %26, i32 0, i32 0, !dbg !65
  %28 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %27, i32 0, i32 2, !dbg !66
  %29 = load i32, i32* %28, align 8, !dbg !66
  store i32 %29, i32* %8, align 4, !dbg !63
  call void @llvm.dbg.declare(metadata %struct._IO_FILE** %9, metadata !67, metadata !44), !dbg !128
  %30 = load i8*, i8** %3, align 8, !dbg !129
  %31 = call %struct._IO_FILE* @fopen(i8* %30, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0)), !dbg !130
  store %struct._IO_FILE* %31, %struct._IO_FILE** %9, align 8, !dbg !128
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** %9, align 8, !dbg !131
  %33 = icmp eq %struct._IO_FILE* %32, null, !dbg !133
  br i1 %33, label %34, label %37, !dbg !134

; <label>:34:                                     ; preds = %2
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !135
  %36 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %35, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.1, i32 0, i32 0)), !dbg !137
  call void @exit(i32 1) #6, !dbg !138
  unreachable, !dbg !138

; <label>:37:                                     ; preds = %2
  call void @llvm.dbg.declare(metadata double* %10, metadata !139, metadata !44), !dbg !141
  store double 0.000000e+00, double* %10, align 8, !dbg !141
  call void @llvm.dbg.declare(metadata float* %11, metadata !142, metadata !44), !dbg !143
  call void @llvm.dbg.declare(metadata i32* %12, metadata !144, metadata !44), !dbg !145
  store i32 0, i32* %12, align 4, !dbg !146
  br label %38, !dbg !148

; <label>:38:                                     ; preds = %56, %37
  %39 = load i32, i32* %12, align 4, !dbg !149
  %40 = load i32, i32* %6, align 4, !dbg !152
  %41 = load i32, i32* %7, align 4, !dbg !153
  %42 = mul nsw i32 %40, %41, !dbg !154
  %43 = load i32, i32* %8, align 4, !dbg !155
  %44 = mul nsw i32 %42, %43, !dbg !156
  %45 = icmp slt i32 %39, %44, !dbg !157
  br i1 %45, label %46, label %59, !dbg !158

; <label>:46:                                     ; preds = %38
  %47 = load i32, i32* %12, align 4, !dbg !159
  %48 = sext i32 %47 to i64, !dbg !161
  %49 = load float*, float** %5, align 8, !dbg !161
  %50 = getelementptr inbounds float, float* %49, i64 %48, !dbg !161
  %51 = load float, float* %50, align 4, !dbg !161
  %52 = fpext float %51 to double, !dbg !162
  %53 = call double @fabs(double %52) #1, !dbg !163
  %54 = load double, double* %10, align 8, !dbg !164
  %55 = fadd double %54, %53, !dbg !164
  store double %55, double* %10, align 8, !dbg !164
  br label %56, !dbg !165

; <label>:56:                                     ; preds = %46
  %57 = load i32, i32* %12, align 4, !dbg !166
  %58 = add nsw i32 %57, 1, !dbg !166
  store i32 %58, i32* %12, align 4, !dbg !166
  br label %38, !dbg !168, !llvm.loop !169

; <label>:59:                                     ; preds = %38
  %60 = load double, double* %10, align 8, !dbg !171
  %61 = fptrunc double %60 to float, !dbg !172
  store float %61, float* %11, align 4, !dbg !173
  %62 = bitcast float* %11 to i8*, !dbg !174
  %63 = load %struct._IO_FILE*, %struct._IO_FILE** %9, align 8, !dbg !175
  %64 = call i64 @fwrite(i8* %62, i64 1, i64 4, %struct._IO_FILE* %63), !dbg !176
  call void @llvm.dbg.declare(metadata i32* %13, metadata !177, metadata !44), !dbg !179
  %65 = load %struct.Lattice_t*, %struct.Lattice_t** %4, align 8, !dbg !180
  %66 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %65, i32 0, i32 0, !dbg !181
  %67 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %66, i32 0, i32 0, !dbg !182
  %68 = load i32, i32* %67, align 8, !dbg !182
  %69 = load %struct.Lattice_t*, %struct.Lattice_t** %4, align 8, !dbg !183
  %70 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %69, i32 0, i32 0, !dbg !184
  %71 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %70, i32 0, i32 1, !dbg !185
  %72 = load i32, i32* %71, align 4, !dbg !185
  %73 = mul nsw i32 %68, %72, !dbg !186
  store i32 %73, i32* %13, align 4, !dbg !187
  %74 = bitcast i32* %13 to i8*, !dbg !188
  %75 = load %struct._IO_FILE*, %struct._IO_FILE** %9, align 8, !dbg !189
  %76 = call i64 @fwrite(i8* %74, i64 1, i64 4, %struct._IO_FILE* %75), !dbg !190
  call void @llvm.dbg.declare(metadata i32* %14, metadata !191, metadata !44), !dbg !193
  %77 = load i32, i32* %6, align 4, !dbg !194
  %78 = load i32, i32* %7, align 4, !dbg !195
  %79 = mul nsw i32 %77, %78, !dbg !196
  store i32 %79, i32* %14, align 4, !dbg !193
  %80 = load float*, float** %5, align 8, !dbg !197
  %81 = bitcast float* %80 to i8*, !dbg !197
  %82 = load i32, i32* %14, align 4, !dbg !198
  %83 = sext i32 %82 to i64, !dbg !198
  %84 = load %struct._IO_FILE*, %struct._IO_FILE** %9, align 8, !dbg !199
  %85 = call i64 @fwrite(i8* %81, i64 %83, i64 4, %struct._IO_FILE* %84), !dbg !200
  %86 = load float*, float** %5, align 8, !dbg !201
  %87 = load i32, i32* %8, align 4, !dbg !202
  %88 = sub nsw i32 %87, 1, !dbg !203
  %89 = load i32, i32* %14, align 4, !dbg !204
  %90 = mul nsw i32 %88, %89, !dbg !205
  %91 = sext i32 %90 to i64, !dbg !206
  %92 = getelementptr inbounds float, float* %86, i64 %91, !dbg !206
  %93 = bitcast float* %92 to i8*, !dbg !201
  %94 = load i32, i32* %14, align 4, !dbg !207
  %95 = sext i32 %94 to i64, !dbg !207
  %96 = load %struct._IO_FILE*, %struct._IO_FILE** %9, align 8, !dbg !208
  %97 = call i64 @fwrite(i8* %93, i64 %95, i64 4, %struct._IO_FILE* %96), !dbg !209
  %98 = load %struct._IO_FILE*, %struct._IO_FILE** %9, align 8, !dbg !210
  %99 = call i32 @fclose(%struct._IO_FILE* %98), !dbg !211
  ret void, !dbg !212
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

declare %struct._IO_FILE* @fopen(i8*, i8*) #2

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: noreturn nounwind
declare void @exit(i32) #3

; Function Attrs: nounwind readnone
declare double @fabs(double) #4

declare i64 @fwrite(i8*, i64, i64, %struct._IO_FILE*) #2

declare i32 @fclose(%struct._IO_FILE*) #2

; Function Attrs: noinline
declare void @__check_dependence(i8*, i32, i8, i8*) #5

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline }
attributes #6 = { noreturn nounwind }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!10, !11}
!llvm.ident = !{!12}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 4.0.0 (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause-clang.git 587724e9c6678af22b0b9d7649d8b84fb39aaaee) (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause.git 6f76a77790273ec9529638e6dc9391b206557c59)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2, retainedTypes: !3)
!1 = !DIFile(filename: "CMakeFiles/cutcp.dir/output-inst.c", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!2 = !{}
!3 = !{!4, !5, !6, !7}
!4 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64, align: 64)
!5 = !DIBasicType(name: "double", size: 64, align: 64, encoding: DW_ATE_float)
!6 = !DIBasicType(name: "float", size: 32, align: 32, encoding: DW_ATE_float)
!7 = !DIDerivedType(tag: DW_TAG_typedef, name: "uint32_t", file: !8, line: 51, baseType: !9)
!8 = !DIFile(filename: "/usr/include/stdint.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!9 = !DIBasicType(name: "unsigned int", size: 32, align: 32, encoding: DW_ATE_unsigned)
!10 = !{i32 2, !"Dwarf Version", i32 4}
!11 = !{i32 2, !"Debug Info Version", i32 3}
!12 = !{!"clang version 4.0.0 (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause-clang.git 587724e9c6678af22b0b9d7649d8b84fb39aaaee) (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause.git 6f76a77790273ec9529638e6dc9391b206557c59)"}
!13 = distinct !DISubprogram(name: "write_lattice_summary", scope: !1, file: !1, line: 16, type: !14, isLocal: false, isDefinition: true, scopeLine: 16, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !2)
!14 = !DISubroutineType(types: !15)
!15 = !{null, !16, !19}
!16 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !17, size: 64, align: 64)
!17 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !18)
!18 = !DIBasicType(name: "char", size: 8, align: 8, encoding: DW_ATE_signed_char)
!19 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !20, size: 64, align: 64)
!20 = !DIDerivedType(tag: DW_TAG_typedef, name: "Lattice", file: !21, line: 40, baseType: !22)
!21 = !DIFile(filename: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/cutoff.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!22 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Lattice_t", file: !21, line: 37, size: 320, align: 64, elements: !23)
!23 = !{!24, !41}
!24 = !DIDerivedType(tag: DW_TAG_member, name: "dim", scope: !22, file: !21, line: 38, baseType: !25, size: 224, align: 32)
!25 = !DIDerivedType(tag: DW_TAG_typedef, name: "LatticeDim", file: !21, line: 32, baseType: !26)
!26 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "LatticeDim_t", file: !21, line: 23, size: 224, align: 32, elements: !27)
!27 = !{!28, !30, !31, !32, !40}
!28 = !DIDerivedType(tag: DW_TAG_member, name: "nx", scope: !26, file: !21, line: 25, baseType: !29, size: 32, align: 32)
!29 = !DIBasicType(name: "int", size: 32, align: 32, encoding: DW_ATE_signed)
!30 = !DIDerivedType(tag: DW_TAG_member, name: "ny", scope: !26, file: !21, line: 25, baseType: !29, size: 32, align: 32, offset: 32)
!31 = !DIDerivedType(tag: DW_TAG_member, name: "nz", scope: !26, file: !21, line: 25, baseType: !29, size: 32, align: 32, offset: 64)
!32 = !DIDerivedType(tag: DW_TAG_member, name: "lo", scope: !26, file: !21, line: 28, baseType: !33, size: 96, align: 32, offset: 96)
!33 = !DIDerivedType(tag: DW_TAG_typedef, name: "Vec3", file: !34, line: 23, baseType: !35)
!34 = !DIFile(filename: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/atom.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!35 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Vec3_t", file: !34, line: 23, size: 96, align: 32, elements: !36)
!36 = !{!37, !38, !39}
!37 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !35, file: !34, line: 23, baseType: !6, size: 32, align: 32)
!38 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !35, file: !34, line: 23, baseType: !6, size: 32, align: 32, offset: 32)
!39 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !35, file: !34, line: 23, baseType: !6, size: 32, align: 32, offset: 64)
!40 = !DIDerivedType(tag: DW_TAG_member, name: "h", scope: !26, file: !21, line: 31, baseType: !6, size: 32, align: 32, offset: 192)
!41 = !DIDerivedType(tag: DW_TAG_member, name: "lattice", scope: !22, file: !21, line: 39, baseType: !42, size: 64, align: 64, offset: 256)
!42 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !6, size: 64, align: 64)
!43 = !DILocalVariable(name: "filename", arg: 1, scope: !13, file: !1, line: 16, type: !16)
!44 = !DIExpression()
!45 = !DILocation(line: 16, column: 40, scope: !13)
!46 = !DILocalVariable(name: "lattice", arg: 2, scope: !13, file: !1, line: 16, type: !19)
!47 = !DILocation(line: 16, column: 59, scope: !13)
!48 = !DILocalVariable(name: "lattice_data", scope: !13, file: !1, line: 17, type: !42)
!49 = !DILocation(line: 17, column: 10, scope: !13)
!50 = !DILocation(line: 17, column: 25, scope: !13)
!51 = !DILocation(line: 17, column: 34, scope: !13)
!52 = !DILocalVariable(name: "nx", scope: !13, file: !1, line: 18, type: !29)
!53 = !DILocation(line: 18, column: 7, scope: !13)
!54 = !DILocation(line: 18, column: 12, scope: !13)
!55 = !DILocation(line: 18, column: 21, scope: !13)
!56 = !DILocation(line: 18, column: 25, scope: !13)
!57 = !DILocalVariable(name: "ny", scope: !13, file: !1, line: 19, type: !29)
!58 = !DILocation(line: 19, column: 7, scope: !13)
!59 = !DILocation(line: 19, column: 12, scope: !13)
!60 = !DILocation(line: 19, column: 21, scope: !13)
!61 = !DILocation(line: 19, column: 25, scope: !13)
!62 = !DILocalVariable(name: "nz", scope: !13, file: !1, line: 20, type: !29)
!63 = !DILocation(line: 20, column: 7, scope: !13)
!64 = !DILocation(line: 20, column: 12, scope: !13)
!65 = !DILocation(line: 20, column: 21, scope: !13)
!66 = !DILocation(line: 20, column: 25, scope: !13)
!67 = !DILocalVariable(name: "outfile", scope: !13, file: !1, line: 23, type: !68)
!68 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !69, size: 64, align: 64)
!69 = !DIDerivedType(tag: DW_TAG_typedef, name: "FILE", file: !70, line: 48, baseType: !71)
!70 = !DIFile(filename: "/usr/include/stdio.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!71 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_FILE", file: !72, line: 245, size: 1728, align: 64, elements: !73)
!72 = !DIFile(filename: "/usr/include/libio.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!73 = !{!74, !75, !77, !78, !79, !80, !81, !82, !83, !84, !85, !86, !87, !95, !96, !97, !98, !102, !104, !106, !110, !113, !115, !116, !117, !118, !119, !123, !124}
!74 = !DIDerivedType(tag: DW_TAG_member, name: "_flags", scope: !71, file: !72, line: 246, baseType: !29, size: 32, align: 32)
!75 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_ptr", scope: !71, file: !72, line: 251, baseType: !76, size: 64, align: 64, offset: 64)
!76 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !18, size: 64, align: 64)
!77 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_end", scope: !71, file: !72, line: 252, baseType: !76, size: 64, align: 64, offset: 128)
!78 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_base", scope: !71, file: !72, line: 253, baseType: !76, size: 64, align: 64, offset: 192)
!79 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_base", scope: !71, file: !72, line: 254, baseType: !76, size: 64, align: 64, offset: 256)
!80 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_ptr", scope: !71, file: !72, line: 255, baseType: !76, size: 64, align: 64, offset: 320)
!81 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_end", scope: !71, file: !72, line: 256, baseType: !76, size: 64, align: 64, offset: 384)
!82 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_buf_base", scope: !71, file: !72, line: 257, baseType: !76, size: 64, align: 64, offset: 448)
!83 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_buf_end", scope: !71, file: !72, line: 258, baseType: !76, size: 64, align: 64, offset: 512)
!84 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_save_base", scope: !71, file: !72, line: 260, baseType: !76, size: 64, align: 64, offset: 576)
!85 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_backup_base", scope: !71, file: !72, line: 261, baseType: !76, size: 64, align: 64, offset: 640)
!86 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_save_end", scope: !71, file: !72, line: 262, baseType: !76, size: 64, align: 64, offset: 704)
!87 = !DIDerivedType(tag: DW_TAG_member, name: "_markers", scope: !71, file: !72, line: 264, baseType: !88, size: 64, align: 64, offset: 768)
!88 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !89, size: 64, align: 64)
!89 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_marker", file: !72, line: 160, size: 192, align: 64, elements: !90)
!90 = !{!91, !92, !94}
!91 = !DIDerivedType(tag: DW_TAG_member, name: "_next", scope: !89, file: !72, line: 161, baseType: !88, size: 64, align: 64)
!92 = !DIDerivedType(tag: DW_TAG_member, name: "_sbuf", scope: !89, file: !72, line: 162, baseType: !93, size: 64, align: 64, offset: 64)
!93 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !71, size: 64, align: 64)
!94 = !DIDerivedType(tag: DW_TAG_member, name: "_pos", scope: !89, file: !72, line: 166, baseType: !29, size: 32, align: 32, offset: 128)
!95 = !DIDerivedType(tag: DW_TAG_member, name: "_chain", scope: !71, file: !72, line: 266, baseType: !93, size: 64, align: 64, offset: 832)
!96 = !DIDerivedType(tag: DW_TAG_member, name: "_fileno", scope: !71, file: !72, line: 268, baseType: !29, size: 32, align: 32, offset: 896)
!97 = !DIDerivedType(tag: DW_TAG_member, name: "_flags2", scope: !71, file: !72, line: 272, baseType: !29, size: 32, align: 32, offset: 928)
!98 = !DIDerivedType(tag: DW_TAG_member, name: "_old_offset", scope: !71, file: !72, line: 274, baseType: !99, size: 64, align: 64, offset: 960)
!99 = !DIDerivedType(tag: DW_TAG_typedef, name: "__off_t", file: !100, line: 131, baseType: !101)
!100 = !DIFile(filename: "/usr/include/bits/types.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!101 = !DIBasicType(name: "long int", size: 64, align: 64, encoding: DW_ATE_signed)
!102 = !DIDerivedType(tag: DW_TAG_member, name: "_cur_column", scope: !71, file: !72, line: 278, baseType: !103, size: 16, align: 16, offset: 1024)
!103 = !DIBasicType(name: "unsigned short", size: 16, align: 16, encoding: DW_ATE_unsigned)
!104 = !DIDerivedType(tag: DW_TAG_member, name: "_vtable_offset", scope: !71, file: !72, line: 279, baseType: !105, size: 8, align: 8, offset: 1040)
!105 = !DIBasicType(name: "signed char", size: 8, align: 8, encoding: DW_ATE_signed_char)
!106 = !DIDerivedType(tag: DW_TAG_member, name: "_shortbuf", scope: !71, file: !72, line: 280, baseType: !107, size: 8, align: 8, offset: 1048)
!107 = !DICompositeType(tag: DW_TAG_array_type, baseType: !18, size: 8, align: 8, elements: !108)
!108 = !{!109}
!109 = !DISubrange(count: 1)
!110 = !DIDerivedType(tag: DW_TAG_member, name: "_lock", scope: !71, file: !72, line: 284, baseType: !111, size: 64, align: 64, offset: 1088)
!111 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !112, size: 64, align: 64)
!112 = !DIDerivedType(tag: DW_TAG_typedef, name: "_IO_lock_t", file: !72, line: 154, baseType: null)
!113 = !DIDerivedType(tag: DW_TAG_member, name: "_offset", scope: !71, file: !72, line: 293, baseType: !114, size: 64, align: 64, offset: 1152)
!114 = !DIDerivedType(tag: DW_TAG_typedef, name: "__off64_t", file: !100, line: 132, baseType: !101)
!115 = !DIDerivedType(tag: DW_TAG_member, name: "__pad1", scope: !71, file: !72, line: 302, baseType: !4, size: 64, align: 64, offset: 1216)
!116 = !DIDerivedType(tag: DW_TAG_member, name: "__pad2", scope: !71, file: !72, line: 303, baseType: !4, size: 64, align: 64, offset: 1280)
!117 = !DIDerivedType(tag: DW_TAG_member, name: "__pad3", scope: !71, file: !72, line: 304, baseType: !4, size: 64, align: 64, offset: 1344)
!118 = !DIDerivedType(tag: DW_TAG_member, name: "__pad4", scope: !71, file: !72, line: 305, baseType: !4, size: 64, align: 64, offset: 1408)
!119 = !DIDerivedType(tag: DW_TAG_member, name: "__pad5", scope: !71, file: !72, line: 306, baseType: !120, size: 64, align: 64, offset: 1472)
!120 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !121, line: 62, baseType: !122)
!121 = !DIFile(filename: "/opt/aclang/install/bin/../lib/clang/4.0.0/include/stddef.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!122 = !DIBasicType(name: "long unsigned int", size: 64, align: 64, encoding: DW_ATE_unsigned)
!123 = !DIDerivedType(tag: DW_TAG_member, name: "_mode", scope: !71, file: !72, line: 308, baseType: !29, size: 32, align: 32, offset: 1536)
!124 = !DIDerivedType(tag: DW_TAG_member, name: "_unused2", scope: !71, file: !72, line: 310, baseType: !125, size: 160, align: 8, offset: 1568)
!125 = !DICompositeType(tag: DW_TAG_array_type, baseType: !18, size: 160, align: 8, elements: !126)
!126 = !{!127}
!127 = !DISubrange(count: 20)
!128 = !DILocation(line: 23, column: 9, scope: !13)
!129 = !DILocation(line: 23, column: 25, scope: !13)
!130 = !DILocation(line: 23, column: 19, scope: !13)
!131 = !DILocation(line: 25, column: 7, scope: !132)
!132 = distinct !DILexicalBlock(scope: !13, file: !1, line: 25, column: 7)
!133 = !DILocation(line: 25, column: 15, scope: !132)
!134 = !DILocation(line: 25, column: 7, scope: !13)
!135 = !DILocation(line: 26, column: 13, scope: !136)
!136 = distinct !DILexicalBlock(scope: !132, file: !1, line: 25, column: 24)
!137 = !DILocation(line: 26, column: 5, scope: !136)
!138 = !DILocation(line: 27, column: 5, scope: !136)
!139 = !DILocalVariable(name: "abspotential", scope: !140, file: !1, line: 32, type: !5)
!140 = distinct !DILexicalBlock(scope: !13, file: !1, line: 31, column: 3)
!141 = !DILocation(line: 32, column: 12, scope: !140)
!142 = !DILocalVariable(name: "tmp", scope: !140, file: !1, line: 33, type: !6)
!143 = !DILocation(line: 33, column: 11, scope: !140)
!144 = !DILocalVariable(name: "i", scope: !140, file: !1, line: 34, type: !29)
!145 = !DILocation(line: 34, column: 9, scope: !140)
!146 = !DILocation(line: 36, column: 12, scope: !147)
!147 = distinct !DILexicalBlock(scope: !140, file: !1, line: 36, column: 5)
!148 = !DILocation(line: 36, column: 10, scope: !147)
!149 = !DILocation(line: 36, column: 17, scope: !150)
!150 = !DILexicalBlockFile(scope: !151, file: !1, discriminator: 1)
!151 = distinct !DILexicalBlock(scope: !147, file: !1, line: 36, column: 5)
!152 = !DILocation(line: 36, column: 21, scope: !150)
!153 = !DILocation(line: 36, column: 26, scope: !150)
!154 = !DILocation(line: 36, column: 24, scope: !150)
!155 = !DILocation(line: 36, column: 31, scope: !150)
!156 = !DILocation(line: 36, column: 29, scope: !150)
!157 = !DILocation(line: 36, column: 19, scope: !150)
!158 = !DILocation(line: 36, column: 5, scope: !150)
!159 = !DILocation(line: 37, column: 49, scope: !160)
!160 = distinct !DILexicalBlock(scope: !151, file: !1, line: 36, column: 40)
!161 = !DILocation(line: 37, column: 36, scope: !160)
!162 = !DILocation(line: 37, column: 28, scope: !160)
!163 = !DILocation(line: 37, column: 23, scope: !160)
!164 = !DILocation(line: 37, column: 20, scope: !160)
!165 = !DILocation(line: 38, column: 5, scope: !160)
!166 = !DILocation(line: 36, column: 36, scope: !167)
!167 = !DILexicalBlockFile(scope: !151, file: !1, discriminator: 2)
!168 = !DILocation(line: 36, column: 5, scope: !167)
!169 = distinct !{!169, !170}
!170 = !DILocation(line: 36, column: 5, scope: !140)
!171 = !DILocation(line: 40, column: 18, scope: !140)
!172 = !DILocation(line: 40, column: 11, scope: !140)
!173 = !DILocation(line: 40, column: 9, scope: !140)
!174 = !DILocation(line: 42, column: 12, scope: !140)
!175 = !DILocation(line: 42, column: 36, scope: !140)
!176 = !DILocation(line: 42, column: 5, scope: !140)
!177 = !DILocalVariable(name: "tmp", scope: !178, file: !1, line: 47, type: !7)
!178 = distinct !DILexicalBlock(scope: !13, file: !1, line: 46, column: 3)
!179 = !DILocation(line: 47, column: 14, scope: !178)
!180 = !DILocation(line: 49, column: 22, scope: !178)
!181 = !DILocation(line: 49, column: 31, scope: !178)
!182 = !DILocation(line: 49, column: 35, scope: !178)
!183 = !DILocation(line: 49, column: 40, scope: !178)
!184 = !DILocation(line: 49, column: 49, scope: !178)
!185 = !DILocation(line: 49, column: 53, scope: !178)
!186 = !DILocation(line: 49, column: 38, scope: !178)
!187 = !DILocation(line: 49, column: 9, scope: !178)
!188 = !DILocation(line: 50, column: 12, scope: !178)
!189 = !DILocation(line: 50, column: 39, scope: !178)
!190 = !DILocation(line: 50, column: 5, scope: !178)
!191 = !DILocalVariable(name: "plane_size", scope: !192, file: !1, line: 55, type: !29)
!192 = distinct !DILexicalBlock(scope: !13, file: !1, line: 54, column: 3)
!193 = !DILocation(line: 55, column: 9, scope: !192)
!194 = !DILocation(line: 55, column: 22, scope: !192)
!195 = !DILocation(line: 55, column: 27, scope: !192)
!196 = !DILocation(line: 55, column: 25, scope: !192)
!197 = !DILocation(line: 57, column: 12, scope: !192)
!198 = !DILocation(line: 57, column: 26, scope: !192)
!199 = !DILocation(line: 57, column: 53, scope: !192)
!200 = !DILocation(line: 57, column: 5, scope: !192)
!201 = !DILocation(line: 58, column: 12, scope: !192)
!202 = !DILocation(line: 58, column: 28, scope: !192)
!203 = !DILocation(line: 58, column: 31, scope: !192)
!204 = !DILocation(line: 58, column: 38, scope: !192)
!205 = !DILocation(line: 58, column: 36, scope: !192)
!206 = !DILocation(line: 58, column: 25, scope: !192)
!207 = !DILocation(line: 58, column: 50, scope: !192)
!208 = !DILocation(line: 59, column: 12, scope: !192)
!209 = !DILocation(line: 58, column: 5, scope: !192)
!210 = !DILocation(line: 63, column: 10, scope: !13)
!211 = !DILocation(line: 63, column: 3, scope: !13)
!212 = !DILocation(line: 64, column: 1, scope: !13)
