; ModuleID = 'CMakeFiles/cutcp.dir/main-inst.c'
source_filename = "CMakeFiles/cutcp.dir/main-inst.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.LatticeDim_t = type { i32, i32, i32, %struct.Vec3_t, float }
%struct.Vec3_t = type { float, float, float }
%struct.Lattice_t = type { %struct.LatticeDim_t, float* }
%struct.Atoms_t = type { %struct.Atom_t*, i32 }
%struct.Atom_t = type { float, float, float, float }
%struct.pb_Parameters = type { i8*, i8** }
%struct.pb_TimerSet = type { i32, %struct.pb_async_time_marker_list*, i64, i64, [8 x %struct.pb_Timer], [8 x %struct.pb_SubTimerList*] }
%struct.pb_async_time_marker_list = type { i8*, i32, i8*, %struct.pb_async_time_marker_list* }
%struct.pb_Timer = type { i32, i64, i64 }
%struct.pb_SubTimerList = type { %struct.pb_SubTimer*, %struct.pb_SubTimer* }
%struct.pb_SubTimer = type { i8*, %struct.pb_Timer, %struct.pb_SubTimer* }

@.str = private unnamed_addr constant [2 x i8] c"a\00", align 1
@.str.1 = private unnamed_addr constant [30 x i8] c"error appending to file %s..\0A\00", align 1
@.str.2 = private unnamed_addr constant [10 x i8] c"%d  %.3f\0A\00", align 1
@stderr = external global %struct._IO_FILE*, align 8
@.str.3 = private unnamed_addr constant [15 x i8] c"Out of memory\0A\00", align 1
@.str.4 = private unnamed_addr constant [26 x i8] c"Expecting one input file\0A\00", align 1
@.str.5 = private unnamed_addr constant [25 x i8] c"read_atom_file() failed\0A\00", align 1
@.str.6 = private unnamed_addr constant [30 x i8] c"read %d atoms from file '%s'\0A\00", align 1
@.str.7 = private unnamed_addr constant [22 x i8] c"extent of domain is:\0A\00", align 1
@.str.8 = private unnamed_addr constant [20 x i8] c"  minimum %g %g %g\0A\00", align 1
@.str.9 = private unnamed_addr constant [20 x i8] c"  maximum %g %g %g\0A\00", align 1
@.str.10 = private unnamed_addr constant [32 x i8] c"padding domain by %g Angstroms\0A\00", align 1
@.str.11 = private unnamed_addr constant [35 x i8] c"domain lengths are %g by %g by %g\0A\00", align 1
@.str.12 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.13 = private unnamed_addr constant [20 x i8] c"Computation failed\0A\00", align 1
@.str.14 = private unnamed_addr constant [44 x i8] c"remove_exclusions() failed for cpu lattice\0A\00", align 1
@0 = internal constant [33 x i8] c"CMakeFiles/cutcp.dir/main-inst.c\00"
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @__initCheckRuntime, i8* null }]
@llvm.global_dtors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @__termCheckRuntime, i8* null }]

; Function Attrs: nounwind uwtable
define i32 @appenddata(i8*, i32, double) #0 !dbg !49 {
  %4 = alloca i32, align 4
  %5 = alloca i8*, align 8
  %6 = alloca i32, align 4
  %7 = alloca double, align 8
  %8 = alloca %struct._IO_FILE*, align 8
  store i8* %0, i8** %5, align 8
  call void @llvm.dbg.declare(metadata i8** %5, metadata !57, metadata !58), !dbg !59
  store i32 %1, i32* %6, align 4
  call void @llvm.dbg.declare(metadata i32* %6, metadata !60, metadata !58), !dbg !61
  store double %2, double* %7, align 8
  call void @llvm.dbg.declare(metadata double* %7, metadata !62, metadata !58), !dbg !63
  call void @llvm.dbg.declare(metadata %struct._IO_FILE** %8, metadata !64, metadata !58), !dbg !125
  %9 = load i8*, i8** %5, align 8, !dbg !126
  %10 = call %struct._IO_FILE* @fopen(i8* %9, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0)), !dbg !127
  store %struct._IO_FILE* %10, %struct._IO_FILE** %8, align 8, !dbg !128
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** %8, align 8, !dbg !129
  %12 = icmp eq %struct._IO_FILE* %11, null, !dbg !131
  br i1 %12, label %13, label %16, !dbg !132

; <label>:13:                                     ; preds = %3
  %14 = load i8*, i8** %5, align 8, !dbg !133
  %15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.1, i32 0, i32 0), i8* %14), !dbg !135
  store i32 -1, i32* %4, align 4, !dbg !136
  br label %23, !dbg !136

; <label>:16:                                     ; preds = %3
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** %8, align 8, !dbg !137
  %18 = load i32, i32* %6, align 4, !dbg !138
  %19 = load double, double* %7, align 8, !dbg !139
  %20 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.2, i32 0, i32 0), i32 %18, double %19), !dbg !140
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** %8, align 8, !dbg !141
  %22 = call i32 @fclose(%struct._IO_FILE* %21), !dbg !142
  store i32 0, i32* %4, align 4, !dbg !143
  br label %23, !dbg !143

; <label>:23:                                     ; preds = %16, %13
  %24 = load i32, i32* %4, align 4, !dbg !144
  ret i32 %24, !dbg !144
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

declare %struct._IO_FILE* @fopen(i8*, i8*) #2

declare i32 @printf(i8*, ...) #2

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

declare i32 @fclose(%struct._IO_FILE*) #2

; Function Attrs: nounwind uwtable
define void @lattice_from_bounding_box(%struct.LatticeDim_t* noalias sret, <2 x float>, float, <2 x float>, float, float) #0 !dbg !145 {
  %7 = alloca %struct.Vec3_t, align 4
  %8 = alloca { <2 x float>, float }, align 4
  %9 = alloca %struct.Vec3_t, align 4
  %10 = alloca { <2 x float>, float }, align 4
  %11 = alloca float, align 4
  %12 = alloca %struct.LatticeDim_t, align 4
  %13 = getelementptr inbounds { <2 x float>, float }, { <2 x float>, float }* %8, i32 0, i32 0
  store <2 x float> %1, <2 x float>* %13, align 4
  %14 = getelementptr inbounds { <2 x float>, float }, { <2 x float>, float }* %8, i32 0, i32 1
  store float %2, float* %14, align 4
  %15 = bitcast %struct.Vec3_t* %7 to i8*
  %16 = bitcast { <2 x float>, float }* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %15, i8* %16, i64 12, i32 4, i1 false)
  %17 = getelementptr inbounds { <2 x float>, float }, { <2 x float>, float }* %10, i32 0, i32 0
  store <2 x float> %3, <2 x float>* %17, align 4
  %18 = getelementptr inbounds { <2 x float>, float }, { <2 x float>, float }* %10, i32 0, i32 1
  store float %4, float* %18, align 4
  %19 = bitcast %struct.Vec3_t* %9 to i8*
  %20 = bitcast { <2 x float>, float }* %10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %19, i8* %20, i64 12, i32 4, i1 false)
  call void @llvm.dbg.declare(metadata %struct.Vec3_t* %7, metadata !148, metadata !58), !dbg !149
  call void @llvm.dbg.declare(metadata %struct.Vec3_t* %9, metadata !150, metadata !58), !dbg !151
  store float %5, float* %11, align 4
  call void @llvm.dbg.declare(metadata float* %11, metadata !152, metadata !58), !dbg !153
  call void @llvm.dbg.declare(metadata %struct.LatticeDim_t* %12, metadata !154, metadata !58), !dbg !155
  %21 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 0, !dbg !156
  %22 = load float, float* %21, align 4, !dbg !156
  %23 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %7, i32 0, i32 0, !dbg !157
  %24 = load float, float* %23, align 4, !dbg !157
  %25 = fsub float %22, %24, !dbg !158
  %26 = load float, float* %11, align 4, !dbg !159
  %27 = fdiv float %25, %26, !dbg !160
  %28 = call float @floorf(float %27) #1, !dbg !161
  %29 = fptosi float %28 to i32, !dbg !162
  %30 = add nsw i32 %29, 1, !dbg !163
  %31 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %12, i32 0, i32 0, !dbg !164
  store i32 %30, i32* %31, align 4, !dbg !165
  %32 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 1, !dbg !166
  %33 = load float, float* %32, align 4, !dbg !166
  %34 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %7, i32 0, i32 1, !dbg !167
  %35 = load float, float* %34, align 4, !dbg !167
  %36 = fsub float %33, %35, !dbg !168
  %37 = load float, float* %11, align 4, !dbg !169
  %38 = fdiv float %36, %37, !dbg !170
  %39 = call float @floorf(float %38) #1, !dbg !171
  %40 = fptosi float %39 to i32, !dbg !172
  %41 = add nsw i32 %40, 1, !dbg !173
  %42 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %12, i32 0, i32 1, !dbg !174
  store i32 %41, i32* %42, align 4, !dbg !175
  %43 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 2, !dbg !176
  %44 = load float, float* %43, align 4, !dbg !176
  %45 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %7, i32 0, i32 2, !dbg !177
  %46 = load float, float* %45, align 4, !dbg !177
  %47 = fsub float %44, %46, !dbg !178
  %48 = load float, float* %11, align 4, !dbg !179
  %49 = fdiv float %47, %48, !dbg !180
  %50 = call float @floorf(float %49) #1, !dbg !181
  %51 = fptosi float %50 to i32, !dbg !182
  %52 = add nsw i32 %51, 1, !dbg !183
  %53 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %12, i32 0, i32 2, !dbg !184
  store i32 %52, i32* %53, align 4, !dbg !185
  %54 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %12, i32 0, i32 3, !dbg !186
  %55 = bitcast %struct.Vec3_t* %54 to i8*, !dbg !187
  %56 = bitcast %struct.Vec3_t* %7 to i8*, !dbg !187
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %55, i8* %56, i64 12, i32 4, i1 false), !dbg !187
  %57 = load float, float* %11, align 4, !dbg !188
  %58 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %12, i32 0, i32 4, !dbg !189
  store float %57, float* %58, align 4, !dbg !190
  %59 = bitcast %struct.LatticeDim_t* %0 to i8*, !dbg !191
  %60 = bitcast %struct.LatticeDim_t* %12 to i8*, !dbg !191
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %59, i8* %60, i64 28, i32 4, i1 false), !dbg !191
  ret void, !dbg !192
}

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #3

; Function Attrs: nounwind readnone
declare float @floorf(float) #4

; Function Attrs: nounwind uwtable
define %struct.Lattice_t* @create_lattice(%struct.LatticeDim_t* byval align 8) #0 !dbg !193 {
  %2 = alloca i32, align 4
  %3 = alloca %struct.Lattice_t*, align 8
  call void @llvm.dbg.declare(metadata %struct.LatticeDim_t* %0, metadata !196, metadata !58), !dbg !197
  call void @llvm.dbg.declare(metadata i32* %2, metadata !198, metadata !58), !dbg !199
  call void @llvm.dbg.declare(metadata %struct.Lattice_t** %3, metadata !200, metadata !58), !dbg !201
  %4 = call noalias i8* @malloc(i64 40) #8, !dbg !202
  %5 = bitcast i8* %4 to %struct.Lattice_t*, !dbg !203
  store %struct.Lattice_t* %5, %struct.Lattice_t** %3, align 8, !dbg !201
  %6 = load %struct.Lattice_t*, %struct.Lattice_t** %3, align 8, !dbg !204
  %7 = icmp eq %struct.Lattice_t* %6, null, !dbg !206
  br i1 %7, label %8, label %11, !dbg !207

; <label>:8:                                      ; preds = %1
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !208
  %10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.3, i32 0, i32 0)), !dbg !210
  call void @exit(i32 1) #9, !dbg !211
  unreachable, !dbg !211

; <label>:11:                                     ; preds = %1
  %12 = load %struct.Lattice_t*, %struct.Lattice_t** %3, align 8, !dbg !212
  %13 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %12, i32 0, i32 0, !dbg !213
  %14 = bitcast %struct.LatticeDim_t* %13 to i8*, !dbg !214
  %15 = bitcast %struct.LatticeDim_t* %0 to i8*, !dbg !214
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %14, i8* %15, i64 28, i32 8, i1 false), !dbg !214
  %16 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %0, i32 0, i32 0, !dbg !215
  %17 = load i32, i32* %16, align 8, !dbg !215
  %18 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %0, i32 0, i32 1, !dbg !216
  %19 = load i32, i32* %18, align 4, !dbg !216
  %20 = mul nsw i32 %17, %19, !dbg !217
  %21 = getelementptr inbounds %struct.LatticeDim_t, %struct.LatticeDim_t* %0, i32 0, i32 2, !dbg !218
  %22 = load i32, i32* %21, align 8, !dbg !218
  %23 = mul nsw i32 %20, %22, !dbg !219
  %24 = add nsw i32 %23, 7, !dbg !220
  %25 = and i32 %24, -8, !dbg !221
  store i32 %25, i32* %2, align 4, !dbg !222
  %26 = load i32, i32* %2, align 4, !dbg !223
  %27 = sext i32 %26 to i64, !dbg !223
  %28 = call noalias i8* @calloc(i64 %27, i64 4) #8, !dbg !224
  %29 = bitcast i8* %28 to float*, !dbg !225
  %30 = load %struct.Lattice_t*, %struct.Lattice_t** %3, align 8, !dbg !226
  %31 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %30, i32 0, i32 1, !dbg !227
  store float* %29, float** %31, align 8, !dbg !228
  %32 = load %struct.Lattice_t*, %struct.Lattice_t** %3, align 8, !dbg !229
  %33 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %32, i32 0, i32 1, !dbg !231
  %34 = load float*, float** %33, align 8, !dbg !231
  %35 = icmp eq float* %34, null, !dbg !232
  br i1 %35, label %36, label %39, !dbg !233

; <label>:36:                                     ; preds = %11
  %37 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !234
  %38 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %37, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.3, i32 0, i32 0)), !dbg !236
  call void @exit(i32 1) #9, !dbg !237
  unreachable, !dbg !237

; <label>:39:                                     ; preds = %11
  %40 = load %struct.Lattice_t*, %struct.Lattice_t** %3, align 8, !dbg !238
  ret %struct.Lattice_t* %40, !dbg !239
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #5

; Function Attrs: noreturn nounwind
declare void @exit(i32) #6

; Function Attrs: nounwind
declare noalias i8* @calloc(i64, i64) #5

; Function Attrs: nounwind uwtable
define void @destroy_lattice(%struct.Lattice_t*) #0 !dbg !240 {
  %2 = alloca %struct.Lattice_t*, align 8
  store %struct.Lattice_t* %0, %struct.Lattice_t** %2, align 8
  call void @llvm.dbg.declare(metadata %struct.Lattice_t** %2, metadata !243, metadata !58), !dbg !244
  %3 = load %struct.Lattice_t*, %struct.Lattice_t** %2, align 8, !dbg !245
  %4 = icmp ne %struct.Lattice_t* %3, null, !dbg !245
  br i1 %4, label %5, label %12, !dbg !247

; <label>:5:                                      ; preds = %1
  %6 = load %struct.Lattice_t*, %struct.Lattice_t** %2, align 8, !dbg !248
  %7 = getelementptr inbounds %struct.Lattice_t, %struct.Lattice_t* %6, i32 0, i32 1, !dbg !250
  %8 = load float*, float** %7, align 8, !dbg !250
  %9 = bitcast float* %8 to i8*, !dbg !248
  call void @free(i8* %9) #8, !dbg !251
  %10 = load %struct.Lattice_t*, %struct.Lattice_t** %2, align 8, !dbg !252
  %11 = bitcast %struct.Lattice_t* %10 to i8*, !dbg !252
  call void @free(i8* %11) #8, !dbg !253
  br label %12, !dbg !254

; <label>:12:                                     ; preds = %5, %1
  ret void, !dbg !255
}

; Function Attrs: nounwind
declare void @free(i8*) #5

; Function Attrs: nounwind uwtable
define i32 @main(i32, i8**) #0 !dbg !256 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i8**, align 8
  %6 = alloca %struct.Atoms_t*, align 8
  %7 = alloca %struct.LatticeDim_t, align 8
  %8 = alloca %struct.Lattice_t*, align 8
  %9 = alloca %struct.Vec3_t, align 4
  %10 = alloca %struct.Vec3_t, align 4
  %11 = alloca %struct.Vec3_t, align 4
  %12 = alloca %struct.Vec3_t, align 4
  %13 = alloca float, align 4
  %14 = alloca float, align 4
  %15 = alloca float, align 4
  %16 = alloca float, align 4
  %17 = alloca i32, align 4
  %18 = alloca %struct.pb_Parameters*, align 8
  %19 = alloca %struct.pb_TimerSet, align 8
  %20 = alloca i8*, align 8
  %21 = alloca %struct.Vec3_t, align 4
  %22 = alloca %struct.Vec3_t, align 4
  %23 = alloca %struct.LatticeDim_t, align 4
  %24 = alloca { <2 x float>, float }, align 4
  %25 = alloca { <2 x float>, float }, align 4
  store i32 0, i32* %3, align 4
  store i32 %0, i32* %4, align 4
  call void @llvm.dbg.declare(metadata i32* %4, metadata !260, metadata !58), !dbg !261
  store i8** %1, i8*** %5, align 8
  call void @llvm.dbg.declare(metadata i8*** %5, metadata !262, metadata !58), !dbg !263
  call void @llvm.dbg.declare(metadata %struct.Atoms_t** %6, metadata !264, metadata !58), !dbg !279
  call void @llvm.dbg.declare(metadata %struct.LatticeDim_t* %7, metadata !280, metadata !58), !dbg !281
  call void @llvm.dbg.declare(metadata %struct.Lattice_t** %8, metadata !282, metadata !58), !dbg !283
  call void @llvm.dbg.declare(metadata %struct.Vec3_t* %9, metadata !284, metadata !58), !dbg !285
  call void @llvm.dbg.declare(metadata %struct.Vec3_t* %10, metadata !286, metadata !58), !dbg !287
  call void @llvm.dbg.declare(metadata %struct.Vec3_t* %11, metadata !288, metadata !58), !dbg !289
  call void @llvm.dbg.declare(metadata %struct.Vec3_t* %12, metadata !290, metadata !58), !dbg !291
  call void @llvm.dbg.declare(metadata float* %13, metadata !292, metadata !58), !dbg !293
  store float 5.000000e-01, float* %13, align 4, !dbg !293
  call void @llvm.dbg.declare(metadata float* %14, metadata !294, metadata !58), !dbg !295
  store float 1.200000e+01, float* %14, align 4, !dbg !295
  call void @llvm.dbg.declare(metadata float* %15, metadata !296, metadata !58), !dbg !297
  store float 1.000000e+00, float* %15, align 4, !dbg !297
  call void @llvm.dbg.declare(metadata float* %16, metadata !298, metadata !58), !dbg !299
  store float 5.000000e-01, float* %16, align 4, !dbg !299
  call void @llvm.dbg.declare(metadata i32* %17, metadata !300, metadata !58), !dbg !301
  call void @llvm.dbg.declare(metadata %struct.pb_Parameters** %18, metadata !302, metadata !58), !dbg !308
  call void @llvm.dbg.declare(metadata %struct.pb_TimerSet* %19, metadata !309, metadata !58), !dbg !347
  %26 = load i8**, i8*** %5, align 8, !dbg !348
  %27 = call %struct.pb_Parameters* @pb_ReadParameters(i32* %4, i8** %26), !dbg !349
  store %struct.pb_Parameters* %27, %struct.pb_Parameters** %18, align 8, !dbg !350
  %28 = load %struct.pb_Parameters*, %struct.pb_Parameters** %18, align 8, !dbg !351
  %29 = icmp eq %struct.pb_Parameters* %28, null, !dbg !353
  br i1 %29, label %30, label %31, !dbg !354

; <label>:30:                                     ; preds = %2
  call void @exit(i32 1) #9, !dbg !355
  unreachable, !dbg !355

; <label>:31:                                     ; preds = %2
  %32 = load %struct.pb_Parameters*, %struct.pb_Parameters** %18, align 8, !dbg !357
  %33 = call i32 @pb_Parameters_CountInputs(%struct.pb_Parameters* %32), !dbg !359
  %34 = icmp ne i32 %33, 1, !dbg !360
  br i1 %34, label %35, label %38, !dbg !361

; <label>:35:                                     ; preds = %31
  %36 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !362
  %37 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %36, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.4, i32 0, i32 0)), !dbg !364
  call void @exit(i32 1) #9, !dbg !365
  unreachable, !dbg !365

; <label>:38:                                     ; preds = %31
  call void @pb_InitializeTimerSet(%struct.pb_TimerSet* %19), !dbg !366
  call void @pb_SwitchToTimer(%struct.pb_TimerSet* %19, i32 1), !dbg !367
  call void @llvm.dbg.declare(metadata i8** %20, metadata !368, metadata !58), !dbg !370
  %39 = load %struct.pb_Parameters*, %struct.pb_Parameters** %18, align 8, !dbg !371
  %40 = getelementptr inbounds %struct.pb_Parameters, %struct.pb_Parameters* %39, i32 0, i32 1, !dbg !372
  %41 = load i8**, i8*** %40, align 8, !dbg !372
  %42 = getelementptr inbounds i8*, i8** %41, i64 0, !dbg !371
  %43 = load i8*, i8** %42, align 8, !dbg !371
  store i8* %43, i8** %20, align 8, !dbg !370
  %44 = load i8*, i8** %20, align 8, !dbg !373
  %45 = call %struct.Atoms_t* @read_atom_file(i8* %44), !dbg !375
  store %struct.Atoms_t* %45, %struct.Atoms_t** %6, align 8, !dbg !376
  %46 = icmp ne %struct.Atoms_t* %45, null, !dbg !376
  br i1 %46, label %50, label %47, !dbg !377

; <label>:47:                                     ; preds = %38
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !378
  %49 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %48, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.5, i32 0, i32 0)), !dbg !380
  call void @exit(i32 1) #9, !dbg !381
  unreachable, !dbg !381

; <label>:50:                                     ; preds = %38
  %51 = load %struct.Atoms_t*, %struct.Atoms_t** %6, align 8, !dbg !382
  %52 = getelementptr inbounds %struct.Atoms_t, %struct.Atoms_t* %51, i32 0, i32 1, !dbg !383
  %53 = load i32, i32* %52, align 8, !dbg !383
  %54 = load i8*, i8** %20, align 8, !dbg !384
  %55 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.6, i32 0, i32 0), i32 %53, i8* %54), !dbg !385
  call void @pb_SwitchToTimer(%struct.pb_TimerSet* %19, i32 6), !dbg !386
  %56 = load %struct.Atoms_t*, %struct.Atoms_t** %6, align 8, !dbg !387
  call void @get_atom_extent(%struct.Vec3_t* %9, %struct.Vec3_t* %10, %struct.Atoms_t* %56), !dbg !388
  %57 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.7, i32 0, i32 0)), !dbg !389
  %58 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 0, !dbg !390
  %59 = load float, float* %58, align 4, !dbg !390
  %60 = fpext float %59 to double, !dbg !391
  %61 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 1, !dbg !392
  %62 = load float, float* %61, align 4, !dbg !392
  %63 = fpext float %62 to double, !dbg !393
  %64 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 2, !dbg !394
  %65 = load float, float* %64, align 4, !dbg !394
  %66 = fpext float %65 to double, !dbg !395
  %67 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.8, i32 0, i32 0), double %60, double %63, double %66), !dbg !396
  %68 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 0, !dbg !397
  %69 = load float, float* %68, align 4, !dbg !397
  %70 = fpext float %69 to double, !dbg !398
  %71 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 1, !dbg !399
  %72 = load float, float* %71, align 4, !dbg !399
  %73 = fpext float %72 to double, !dbg !400
  %74 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 2, !dbg !401
  %75 = load float, float* %74, align 4, !dbg !401
  %76 = fpext float %75 to double, !dbg !402
  %77 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.9, i32 0, i32 0), double %70, double %73, double %76), !dbg !403
  %78 = load float, float* %16, align 4, !dbg !404
  %79 = fpext float %78 to double, !dbg !404
  %80 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.10, i32 0, i32 0), double %79), !dbg !405
  %81 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %21, i32 0, i32 0, !dbg !406
  %82 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 0, !dbg !407
  %83 = load float, float* %82, align 4, !dbg !407
  %84 = load float, float* %16, align 4, !dbg !408
  %85 = fsub float %83, %84, !dbg !409
  store float %85, float* %81, align 4, !dbg !406
  %86 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %21, i32 0, i32 1, !dbg !406
  %87 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 1, !dbg !410
  %88 = load float, float* %87, align 4, !dbg !410
  %89 = load float, float* %16, align 4, !dbg !411
  %90 = fsub float %88, %89, !dbg !412
  store float %90, float* %86, align 4, !dbg !406
  %91 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %21, i32 0, i32 2, !dbg !406
  %92 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %9, i32 0, i32 2, !dbg !413
  %93 = load float, float* %92, align 4, !dbg !413
  %94 = load float, float* %16, align 4, !dbg !414
  %95 = fsub float %93, %94, !dbg !415
  store float %95, float* %91, align 4, !dbg !406
  %96 = bitcast %struct.Vec3_t* %11 to i8*, !dbg !416
  %97 = bitcast %struct.Vec3_t* %21 to i8*, !dbg !416
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %96, i8* %97, i64 12, i32 4, i1 false), !dbg !416
  %98 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %22, i32 0, i32 0, !dbg !417
  %99 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 0, !dbg !418
  %100 = load float, float* %99, align 4, !dbg !418
  %101 = load float, float* %16, align 4, !dbg !419
  %102 = fadd float %100, %101, !dbg !420
  store float %102, float* %98, align 4, !dbg !417
  %103 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %22, i32 0, i32 1, !dbg !417
  %104 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 1, !dbg !421
  %105 = load float, float* %104, align 4, !dbg !421
  %106 = load float, float* %16, align 4, !dbg !422
  %107 = fadd float %105, %106, !dbg !423
  store float %107, float* %103, align 4, !dbg !417
  %108 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %22, i32 0, i32 2, !dbg !417
  %109 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %10, i32 0, i32 2, !dbg !424
  %110 = load float, float* %109, align 4, !dbg !424
  %111 = load float, float* %16, align 4, !dbg !425
  %112 = fadd float %110, %111, !dbg !426
  store float %112, float* %108, align 4, !dbg !417
  %113 = bitcast %struct.Vec3_t* %12 to i8*, !dbg !427
  %114 = bitcast %struct.Vec3_t* %22 to i8*, !dbg !427
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %113, i8* %114, i64 12, i32 4, i1 false), !dbg !427
  %115 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %12, i32 0, i32 0, !dbg !428
  %116 = load float, float* %115, align 4, !dbg !428
  %117 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %11, i32 0, i32 0, !dbg !429
  %118 = load float, float* %117, align 4, !dbg !429
  %119 = fsub float %116, %118, !dbg !430
  %120 = fpext float %119 to double, !dbg !431
  %121 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %12, i32 0, i32 1, !dbg !432
  %122 = load float, float* %121, align 4, !dbg !432
  %123 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %11, i32 0, i32 1, !dbg !433
  %124 = load float, float* %123, align 4, !dbg !433
  %125 = fsub float %122, %124, !dbg !434
  %126 = fpext float %125 to double, !dbg !435
  %127 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %12, i32 0, i32 2, !dbg !436
  %128 = load float, float* %127, align 4, !dbg !436
  %129 = getelementptr inbounds %struct.Vec3_t, %struct.Vec3_t* %11, i32 0, i32 2, !dbg !437
  %130 = load float, float* %129, align 4, !dbg !437
  %131 = fsub float %128, %130, !dbg !438
  %132 = fpext float %131 to double, !dbg !439
  %133 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.11, i32 0, i32 0), double %120, double %126, double %132), !dbg !440
  %134 = load float, float* %13, align 4, !dbg !441
  %135 = bitcast { <2 x float>, float }* %24 to i8*, !dbg !442
  %136 = bitcast %struct.Vec3_t* %11 to i8*, !dbg !442
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %135, i8* %136, i64 12, i32 4, i1 false), !dbg !442
  %137 = getelementptr inbounds { <2 x float>, float }, { <2 x float>, float }* %24, i32 0, i32 0, !dbg !442
  %138 = load <2 x float>, <2 x float>* %137, align 4, !dbg !442
  %139 = getelementptr inbounds { <2 x float>, float }, { <2 x float>, float }* %24, i32 0, i32 1, !dbg !442
  %140 = load float, float* %139, align 4, !dbg !442
  %141 = bitcast { <2 x float>, float }* %25 to i8*, !dbg !442
  %142 = bitcast %struct.Vec3_t* %12 to i8*, !dbg !442
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %141, i8* %142, i64 12, i32 4, i1 false), !dbg !443
  %143 = getelementptr inbounds { <2 x float>, float }, { <2 x float>, float }* %25, i32 0, i32 0, !dbg !442
  %144 = load <2 x float>, <2 x float>* %143, align 4, !dbg !442
  %145 = getelementptr inbounds { <2 x float>, float }, { <2 x float>, float }* %25, i32 0, i32 1, !dbg !442
  %146 = load float, float* %145, align 4, !dbg !442
  call void @lattice_from_bounding_box(%struct.LatticeDim_t* sret %23, <2 x float> %138, float %140, <2 x float> %144, float %146, float %134), !dbg !445
  %147 = bitcast %struct.LatticeDim_t* %7 to i8*, !dbg !442
  %148 = bitcast %struct.LatticeDim_t* %23 to i8*, !dbg !442
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %147, i8* %148, i64 28, i32 4, i1 false), !dbg !447
  %149 = call %struct.Lattice_t* @create_lattice(%struct.LatticeDim_t* byval align 8 %7), !dbg !449
  store %struct.Lattice_t* %149, %struct.Lattice_t** %8, align 8, !dbg !450
  %150 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.12, i32 0, i32 0)), !dbg !451
  %151 = load %struct.Lattice_t*, %struct.Lattice_t** %8, align 8, !dbg !452
  %152 = load float, float* %14, align 4, !dbg !454
  %153 = load %struct.Atoms_t*, %struct.Atoms_t** %6, align 8, !dbg !455
  %154 = call i32 @cpu_compute_cutoff_potential_lattice(%struct.Lattice_t* %151, float %152, %struct.Atoms_t* %153), !dbg !456
  %155 = icmp ne i32 %154, 0, !dbg !456
  br i1 %155, label %156, label %159, !dbg !457

; <label>:156:                                    ; preds = %50
  %157 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !458
  %158 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %157, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.13, i32 0, i32 0)), !dbg !460
  call void @exit(i32 1) #9, !dbg !461
  unreachable, !dbg !461

; <label>:159:                                    ; preds = %50
  %160 = load %struct.Lattice_t*, %struct.Lattice_t** %8, align 8, !dbg !462
  %161 = load float, float* %15, align 4, !dbg !464
  %162 = load %struct.Atoms_t*, %struct.Atoms_t** %6, align 8, !dbg !465
  %163 = call i32 @remove_exclusions(%struct.Lattice_t* %160, float %161, %struct.Atoms_t* %162), !dbg !466
  %164 = icmp ne i32 %163, 0, !dbg !466
  br i1 %164, label %165, label %168, !dbg !467

; <label>:165:                                    ; preds = %159
  %166 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !dbg !468
  %167 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %166, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.14, i32 0, i32 0)), !dbg !470
  call void @exit(i32 1) #9, !dbg !471
  unreachable, !dbg !471

; <label>:168:                                    ; preds = %159
  call void @pb_SwitchToTimer(%struct.pb_TimerSet* %19, i32 1), !dbg !472
  %169 = load %struct.pb_Parameters*, %struct.pb_Parameters** %18, align 8, !dbg !473
  %170 = getelementptr inbounds %struct.pb_Parameters, %struct.pb_Parameters* %169, i32 0, i32 0, !dbg !475
  %171 = load i8*, i8** %170, align 8, !dbg !475
  %172 = icmp ne i8* %171, null, !dbg !473
  br i1 %172, label %173, label %178, !dbg !476

; <label>:173:                                    ; preds = %168
  %174 = load %struct.pb_Parameters*, %struct.pb_Parameters** %18, align 8, !dbg !477
  %175 = getelementptr inbounds %struct.pb_Parameters, %struct.pb_Parameters* %174, i32 0, i32 0, !dbg !479
  %176 = load i8*, i8** %175, align 8, !dbg !479
  %177 = load %struct.Lattice_t*, %struct.Lattice_t** %8, align 8, !dbg !480
  call void @write_lattice_summary(i8* %176, %struct.Lattice_t* %177), !dbg !481
  br label %178, !dbg !482

; <label>:178:                                    ; preds = %173, %168
  call void @pb_SwitchToTimer(%struct.pb_TimerSet* %19, i32 6), !dbg !483
  %179 = load %struct.Lattice_t*, %struct.Lattice_t** %8, align 8, !dbg !484
  call void @destroy_lattice(%struct.Lattice_t* %179), !dbg !485
  %180 = load %struct.Atoms_t*, %struct.Atoms_t** %6, align 8, !dbg !486
  call void @free_atom(%struct.Atoms_t* %180), !dbg !487
  call void @pb_SwitchToTimer(%struct.pb_TimerSet* %19, i32 0), !dbg !488
  call void @pb_PrintTimerSet(%struct.pb_TimerSet* %19), !dbg !489
  %181 = load %struct.pb_Parameters*, %struct.pb_Parameters** %18, align 8, !dbg !490
  call void @pb_FreeParameters(%struct.pb_Parameters* %181), !dbg !491
  ret i32 0, !dbg !492
}

declare %struct.pb_Parameters* @pb_ReadParameters(i32*, i8**) #2

declare i32 @pb_Parameters_CountInputs(%struct.pb_Parameters*) #2

declare void @pb_InitializeTimerSet(%struct.pb_TimerSet*) #2

declare void @pb_SwitchToTimer(%struct.pb_TimerSet*, i32) #2

declare %struct.Atoms_t* @read_atom_file(i8*) #2

declare void @get_atom_extent(%struct.Vec3_t*, %struct.Vec3_t*, %struct.Atoms_t*) #2

declare i32 @cpu_compute_cutoff_potential_lattice(%struct.Lattice_t*, float, %struct.Atoms_t*) #2

declare i32 @remove_exclusions(%struct.Lattice_t*, float, %struct.Atoms_t*) #2

declare void @write_lattice_summary(i8*, %struct.Lattice_t*) #2

declare void @free_atom(%struct.Atoms_t*) #2

declare void @pb_PrintTimerSet(%struct.pb_TimerSet*) #2

declare void @pb_FreeParameters(%struct.pb_Parameters*) #2

; Function Attrs: noinline
declare void @__check_dependence(i8*, i32, i8, i8*) #7

; Function Attrs: noinline
declare void @__initCheckRuntime() #7

; Function Attrs: noinline
declare void @__termCheckRuntime() #7

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind }
attributes #4 = { nounwind readnone "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noreturn nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { noinline }
attributes #8 = { nounwind }
attributes #9 = { noreturn nounwind }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!46, !47}
!llvm.ident = !{!48}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 4.0.0 (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause-clang.git 587724e9c6678af22b0b9d7649d8b84fb39aaaee) (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause.git 6f76a77790273ec9529638e6dc9391b206557c59)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2, retainedTypes: !19)
!1 = !DIFile(filename: "CMakeFiles/cutcp.dir/main-inst.c", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!2 = !{!3, !15}
!3 = !DICompositeType(tag: DW_TAG_enumeration_type, name: "pb_TimerID", file: !4, line: 93, size: 32, align: 32, elements: !5)
!4 = !DIFile(filename: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/../../common/include/parboil.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!5 = !{!6, !7, !8, !9, !10, !11, !12, !13, !14}
!6 = !DIEnumerator(name: "pb_TimerID_NONE", value: 0)
!7 = !DIEnumerator(name: "pb_TimerID_IO", value: 1)
!8 = !DIEnumerator(name: "pb_TimerID_KERNEL", value: 2)
!9 = !DIEnumerator(name: "pb_TimerID_COPY", value: 3)
!10 = !DIEnumerator(name: "pb_TimerID_DRIVER", value: 4)
!11 = !DIEnumerator(name: "pb_TimerID_COPY_ASYNC", value: 5)
!12 = !DIEnumerator(name: "pb_TimerID_COMPUTE", value: 6)
!13 = !DIEnumerator(name: "pb_TimerID_OVERLAP", value: 7)
!14 = !DIEnumerator(name: "pb_TimerID_LAST", value: 8)
!15 = !DICompositeType(tag: DW_TAG_enumeration_type, name: "pb_TimerState", file: !4, line: 53, size: 32, align: 32, elements: !16)
!16 = !{!17, !18}
!17 = !DIEnumerator(name: "pb_Timer_STOPPED", value: 0)
!18 = !DIEnumerator(name: "pb_Timer_RUNNING", value: 1)
!19 = !{!20, !21, !22, !45}
!20 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64, align: 64)
!21 = !DIBasicType(name: "int", size: 32, align: 32, encoding: DW_ATE_signed)
!22 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !23, size: 64, align: 64)
!23 = !DIDerivedType(tag: DW_TAG_typedef, name: "Lattice", file: !24, line: 40, baseType: !25)
!24 = !DIFile(filename: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/cutoff.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!25 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Lattice_t", file: !24, line: 37, size: 320, align: 64, elements: !26)
!26 = !{!27, !44}
!27 = !DIDerivedType(tag: DW_TAG_member, name: "dim", scope: !25, file: !24, line: 38, baseType: !28, size: 224, align: 32)
!28 = !DIDerivedType(tag: DW_TAG_typedef, name: "LatticeDim", file: !24, line: 32, baseType: !29)
!29 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "LatticeDim_t", file: !24, line: 23, size: 224, align: 32, elements: !30)
!30 = !{!31, !32, !33, !34, !43}
!31 = !DIDerivedType(tag: DW_TAG_member, name: "nx", scope: !29, file: !24, line: 25, baseType: !21, size: 32, align: 32)
!32 = !DIDerivedType(tag: DW_TAG_member, name: "ny", scope: !29, file: !24, line: 25, baseType: !21, size: 32, align: 32, offset: 32)
!33 = !DIDerivedType(tag: DW_TAG_member, name: "nz", scope: !29, file: !24, line: 25, baseType: !21, size: 32, align: 32, offset: 64)
!34 = !DIDerivedType(tag: DW_TAG_member, name: "lo", scope: !29, file: !24, line: 28, baseType: !35, size: 96, align: 32, offset: 96)
!35 = !DIDerivedType(tag: DW_TAG_typedef, name: "Vec3", file: !36, line: 23, baseType: !37)
!36 = !DIFile(filename: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/atom.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!37 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Vec3_t", file: !36, line: 23, size: 96, align: 32, elements: !38)
!38 = !{!39, !41, !42}
!39 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !37, file: !36, line: 23, baseType: !40, size: 32, align: 32)
!40 = !DIBasicType(name: "float", size: 32, align: 32, encoding: DW_ATE_float)
!41 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !37, file: !36, line: 23, baseType: !40, size: 32, align: 32, offset: 32)
!42 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !37, file: !36, line: 23, baseType: !40, size: 32, align: 32, offset: 64)
!43 = !DIDerivedType(tag: DW_TAG_member, name: "h", scope: !29, file: !24, line: 31, baseType: !40, size: 32, align: 32, offset: 192)
!44 = !DIDerivedType(tag: DW_TAG_member, name: "lattice", scope: !25, file: !24, line: 39, baseType: !45, size: 64, align: 64, offset: 256)
!45 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !40, size: 64, align: 64)
!46 = !{i32 2, !"Dwarf Version", i32 4}
!47 = !{i32 2, !"Debug Info Version", i32 3}
!48 = !{!"clang version 4.0.0 (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause-clang.git 587724e9c6678af22b0b9d7649d8b84fb39aaaee) (http://gitlab.lsc.ic.unicamp.br/luis.mattos/DoAcrossClause.git 6f76a77790273ec9529638e6dc9391b206557c59)"}
!49 = distinct !DISubprogram(name: "appenddata", scope: !1, file: !1, line: 26, type: !50, isLocal: false, isDefinition: true, scopeLine: 26, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !56)
!50 = !DISubroutineType(types: !51)
!51 = !{!21, !52, !21, !55}
!52 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !53, size: 64, align: 64)
!53 = !DIDerivedType(tag: DW_TAG_const_type, baseType: !54)
!54 = !DIBasicType(name: "char", size: 8, align: 8, encoding: DW_ATE_signed_char)
!55 = !DIBasicType(name: "double", size: 64, align: 64, encoding: DW_ATE_float)
!56 = !{}
!57 = !DILocalVariable(name: "filename", arg: 1, scope: !49, file: !1, line: 26, type: !52)
!58 = !DIExpression()
!59 = !DILocation(line: 26, column: 28, scope: !49)
!60 = !DILocalVariable(name: "size", arg: 2, scope: !49, file: !1, line: 26, type: !21)
!61 = !DILocation(line: 26, column: 42, scope: !49)
!62 = !DILocalVariable(name: "time", arg: 3, scope: !49, file: !1, line: 26, type: !55)
!63 = !DILocation(line: 26, column: 55, scope: !49)
!64 = !DILocalVariable(name: "fp", scope: !49, file: !1, line: 27, type: !65)
!65 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !66, size: 64, align: 64)
!66 = !DIDerivedType(tag: DW_TAG_typedef, name: "FILE", file: !67, line: 48, baseType: !68)
!67 = !DIFile(filename: "/usr/include/stdio.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!68 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_FILE", file: !69, line: 245, size: 1728, align: 64, elements: !70)
!69 = !DIFile(filename: "/usr/include/libio.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!70 = !{!71, !72, !74, !75, !76, !77, !78, !79, !80, !81, !82, !83, !84, !92, !93, !94, !95, !99, !101, !103, !107, !110, !112, !113, !114, !115, !116, !120, !121}
!71 = !DIDerivedType(tag: DW_TAG_member, name: "_flags", scope: !68, file: !69, line: 246, baseType: !21, size: 32, align: 32)
!72 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_ptr", scope: !68, file: !69, line: 251, baseType: !73, size: 64, align: 64, offset: 64)
!73 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !54, size: 64, align: 64)
!74 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_end", scope: !68, file: !69, line: 252, baseType: !73, size: 64, align: 64, offset: 128)
!75 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_read_base", scope: !68, file: !69, line: 253, baseType: !73, size: 64, align: 64, offset: 192)
!76 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_base", scope: !68, file: !69, line: 254, baseType: !73, size: 64, align: 64, offset: 256)
!77 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_ptr", scope: !68, file: !69, line: 255, baseType: !73, size: 64, align: 64, offset: 320)
!78 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_write_end", scope: !68, file: !69, line: 256, baseType: !73, size: 64, align: 64, offset: 384)
!79 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_buf_base", scope: !68, file: !69, line: 257, baseType: !73, size: 64, align: 64, offset: 448)
!80 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_buf_end", scope: !68, file: !69, line: 258, baseType: !73, size: 64, align: 64, offset: 512)
!81 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_save_base", scope: !68, file: !69, line: 260, baseType: !73, size: 64, align: 64, offset: 576)
!82 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_backup_base", scope: !68, file: !69, line: 261, baseType: !73, size: 64, align: 64, offset: 640)
!83 = !DIDerivedType(tag: DW_TAG_member, name: "_IO_save_end", scope: !68, file: !69, line: 262, baseType: !73, size: 64, align: 64, offset: 704)
!84 = !DIDerivedType(tag: DW_TAG_member, name: "_markers", scope: !68, file: !69, line: 264, baseType: !85, size: 64, align: 64, offset: 768)
!85 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !86, size: 64, align: 64)
!86 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "_IO_marker", file: !69, line: 160, size: 192, align: 64, elements: !87)
!87 = !{!88, !89, !91}
!88 = !DIDerivedType(tag: DW_TAG_member, name: "_next", scope: !86, file: !69, line: 161, baseType: !85, size: 64, align: 64)
!89 = !DIDerivedType(tag: DW_TAG_member, name: "_sbuf", scope: !86, file: !69, line: 162, baseType: !90, size: 64, align: 64, offset: 64)
!90 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !68, size: 64, align: 64)
!91 = !DIDerivedType(tag: DW_TAG_member, name: "_pos", scope: !86, file: !69, line: 166, baseType: !21, size: 32, align: 32, offset: 128)
!92 = !DIDerivedType(tag: DW_TAG_member, name: "_chain", scope: !68, file: !69, line: 266, baseType: !90, size: 64, align: 64, offset: 832)
!93 = !DIDerivedType(tag: DW_TAG_member, name: "_fileno", scope: !68, file: !69, line: 268, baseType: !21, size: 32, align: 32, offset: 896)
!94 = !DIDerivedType(tag: DW_TAG_member, name: "_flags2", scope: !68, file: !69, line: 272, baseType: !21, size: 32, align: 32, offset: 928)
!95 = !DIDerivedType(tag: DW_TAG_member, name: "_old_offset", scope: !68, file: !69, line: 274, baseType: !96, size: 64, align: 64, offset: 960)
!96 = !DIDerivedType(tag: DW_TAG_typedef, name: "__off_t", file: !97, line: 131, baseType: !98)
!97 = !DIFile(filename: "/usr/include/bits/types.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!98 = !DIBasicType(name: "long int", size: 64, align: 64, encoding: DW_ATE_signed)
!99 = !DIDerivedType(tag: DW_TAG_member, name: "_cur_column", scope: !68, file: !69, line: 278, baseType: !100, size: 16, align: 16, offset: 1024)
!100 = !DIBasicType(name: "unsigned short", size: 16, align: 16, encoding: DW_ATE_unsigned)
!101 = !DIDerivedType(tag: DW_TAG_member, name: "_vtable_offset", scope: !68, file: !69, line: 279, baseType: !102, size: 8, align: 8, offset: 1040)
!102 = !DIBasicType(name: "signed char", size: 8, align: 8, encoding: DW_ATE_signed_char)
!103 = !DIDerivedType(tag: DW_TAG_member, name: "_shortbuf", scope: !68, file: !69, line: 280, baseType: !104, size: 8, align: 8, offset: 1048)
!104 = !DICompositeType(tag: DW_TAG_array_type, baseType: !54, size: 8, align: 8, elements: !105)
!105 = !{!106}
!106 = !DISubrange(count: 1)
!107 = !DIDerivedType(tag: DW_TAG_member, name: "_lock", scope: !68, file: !69, line: 284, baseType: !108, size: 64, align: 64, offset: 1088)
!108 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !109, size: 64, align: 64)
!109 = !DIDerivedType(tag: DW_TAG_typedef, name: "_IO_lock_t", file: !69, line: 154, baseType: null)
!110 = !DIDerivedType(tag: DW_TAG_member, name: "_offset", scope: !68, file: !69, line: 293, baseType: !111, size: 64, align: 64, offset: 1152)
!111 = !DIDerivedType(tag: DW_TAG_typedef, name: "__off64_t", file: !97, line: 132, baseType: !98)
!112 = !DIDerivedType(tag: DW_TAG_member, name: "__pad1", scope: !68, file: !69, line: 302, baseType: !20, size: 64, align: 64, offset: 1216)
!113 = !DIDerivedType(tag: DW_TAG_member, name: "__pad2", scope: !68, file: !69, line: 303, baseType: !20, size: 64, align: 64, offset: 1280)
!114 = !DIDerivedType(tag: DW_TAG_member, name: "__pad3", scope: !68, file: !69, line: 304, baseType: !20, size: 64, align: 64, offset: 1344)
!115 = !DIDerivedType(tag: DW_TAG_member, name: "__pad4", scope: !68, file: !69, line: 305, baseType: !20, size: 64, align: 64, offset: 1408)
!116 = !DIDerivedType(tag: DW_TAG_member, name: "__pad5", scope: !68, file: !69, line: 306, baseType: !117, size: 64, align: 64, offset: 1472)
!117 = !DIDerivedType(tag: DW_TAG_typedef, name: "size_t", file: !118, line: 62, baseType: !119)
!118 = !DIFile(filename: "/opt/aclang/install/bin/../lib/clang/4.0.0/include/stddef.h", directory: "/home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build")
!119 = !DIBasicType(name: "long unsigned int", size: 64, align: 64, encoding: DW_ATE_unsigned)
!120 = !DIDerivedType(tag: DW_TAG_member, name: "_mode", scope: !68, file: !69, line: 308, baseType: !21, size: 32, align: 32, offset: 1536)
!121 = !DIDerivedType(tag: DW_TAG_member, name: "_unused2", scope: !68, file: !69, line: 310, baseType: !122, size: 160, align: 8, offset: 1568)
!122 = !DICompositeType(tag: DW_TAG_array_type, baseType: !54, size: 160, align: 8, elements: !123)
!123 = !{!124}
!124 = !DISubrange(count: 20)
!125 = !DILocation(line: 27, column: 9, scope: !49)
!126 = !DILocation(line: 28, column: 14, scope: !49)
!127 = !DILocation(line: 28, column: 8, scope: !49)
!128 = !DILocation(line: 28, column: 6, scope: !49)
!129 = !DILocation(line: 29, column: 7, scope: !130)
!130 = distinct !DILexicalBlock(scope: !49, file: !1, line: 29, column: 7)
!131 = !DILocation(line: 29, column: 10, scope: !130)
!132 = !DILocation(line: 29, column: 7, scope: !49)
!133 = !DILocation(line: 30, column: 46, scope: !134)
!134 = distinct !DILexicalBlock(scope: !130, file: !1, line: 29, column: 19)
!135 = !DILocation(line: 30, column: 5, scope: !134)
!136 = !DILocation(line: 31, column: 5, scope: !134)
!137 = !DILocation(line: 33, column: 11, scope: !49)
!138 = !DILocation(line: 33, column: 29, scope: !49)
!139 = !DILocation(line: 33, column: 35, scope: !49)
!140 = !DILocation(line: 33, column: 3, scope: !49)
!141 = !DILocation(line: 34, column: 10, scope: !49)
!142 = !DILocation(line: 34, column: 3, scope: !49)
!143 = !DILocation(line: 35, column: 3, scope: !49)
!144 = !DILocation(line: 36, column: 1, scope: !49)
!145 = distinct !DISubprogram(name: "lattice_from_bounding_box", scope: !1, file: !1, line: 38, type: !146, isLocal: false, isDefinition: true, scopeLine: 38, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !56)
!146 = !DISubroutineType(types: !147)
!147 = !{!28, !35, !35, !40}
!148 = !DILocalVariable(name: "lo", arg: 1, scope: !145, file: !1, line: 38, type: !35)
!149 = !DILocation(line: 38, column: 43, scope: !145)
!150 = !DILocalVariable(name: "hi", arg: 2, scope: !145, file: !1, line: 38, type: !35)
!151 = !DILocation(line: 38, column: 52, scope: !145)
!152 = !DILocalVariable(name: "h", arg: 3, scope: !145, file: !1, line: 38, type: !40)
!153 = !DILocation(line: 38, column: 62, scope: !145)
!154 = !DILocalVariable(name: "ret", scope: !145, file: !1, line: 39, type: !28)
!155 = !DILocation(line: 39, column: 14, scope: !145)
!156 = !DILocation(line: 41, column: 28, scope: !145)
!157 = !DILocation(line: 41, column: 35, scope: !145)
!158 = !DILocation(line: 41, column: 30, scope: !145)
!159 = !DILocation(line: 41, column: 40, scope: !145)
!160 = !DILocation(line: 41, column: 38, scope: !145)
!161 = !DILocation(line: 41, column: 17, scope: !145)
!162 = !DILocation(line: 41, column: 12, scope: !145)
!163 = !DILocation(line: 41, column: 43, scope: !145)
!164 = !DILocation(line: 41, column: 7, scope: !145)
!165 = !DILocation(line: 41, column: 10, scope: !145)
!166 = !DILocation(line: 42, column: 28, scope: !145)
!167 = !DILocation(line: 42, column: 35, scope: !145)
!168 = !DILocation(line: 42, column: 30, scope: !145)
!169 = !DILocation(line: 42, column: 40, scope: !145)
!170 = !DILocation(line: 42, column: 38, scope: !145)
!171 = !DILocation(line: 42, column: 17, scope: !145)
!172 = !DILocation(line: 42, column: 12, scope: !145)
!173 = !DILocation(line: 42, column: 43, scope: !145)
!174 = !DILocation(line: 42, column: 7, scope: !145)
!175 = !DILocation(line: 42, column: 10, scope: !145)
!176 = !DILocation(line: 43, column: 28, scope: !145)
!177 = !DILocation(line: 43, column: 35, scope: !145)
!178 = !DILocation(line: 43, column: 30, scope: !145)
!179 = !DILocation(line: 43, column: 40, scope: !145)
!180 = !DILocation(line: 43, column: 38, scope: !145)
!181 = !DILocation(line: 43, column: 17, scope: !145)
!182 = !DILocation(line: 43, column: 12, scope: !145)
!183 = !DILocation(line: 43, column: 43, scope: !145)
!184 = !DILocation(line: 43, column: 7, scope: !145)
!185 = !DILocation(line: 43, column: 10, scope: !145)
!186 = !DILocation(line: 44, column: 7, scope: !145)
!187 = !DILocation(line: 44, column: 12, scope: !145)
!188 = !DILocation(line: 45, column: 11, scope: !145)
!189 = !DILocation(line: 45, column: 7, scope: !145)
!190 = !DILocation(line: 45, column: 9, scope: !145)
!191 = !DILocation(line: 47, column: 10, scope: !145)
!192 = !DILocation(line: 47, column: 3, scope: !145)
!193 = distinct !DISubprogram(name: "create_lattice", scope: !1, file: !1, line: 50, type: !194, isLocal: false, isDefinition: true, scopeLine: 50, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !56)
!194 = !DISubroutineType(types: !195)
!195 = !{!22, !28}
!196 = !DILocalVariable(name: "dim", arg: 1, scope: !193, file: !1, line: 50, type: !28)
!197 = !DILocation(line: 50, column: 36, scope: !193)
!198 = !DILocalVariable(name: "size", scope: !193, file: !1, line: 51, type: !21)
!199 = !DILocation(line: 51, column: 7, scope: !193)
!200 = !DILocalVariable(name: "lat", scope: !193, file: !1, line: 52, type: !22)
!201 = !DILocation(line: 52, column: 12, scope: !193)
!202 = !DILocation(line: 52, column: 29, scope: !193)
!203 = !DILocation(line: 52, column: 18, scope: !193)
!204 = !DILocation(line: 54, column: 7, scope: !205)
!205 = distinct !DILexicalBlock(scope: !193, file: !1, line: 54, column: 7)
!206 = !DILocation(line: 54, column: 11, scope: !205)
!207 = !DILocation(line: 54, column: 7, scope: !193)
!208 = !DILocation(line: 55, column: 13, scope: !209)
!209 = distinct !DILexicalBlock(scope: !205, file: !1, line: 54, column: 20)
!210 = !DILocation(line: 55, column: 5, scope: !209)
!211 = !DILocation(line: 56, column: 5, scope: !209)
!212 = !DILocation(line: 59, column: 3, scope: !193)
!213 = !DILocation(line: 59, column: 8, scope: !193)
!214 = !DILocation(line: 59, column: 14, scope: !193)
!215 = !DILocation(line: 62, column: 16, scope: !193)
!216 = !DILocation(line: 62, column: 25, scope: !193)
!217 = !DILocation(line: 62, column: 19, scope: !193)
!218 = !DILocation(line: 62, column: 34, scope: !193)
!219 = !DILocation(line: 62, column: 28, scope: !193)
!220 = !DILocation(line: 62, column: 38, scope: !193)
!221 = !DILocation(line: 62, column: 43, scope: !193)
!222 = !DILocation(line: 62, column: 8, scope: !193)
!223 = !DILocation(line: 63, column: 34, scope: !193)
!224 = !DILocation(line: 63, column: 27, scope: !193)
!225 = !DILocation(line: 63, column: 18, scope: !193)
!226 = !DILocation(line: 63, column: 3, scope: !193)
!227 = !DILocation(line: 63, column: 8, scope: !193)
!228 = !DILocation(line: 63, column: 16, scope: !193)
!229 = !DILocation(line: 65, column: 7, scope: !230)
!230 = distinct !DILexicalBlock(scope: !193, file: !1, line: 65, column: 7)
!231 = !DILocation(line: 65, column: 12, scope: !230)
!232 = !DILocation(line: 65, column: 20, scope: !230)
!233 = !DILocation(line: 65, column: 7, scope: !193)
!234 = !DILocation(line: 66, column: 13, scope: !235)
!235 = distinct !DILexicalBlock(scope: !230, file: !1, line: 65, column: 29)
!236 = !DILocation(line: 66, column: 5, scope: !235)
!237 = !DILocation(line: 67, column: 5, scope: !235)
!238 = !DILocation(line: 70, column: 10, scope: !193)
!239 = !DILocation(line: 70, column: 3, scope: !193)
!240 = distinct !DISubprogram(name: "destroy_lattice", scope: !1, file: !1, line: 73, type: !241, isLocal: false, isDefinition: true, scopeLine: 73, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !56)
!241 = !DISubroutineType(types: !242)
!242 = !{null, !22}
!243 = !DILocalVariable(name: "lat", arg: 1, scope: !240, file: !1, line: 73, type: !22)
!244 = !DILocation(line: 73, column: 31, scope: !240)
!245 = !DILocation(line: 74, column: 7, scope: !246)
!246 = distinct !DILexicalBlock(scope: !240, file: !1, line: 74, column: 7)
!247 = !DILocation(line: 74, column: 7, scope: !240)
!248 = !DILocation(line: 75, column: 10, scope: !249)
!249 = distinct !DILexicalBlock(scope: !246, file: !1, line: 74, column: 12)
!250 = !DILocation(line: 75, column: 15, scope: !249)
!251 = !DILocation(line: 75, column: 5, scope: !249)
!252 = !DILocation(line: 76, column: 10, scope: !249)
!253 = !DILocation(line: 76, column: 5, scope: !249)
!254 = !DILocation(line: 77, column: 3, scope: !249)
!255 = !DILocation(line: 78, column: 1, scope: !240)
!256 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 80, type: !257, isLocal: false, isDefinition: true, scopeLine: 80, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !56)
!257 = !DISubroutineType(types: !258)
!258 = !{!21, !21, !259}
!259 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !73, size: 64, align: 64)
!260 = !DILocalVariable(name: "argc", arg: 1, scope: !256, file: !1, line: 80, type: !21)
!261 = !DILocation(line: 80, column: 14, scope: !256)
!262 = !DILocalVariable(name: "argv", arg: 2, scope: !256, file: !1, line: 80, type: !259)
!263 = !DILocation(line: 80, column: 26, scope: !256)
!264 = !DILocalVariable(name: "atom", scope: !256, file: !1, line: 81, type: !265)
!265 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !266, size: 64, align: 64)
!266 = !DIDerivedType(tag: DW_TAG_typedef, name: "Atoms", file: !36, line: 21, baseType: !267)
!267 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Atoms_t", file: !36, line: 18, size: 128, align: 64, elements: !268)
!268 = !{!269, !278}
!269 = !DIDerivedType(tag: DW_TAG_member, name: "atoms", scope: !267, file: !36, line: 19, baseType: !270, size: 64, align: 64)
!270 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !271, size: 64, align: 64)
!271 = !DIDerivedType(tag: DW_TAG_typedef, name: "Atom", file: !36, line: 16, baseType: !272)
!272 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "Atom_t", file: !36, line: 16, size: 128, align: 32, elements: !273)
!273 = !{!274, !275, !276, !277}
!274 = !DIDerivedType(tag: DW_TAG_member, name: "x", scope: !272, file: !36, line: 16, baseType: !40, size: 32, align: 32)
!275 = !DIDerivedType(tag: DW_TAG_member, name: "y", scope: !272, file: !36, line: 16, baseType: !40, size: 32, align: 32, offset: 32)
!276 = !DIDerivedType(tag: DW_TAG_member, name: "z", scope: !272, file: !36, line: 16, baseType: !40, size: 32, align: 32, offset: 64)
!277 = !DIDerivedType(tag: DW_TAG_member, name: "q", scope: !272, file: !36, line: 16, baseType: !40, size: 32, align: 32, offset: 96)
!278 = !DIDerivedType(tag: DW_TAG_member, name: "size", scope: !267, file: !36, line: 20, baseType: !21, size: 32, align: 32, offset: 64)
!279 = !DILocation(line: 81, column: 10, scope: !256)
!280 = !DILocalVariable(name: "lattice_dim", scope: !256, file: !1, line: 83, type: !28)
!281 = !DILocation(line: 83, column: 14, scope: !256)
!282 = !DILocalVariable(name: "cpu_lattice", scope: !256, file: !1, line: 84, type: !22)
!283 = !DILocation(line: 84, column: 12, scope: !256)
!284 = !DILocalVariable(name: "min_ext", scope: !256, file: !1, line: 85, type: !35)
!285 = !DILocation(line: 85, column: 8, scope: !256)
!286 = !DILocalVariable(name: "max_ext", scope: !256, file: !1, line: 85, type: !35)
!287 = !DILocation(line: 85, column: 17, scope: !256)
!288 = !DILocalVariable(name: "lo", scope: !256, file: !1, line: 86, type: !35)
!289 = !DILocation(line: 86, column: 8, scope: !256)
!290 = !DILocalVariable(name: "hi", scope: !256, file: !1, line: 86, type: !35)
!291 = !DILocation(line: 86, column: 12, scope: !256)
!292 = !DILocalVariable(name: "h", scope: !256, file: !1, line: 88, type: !40)
!293 = !DILocation(line: 88, column: 9, scope: !256)
!294 = !DILocalVariable(name: "cutoff", scope: !256, file: !1, line: 89, type: !40)
!295 = !DILocation(line: 89, column: 9, scope: !256)
!296 = !DILocalVariable(name: "exclcutoff", scope: !256, file: !1, line: 90, type: !40)
!297 = !DILocation(line: 90, column: 9, scope: !256)
!298 = !DILocalVariable(name: "padding", scope: !256, file: !1, line: 91, type: !40)
!299 = !DILocation(line: 91, column: 9, scope: !256)
!300 = !DILocalVariable(name: "n", scope: !256, file: !1, line: 93, type: !21)
!301 = !DILocation(line: 93, column: 7, scope: !256)
!302 = !DILocalVariable(name: "parameters", scope: !256, file: !1, line: 95, type: !303)
!303 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !304, size: 64, align: 64)
!304 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "pb_Parameters", file: !4, line: 14, size: 128, align: 64, elements: !305)
!305 = !{!306, !307}
!306 = !DIDerivedType(tag: DW_TAG_member, name: "outFile", scope: !304, file: !4, line: 15, baseType: !73, size: 64, align: 64)
!307 = !DIDerivedType(tag: DW_TAG_member, name: "inpFiles", scope: !304, file: !4, line: 18, baseType: !259, size: 64, align: 64, offset: 64)
!308 = !DILocation(line: 95, column: 25, scope: !256)
!309 = !DILocalVariable(name: "timers", scope: !256, file: !1, line: 96, type: !310)
!310 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "pb_TimerSet", file: !4, line: 136, size: 2304, align: 64, elements: !311)
!311 = !{!312, !313, !321, !324, !325, !334}
!312 = !DIDerivedType(tag: DW_TAG_member, name: "current", scope: !310, file: !4, line: 137, baseType: !3, size: 32, align: 32)
!313 = !DIDerivedType(tag: DW_TAG_member, name: "async_markers", scope: !310, file: !4, line: 138, baseType: !314, size: 64, align: 64, offset: 64)
!314 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !315, size: 64, align: 64)
!315 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "pb_async_time_marker_list", file: !4, line: 115, size: 256, align: 64, elements: !316)
!316 = !{!317, !318, !319, !320}
!317 = !DIDerivedType(tag: DW_TAG_member, name: "label", scope: !315, file: !4, line: 116, baseType: !73, size: 64, align: 64)
!318 = !DIDerivedType(tag: DW_TAG_member, name: "timerID", scope: !315, file: !4, line: 117, baseType: !3, size: 32, align: 32, offset: 64)
!319 = !DIDerivedType(tag: DW_TAG_member, name: "marker", scope: !315, file: !4, line: 119, baseType: !20, size: 64, align: 64, offset: 128)
!320 = !DIDerivedType(tag: DW_TAG_member, name: "next", scope: !315, file: !4, line: 121, baseType: !314, size: 64, align: 64, offset: 192)
!321 = !DIDerivedType(tag: DW_TAG_member, name: "async_begin", scope: !310, file: !4, line: 139, baseType: !322, size: 64, align: 64, offset: 128)
!322 = !DIDerivedType(tag: DW_TAG_typedef, name: "pb_Timestamp", file: !4, line: 48, baseType: !323)
!323 = !DIBasicType(name: "long long unsigned int", size: 64, align: 64, encoding: DW_ATE_unsigned)
!324 = !DIDerivedType(tag: DW_TAG_member, name: "wall_begin", scope: !310, file: !4, line: 140, baseType: !322, size: 64, align: 64, offset: 192)
!325 = !DIDerivedType(tag: DW_TAG_member, name: "timers", scope: !310, file: !4, line: 141, baseType: !326, size: 1536, align: 64, offset: 256)
!326 = !DICompositeType(tag: DW_TAG_array_type, baseType: !327, size: 1536, align: 64, elements: !332)
!327 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "pb_Timer", file: !4, line: 58, size: 192, align: 64, elements: !328)
!328 = !{!329, !330, !331}
!329 = !DIDerivedType(tag: DW_TAG_member, name: "state", scope: !327, file: !4, line: 59, baseType: !15, size: 32, align: 32)
!330 = !DIDerivedType(tag: DW_TAG_member, name: "elapsed", scope: !327, file: !4, line: 60, baseType: !322, size: 64, align: 64, offset: 64)
!331 = !DIDerivedType(tag: DW_TAG_member, name: "init", scope: !327, file: !4, line: 61, baseType: !322, size: 64, align: 64, offset: 128)
!332 = !{!333}
!333 = !DISubrange(count: 8)
!334 = !DIDerivedType(tag: DW_TAG_member, name: "sub_timer_list", scope: !310, file: !4, line: 142, baseType: !335, size: 512, align: 64, offset: 1792)
!335 = !DICompositeType(tag: DW_TAG_array_type, baseType: !336, size: 512, align: 64, elements: !332)
!336 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !337, size: 64, align: 64)
!337 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "pb_SubTimerList", file: !4, line: 130, size: 128, align: 64, elements: !338)
!338 = !{!339, !346}
!339 = !DIDerivedType(tag: DW_TAG_member, name: "current", scope: !337, file: !4, line: 131, baseType: !340, size: 64, align: 64)
!340 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !341, size: 64, align: 64)
!341 = distinct !DICompositeType(tag: DW_TAG_structure_type, name: "pb_SubTimer", file: !4, line: 124, size: 320, align: 64, elements: !342)
!342 = !{!343, !344, !345}
!343 = !DIDerivedType(tag: DW_TAG_member, name: "label", scope: !341, file: !4, line: 125, baseType: !73, size: 64, align: 64)
!344 = !DIDerivedType(tag: DW_TAG_member, name: "timer", scope: !341, file: !4, line: 126, baseType: !327, size: 192, align: 64, offset: 64)
!345 = !DIDerivedType(tag: DW_TAG_member, name: "next", scope: !341, file: !4, line: 127, baseType: !340, size: 64, align: 64, offset: 256)
!346 = !DIDerivedType(tag: DW_TAG_member, name: "subtimer_list", scope: !337, file: !4, line: 132, baseType: !340, size: 64, align: 64, offset: 64)
!347 = !DILocation(line: 96, column: 22, scope: !256)
!348 = !DILocation(line: 99, column: 41, scope: !256)
!349 = !DILocation(line: 99, column: 16, scope: !256)
!350 = !DILocation(line: 99, column: 14, scope: !256)
!351 = !DILocation(line: 100, column: 7, scope: !352)
!352 = distinct !DILexicalBlock(scope: !256, file: !1, line: 100, column: 7)
!353 = !DILocation(line: 100, column: 18, scope: !352)
!354 = !DILocation(line: 100, column: 7, scope: !256)
!355 = !DILocation(line: 101, column: 5, scope: !356)
!356 = distinct !DILexicalBlock(scope: !352, file: !1, line: 100, column: 27)
!357 = !DILocation(line: 105, column: 33, scope: !358)
!358 = distinct !DILexicalBlock(scope: !256, file: !1, line: 105, column: 7)
!359 = !DILocation(line: 105, column: 7, scope: !358)
!360 = !DILocation(line: 105, column: 45, scope: !358)
!361 = !DILocation(line: 105, column: 7, scope: !256)
!362 = !DILocation(line: 106, column: 13, scope: !363)
!363 = distinct !DILexicalBlock(scope: !358, file: !1, line: 105, column: 51)
!364 = !DILocation(line: 106, column: 5, scope: !363)
!365 = !DILocation(line: 107, column: 5, scope: !363)
!366 = !DILocation(line: 110, column: 3, scope: !256)
!367 = !DILocation(line: 111, column: 3, scope: !256)
!368 = !DILocalVariable(name: "pqrfilename", scope: !369, file: !1, line: 114, type: !52)
!369 = distinct !DILexicalBlock(scope: !256, file: !1, line: 113, column: 3)
!370 = !DILocation(line: 114, column: 17, scope: !369)
!371 = !DILocation(line: 114, column: 31, scope: !369)
!372 = !DILocation(line: 114, column: 43, scope: !369)
!373 = !DILocation(line: 116, column: 33, scope: !374)
!374 = distinct !DILexicalBlock(scope: !369, file: !1, line: 116, column: 9)
!375 = !DILocation(line: 116, column: 18, scope: !374)
!376 = !DILocation(line: 116, column: 16, scope: !374)
!377 = !DILocation(line: 116, column: 9, scope: !369)
!378 = !DILocation(line: 117, column: 15, scope: !379)
!379 = distinct !DILexicalBlock(scope: !374, file: !1, line: 116, column: 48)
!380 = !DILocation(line: 117, column: 7, scope: !379)
!381 = !DILocation(line: 118, column: 7, scope: !379)
!382 = !DILocation(line: 120, column: 46, scope: !369)
!383 = !DILocation(line: 120, column: 52, scope: !369)
!384 = !DILocation(line: 120, column: 58, scope: !369)
!385 = !DILocation(line: 120, column: 5, scope: !369)
!386 = !DILocation(line: 122, column: 3, scope: !256)
!387 = !DILocation(line: 125, column: 39, scope: !256)
!388 = !DILocation(line: 125, column: 3, scope: !256)
!389 = !DILocation(line: 126, column: 3, scope: !256)
!390 = !DILocation(line: 127, column: 42, scope: !256)
!391 = !DILocation(line: 127, column: 34, scope: !256)
!392 = !DILocation(line: 127, column: 53, scope: !256)
!393 = !DILocation(line: 127, column: 45, scope: !256)
!394 = !DILocation(line: 127, column: 64, scope: !256)
!395 = !DILocation(line: 127, column: 56, scope: !256)
!396 = !DILocation(line: 127, column: 3, scope: !256)
!397 = !DILocation(line: 128, column: 42, scope: !256)
!398 = !DILocation(line: 128, column: 34, scope: !256)
!399 = !DILocation(line: 128, column: 53, scope: !256)
!400 = !DILocation(line: 128, column: 45, scope: !256)
!401 = !DILocation(line: 128, column: 64, scope: !256)
!402 = !DILocation(line: 128, column: 56, scope: !256)
!403 = !DILocation(line: 128, column: 3, scope: !256)
!404 = !DILocation(line: 130, column: 46, scope: !256)
!405 = !DILocation(line: 130, column: 3, scope: !256)
!406 = !DILocation(line: 131, column: 14, scope: !256)
!407 = !DILocation(line: 131, column: 23, scope: !256)
!408 = !DILocation(line: 131, column: 27, scope: !256)
!409 = !DILocation(line: 131, column: 25, scope: !256)
!410 = !DILocation(line: 131, column: 44, scope: !256)
!411 = !DILocation(line: 131, column: 48, scope: !256)
!412 = !DILocation(line: 131, column: 46, scope: !256)
!413 = !DILocation(line: 131, column: 65, scope: !256)
!414 = !DILocation(line: 131, column: 69, scope: !256)
!415 = !DILocation(line: 131, column: 67, scope: !256)
!416 = !DILocation(line: 131, column: 8, scope: !256)
!417 = !DILocation(line: 132, column: 14, scope: !256)
!418 = !DILocation(line: 132, column: 23, scope: !256)
!419 = !DILocation(line: 132, column: 27, scope: !256)
!420 = !DILocation(line: 132, column: 25, scope: !256)
!421 = !DILocation(line: 132, column: 44, scope: !256)
!422 = !DILocation(line: 132, column: 48, scope: !256)
!423 = !DILocation(line: 132, column: 46, scope: !256)
!424 = !DILocation(line: 132, column: 65, scope: !256)
!425 = !DILocation(line: 132, column: 69, scope: !256)
!426 = !DILocation(line: 132, column: 67, scope: !256)
!427 = !DILocation(line: 132, column: 8, scope: !256)
!428 = !DILocation(line: 133, column: 52, scope: !256)
!429 = !DILocation(line: 133, column: 59, scope: !256)
!430 = !DILocation(line: 133, column: 54, scope: !256)
!431 = !DILocation(line: 133, column: 49, scope: !256)
!432 = !DILocation(line: 133, column: 65, scope: !256)
!433 = !DILocation(line: 133, column: 72, scope: !256)
!434 = !DILocation(line: 133, column: 67, scope: !256)
!435 = !DILocation(line: 133, column: 62, scope: !256)
!436 = !DILocation(line: 134, column: 13, scope: !256)
!437 = !DILocation(line: 134, column: 20, scope: !256)
!438 = !DILocation(line: 134, column: 15, scope: !256)
!439 = !DILocation(line: 134, column: 10, scope: !256)
!440 = !DILocation(line: 133, column: 3, scope: !256)
!441 = !DILocation(line: 136, column: 51, scope: !256)
!442 = !DILocation(line: 136, column: 17, scope: !256)
!443 = !DILocation(line: 136, column: 17, scope: !444)
!444 = !DILexicalBlockFile(scope: !256, file: !1, discriminator: 1)
!445 = !DILocation(line: 136, column: 17, scope: !446)
!446 = !DILexicalBlockFile(scope: !256, file: !1, discriminator: 2)
!447 = !DILocation(line: 136, column: 17, scope: !448)
!448 = !DILexicalBlockFile(scope: !256, file: !1, discriminator: 3)
!449 = !DILocation(line: 137, column: 17, scope: !256)
!450 = !DILocation(line: 137, column: 15, scope: !256)
!451 = !DILocation(line: 138, column: 3, scope: !256)
!452 = !DILocation(line: 143, column: 44, scope: !453)
!453 = distinct !DILexicalBlock(scope: !256, file: !1, line: 143, column: 7)
!454 = !DILocation(line: 143, column: 57, scope: !453)
!455 = !DILocation(line: 143, column: 65, scope: !453)
!456 = !DILocation(line: 143, column: 7, scope: !453)
!457 = !DILocation(line: 143, column: 7, scope: !256)
!458 = !DILocation(line: 144, column: 13, scope: !459)
!459 = distinct !DILexicalBlock(scope: !453, file: !1, line: 143, column: 72)
!460 = !DILocation(line: 144, column: 5, scope: !459)
!461 = !DILocation(line: 145, column: 5, scope: !459)
!462 = !DILocation(line: 152, column: 25, scope: !463)
!463 = distinct !DILexicalBlock(scope: !256, file: !1, line: 152, column: 7)
!464 = !DILocation(line: 152, column: 38, scope: !463)
!465 = !DILocation(line: 152, column: 50, scope: !463)
!466 = !DILocation(line: 152, column: 7, scope: !463)
!467 = !DILocation(line: 152, column: 7, scope: !256)
!468 = !DILocation(line: 153, column: 13, scope: !469)
!469 = distinct !DILexicalBlock(scope: !463, file: !1, line: 152, column: 57)
!470 = !DILocation(line: 153, column: 5, scope: !469)
!471 = !DILocation(line: 154, column: 5, scope: !469)
!472 = !DILocation(line: 158, column: 3, scope: !256)
!473 = !DILocation(line: 159, column: 7, scope: !474)
!474 = distinct !DILexicalBlock(scope: !256, file: !1, line: 159, column: 7)
!475 = !DILocation(line: 159, column: 19, scope: !474)
!476 = !DILocation(line: 159, column: 7, scope: !256)
!477 = !DILocation(line: 160, column: 27, scope: !478)
!478 = distinct !DILexicalBlock(scope: !474, file: !1, line: 159, column: 28)
!479 = !DILocation(line: 160, column: 39, scope: !478)
!480 = !DILocation(line: 160, column: 48, scope: !478)
!481 = !DILocation(line: 160, column: 5, scope: !478)
!482 = !DILocation(line: 161, column: 3, scope: !478)
!483 = !DILocation(line: 162, column: 3, scope: !256)
!484 = !DILocation(line: 165, column: 19, scope: !256)
!485 = !DILocation(line: 165, column: 3, scope: !256)
!486 = !DILocation(line: 166, column: 13, scope: !256)
!487 = !DILocation(line: 166, column: 3, scope: !256)
!488 = !DILocation(line: 168, column: 3, scope: !256)
!489 = !DILocation(line: 169, column: 3, scope: !256)
!490 = !DILocation(line: 170, column: 21, scope: !256)
!491 = !DILocation(line: 170, column: 3, scope: !256)
!492 = !DILocation(line: 172, column: 3, scope: !256)
