# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.4

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build

# Utility rule file for cutcp-check.

# Include the progress variables for this target.
include CMakeFiles/cutcp-check.dir/progress.make

CMakeFiles/cutcp-check:
	dp-checker -p . /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/cutcpu.c /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/excl.c /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/main.c /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/output.c /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/readatom.c /home/msc2018-ceb/ra212069/T11/parboil/common/src/parboil.c
	clang -g -fopenmp -O3 /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build/CMakeFiles/cutcp.dir/cutcpu.c.ll /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build/CMakeFiles/cutcp.dir/excl.c.ll /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build/CMakeFiles/cutcp.dir/main.c.ll /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build/CMakeFiles/cutcp.dir/output.c.ll /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build/CMakeFiles/cutcp.dir/readatom.c.ll /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build/CMakeFiles/cutcp.dir//home/msc2018-ceb/ra212069/T11/parboil/common/src/parboil.c.ll -o cutcp-check -lm -lgomp -lcheck-rt

cutcp-check: CMakeFiles/cutcp-check
cutcp-check: CMakeFiles/cutcp-check.dir/build.make

.PHONY : cutcp-check

# Rule to build all files generated by this target.
CMakeFiles/cutcp-check.dir/build: cutcp-check

.PHONY : CMakeFiles/cutcp-check.dir/build

CMakeFiles/cutcp-check.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/cutcp-check.dir/cmake_clean.cmake
.PHONY : CMakeFiles/cutcp-check.dir/clean

CMakeFiles/cutcp-check.dir/depend:
	cd /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build /home/msc2018-ceb/ra212069/T11/parboil/benchmarks/cutcp/build/CMakeFiles/cutcp-check.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/cutcp-check.dir/depend

