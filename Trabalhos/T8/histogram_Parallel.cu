#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

#define TILE 16
#define COMMENT "Histogram_GPU"
#define RGB_COMPONENT_COLOR 255

typedef struct {
	unsigned char red, green, blue;
} PPMPixel;

typedef struct {
	int x, y;
	PPMPixel *data;
} PPMImage;

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}


static PPMImage *readPPM(const char *filename) {
	char buff[16];
	PPMImage *img;
	FILE *fp;
	int c, rgb_comp_color;
	fp = fopen(filename, "rb");
	if (!fp) {
		fprintf(stderr, "Unable to open file '%s'\n", filename);
		exit(1);
	}

	if (!fgets(buff, sizeof(buff), fp)) {
		perror(filename);
		exit(1);
	}

	if (buff[0] != 'P' || buff[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		exit(1);
	}

	img = (PPMImage *) malloc(sizeof(PPMImage));
	if (!img) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}

	c = getc(fp);
	while (c == '#') {
		while (getc(fp) != '\n')
			;
		c = getc(fp);
	}

	ungetc(c, fp);
	if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
		exit(1);
	}

	if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
				filename);
		exit(1);
	}

	if (rgb_comp_color != RGB_COMPONENT_COLOR) {
		fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
		exit(1);
	}

	while (fgetc(fp) != '\n')
		;
	img->data = (PPMPixel*) malloc(img->x * img->y * sizeof(PPMPixel));

	if (!img) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}

	if (fread(img->data, 3 * img->x, img->y, fp) != img->y) {
		fprintf(stderr, "Error loading image '%s'\n", filename);
		exit(1);
	}

	fclose(fp);
	return img;
}

__global__ void Hist(PPMPixel *img_data_d, float *h_d,  int cols_d, int rows_d)
{
	__shared__ float histo_private[64]; //Privatization - slide 36 do Cuda - Histogram.pdf

	int linha = blockIdx.y * blockDim.y + threadIdx.y; //linha da thread no vetor
	int coluna = blockIdx.x * blockDim.x + threadIdx.x; //coluna da thread no vetor

	int posTread = linha * cols_d + coluna; //posicao da thread no vetor
	int n = (rows_d * cols_d); //qntd de elementos
  int id = threadIdx.x * blockDim.x + threadIdx.y; //id da thread no bloco

	if(id < 64)
		histo_private[id] = 0; //Initialize the bin counters in the private copies of h - slide 37
	__syncthreads();//slide 37

	if(posTread < n && img_data_d[posTread].red <= 3 && img_data_d[posTread].green <= 3 && img_data_d[posTread].blue <= 3){
	  int index = img_data_d[posTread].red * 16 + img_data_d[posTread].green * 4 + img_data_d[posTread].blue;
	  atomicAdd(&histo_private[index], (1.0 / n)); //slide 38
	}
	__syncthreads();

	if(id < 64)
		atomicAdd(&h_d[id], histo_private[id]); //slide 39
}

void Histogram(PPMImage *image, float *h) {

	int i;
	int rows, cols;
	double t_start, t_end;
	float n = image->y * image->x;

	//Timing Kernel Execution
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float milliseconds = 0;

	cols = image->x;
	rows = image->y;

	//Variaveis para usar na GPU
	PPMPixel *img_data_d;
	float *h_d;
	//int *cols_d, *rows_d;

	//Declarando variáveis na memória da GPU
	//tempo gpu criar buffer: tempo de alocação de memória para a gpu
	cudaEventRecord(start);
	cudaMalloc((void**)&img_data_d, n * sizeof(PPMPixel));
	cudaMalloc((void**)&h_d, 64 * sizeof(float));
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&milliseconds, start, stop);
	fprintf(stdout, "Tempo_gpu_criar_buffer: %0.6lfms\n", milliseconds);
	//cudaMalloc((void**)&cols_d, sizeof(int));
	//cudaMalloc((void**)&rows_d, sizeof(int));

	//printf("%d, %d\n", rows, cols );

	for (i = 0; i < n; i++) {
		image->data[i].red = floor((image->data[i].red * 4) / 256);
		image->data[i].blue = floor((image->data[i].blue * 4) / 256);
		image->data[i].green = floor((image->data[i].green * 4) / 256);
	}

/****************************ALTERAÇÕES PARA A GPU*************************/


	//tempo gpu offload enviar: tempo da cópia dos valores da cpu para gpu
	cudaEventRecord(start);
	cudaMemcpy(img_data_d, image->data, n * sizeof(PPMPixel), cudaMemcpyHostToDevice);
	cudaMemcpy(h_d, h, 64 * sizeof(float), cudaMemcpyHostToDevice);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&milliseconds, start, stop);
	//printf("\n");
	fprintf(stdout, "tempo_gpu_offload_enviar: %0.6lfms\n", milliseconds);
	//cudaMemcpy(cols_d, &cols, sizeof(int), cudaMemcpyHostToDevice);
	//cudaMemcpy(rows_d, &rows, sizeof(int), cudaMemcpyHostToDevice);

	dim3 GRID(TILE, TILE);
  dim3 BLOCKS(ceil((float)cols / TILE), ceil((float)rows / TILE), 1);


	cudaEventRecord(start);//Timing Kernel Execution
	//Chama função no KERNEL
	Hist<<<BLOCKS, GRID>>>(img_data_d, h_d, cols, rows);
	cudaEventRecord(stop);//Timing Kernel Execution
	//Timing Kernel Execution
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&milliseconds, start, stop);
	printf("\n");
	fprintf(stdout, "Tempo Kernel: %0.6lfms\n\n", milliseconds);

	// tempo gpu offload receber: tempo de cópia do valor da gpu para cpu
	cudaEventRecord(start);
	cudaMemcpy(h, h_d, 64 * sizeof(float), cudaMemcpyDeviceToHost);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&milliseconds, start, stop);
	//printf("\n");
	fprintf(stdout, "tempo_gpu_offload_receber: %0.6lfms\n", milliseconds);


	/*count = 0;
	x = 0;
	for (j = 0; j <= 3; j++) {
		for (k = 0; k <= 3; k++) {
			for (l = 0; l <= 3; l++) {
				for (i = 0; i < n; i++) {
					if (image->data[i].red == j && image->data[i].green == k && image->data[i].blue == l) {
						count++;
					}
				}
				h[x] = count / n; //Histograma normalizado
				count = 0;
				x++;
			}
		}
	}*/

	cudaFree(img_data_d);
	//cudaFree(h_d);
	//cudaFree(cols_d);
	//cudaFree(rows_d);
}

int main(int argc, char *argv[]) {

	if( argc != 2 ) {
		printf("Too many or no one arguments supplied.\n");
	}

	//double t_start, t_end;
	int i;
	char *filename = argv[1]; //Recebendo o arquivo!;

	//scanf("%s", filename);
	PPMImage *image = readPPM(filename);

	float *h = (float*)malloc(sizeof(float) * 64);

	//Inicializar h
	for(i=0; i < 64; i++) h[i] = 0.0;

	//t_start = rtclock();
	Histogram(image, h);
	//t_end = rtclock();

	for (i = 0; i < 64; i++){
		printf("%0.3f ", h[i]);
	}
	printf("\n");
	//fprintf(stdout, "\n%0.6lfs\n", t_end - t_start);
	free(h);
}
