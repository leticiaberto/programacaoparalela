#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>

int nt; //numero de threads no programa
char filename[100]; //não muda para cada thread, então pode ser global
int achouSenha = 0; //indica se alguma thread ja achou a senha (achouSenha = 1) ou ainda não (achouSenha = 0)

FILE *popen(const char *command, const char *type);

void* quebraSenha(void* rank){
  long my_rank = (long) rank; //indice da thread
	int qntdBin = 500000 / nt; //qntd de iterações que cada thread deve fazer
	int firstBin = my_rank * qntdBin; //primeiro indice que a thread usa no laço
	int lastBin = firstBin + qntdBin; //ultimo indice que a thread usa no laço
  int i;

  //essas variaveis precisam ser locais de cada thread, pois cada uma usa um comando para abrir o arquivo
  FILE * fp; //se fosse global todas as threads usariam o mesmo arquivo aberto, ocasionando erros de leitura. ALém disso, um arquivo poderia tentar abrir qnd já estiver aberto ou fechar durante a leitura de outra thread
  char finalcmd[300] = "unzip -P%d -t %s 2>&1";
  char cmd[400];
  char ret[200];

  //intervalo de iterações percorridas por cada thread
  for(i = firstBin; i < lastBin; i++) {
    //printf("thread =  %ld i = %d\n",my_rank, i );
    /*se alguma thread achou a senha para de fazer iterações. Esta nessa parte do código para evitar que
    uma thread abra arquivo sendo que outra já achou a senha e que para a iteração seguinte da thread que achou
    a senha pare ela (poderia ser colocado i = lastBin qnd encontrada a senha, mas não atenderia o primeiro caso) */
    if (achouSenha == 1)
      break;

    sprintf((char*)&cmd, finalcmd, i, filename);
  	//printf("Comando a ser executado: %s \n", cmd);

   fp = popen(cmd, "r");
  	while (!feof(fp)) {
      if(achouSenha == 1)//se alguma thread já achou a senha não precisa terminar de ler o arquivo, já pode parar
        break;
      fgets((char*)&ret, 200, fp);
  		if (strcasestr(ret, "ok") != NULL) {
  			printf("Senha:%d\n", i);
  			achouSenha = 1;//para a thread que achou a senha e altera a variavel global para que as demais threads possam parar
        break;
  		}
    }
		pclose(fp);
	}
	return NULL;
}

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return (Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

int main ()
{
	double t_start, t_end;

  //para usar threads
	long thread;
	pthread_t* thread_handles;
  thread_handles = malloc(nt*sizeof(pthread_t));

  scanf("%d", &nt);
	scanf("%s", filename);

	t_start = rtclock();

  //lança threads para quebrar senha
  for(thread = 0; thread < nt; thread++)
		pthread_create(&thread_handles[thread], NULL, quebraSenha, (void*) thread);

  //espera as threads acabarem e da um join
	for(thread = 0; thread < nt; thread++)
		pthread_join(thread_handles[thread], NULL);

	t_end = rtclock();

	fprintf(stdout, "%0.6lf\n", t_end - t_start);

  free (thread_handles);
}
