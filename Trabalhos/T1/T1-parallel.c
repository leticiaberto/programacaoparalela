#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

/* count sort parallel */
double count_sort_parallel(double a[], int n, int nt) {
	int i, j, count;
	double *temp;
	double start, end, duracao;

	//aloca o vetor temporario
	temp = (double *)malloc(n*sizeof(double));

	start = omp_get_wtime();

	//i, j, count são privadas para nao contabilizar valores errados nas iterações das outras threads
	//ou seja, toda thread tem que ter seus valores especificos para essas variaveis para que percorra o vetor corretamente
	#pragma omp parallel for num_threads(nt) private(i, j, count)
	//mesmo código fornecido pelo professor
		for (i = 0; i < n; i++) {
			count = 0;
			for (j = 0; j < n; j++)
				if (a[j] < a[i])
					count++;
				else if (a[j] == a[i] && j < i)
					count++;
			temp[count] = a[i];
		}
	end = omp_get_wtime();

	duracao = end - start;

	/*Esse laço apenas copia os valores do temporário para o vetor original. Como é apenas uma cópia podemos paralelizar,
	pois cada posição será escrita APENAS UMA VEZ (vetor ordenado). Nesse caso, precisamos proteger somente a variavel
	i para que não ocorra "bagunça" dos indices percorridos ao longo da execução de todas as threads

	Paralelizar a copia do vetor em um for é mais rapido que usando a função memcpy()
	parallel for: 0.077717
	memcpy(): 0.078243

	--Estudo de parallel for retirado do livro da bibliografia (Pacheco) - pag 224*/
	#pragma omp parallel for num_threads(nt) private(i)
		for (i = 0; i < n; i++)
			a[i] = temp[i];

	//memcpy(a, temp, n*sizeof(double));
	free(temp);

	return duracao;
}

int main(int argc, char * argv[]) {
	int i, n, nt;
	double  * a, t_s;

	scanf("%d",&nt);

	/* numero de valores */
	scanf("%d",&n);

	/* aloca os vetores de valores para o teste em serial(b) e para o teste em paralelo(a) */
	a = (double *)malloc(n*sizeof(double));

	/* entrada dos valores */
	for(i=0;i<n;i++)
		scanf("%lf",&a[i]);

	/* chama as funcoes de count sort em paralelo e em serial */
	t_s = count_sort_parallel(a,n, nt);

	/* Imprime o vetor ordenado */
	for(i=0;i<n;i++)
		printf("%.2lf ",a[i]);

	printf("\n");

	/* imprime os tempos obtidos e o speedup */
	printf("%lf\n",t_s);

	return 0;
}
