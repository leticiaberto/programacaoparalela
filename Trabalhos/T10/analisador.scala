import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf


object Analisador {

  // Args = path/to/text0.txt path/to/text1.txt
  def main(args: Array[String]) {

		val start = System.currentTimeMillis

    // create Spark context with Spark configuration
    val sc = new SparkContext(new SparkConf().setAppName("Contagem de Palavra"))

    println("TEXT1")

    // read first text file and split into lines
    val lines1 = sc.textFile(args(0))

    // TODO: contar palavras do texto 1 e imprimir as 5 palavras com as maiores ocorrencias (ordem DECRESCENTE)
    // imprimir na cada linha: "palavra=numero"

    val words = lines1.flatMap(line => line.split(" "))

		val intermData = words.map(word => (word.replaceAll("[,.!?:;]","").toLowerCase,1))

		val wordCount = intermData.reduceByKey(_+_)

		//Palavras com mais de 3 letras
		val Mais3letrasT1 = wordCount.filter(_._1.length > 3)
		
		//Oredena decrescente e pega os 5 maiores
		val contagensText1 = Mais3letrasT1.takeOrdered(5)(Ordering[Int].reverse.on(_._2))
		
		contagensText1.foreach(x => printf("%s=%d\n", x._1, x._2))

    println("TEXT2")

    // read second text file and split each document into words
    val lines2 = sc.textFile(args(1))

    // TODO: contar palavras do texto 2 e imprimir as 5 palavras com as maiores ocorrencias (ordem DECRESCENTE)
    // imprimir na cada linha: "palavra=numero"

    val words2 = lines2.flatMap(line => line.split(" "))

		val intermData2 = words2.map(word => (word.replaceAll("[,.!?:;]","").toLowerCase,1))

		val wordCount2 = intermData2.reduceByKey(_+_)

		//Palavras com mais de 3 letras
		val Mais3letrasT2 = wordCount2.filter(_._1.length > 3)
		
		//Oredena decrescente e pega os 5 maiores
		val contagensText2 = Mais3letrasT2.takeOrdered(5)(Ordering[Int].reverse.on(_._2))
		
		contagensText2.foreach(x => printf("%s=%d\n", x._1, x._2))

    println("COMMON")

    // TODO: comparar resultado e imprimir na ordem ALFABETICA todas as palavras que aparecem MAIS que 100 vezes nos 2 textos
    // imprimir na cada linha: "palavra"
		val Mais100Text1 = Mais3letrasT1.filter(_._2 > 100)
		val Mais100Text2 = Mais3letrasT2.filter(_._2 > 100)
		
		val result = Mais100Text1.join(Mais100Text2)

		result.sortByKey(true).collect.foreach{ x => println(x._1.toString)}

		val end = System.currentTimeMillis - start
		printf("Tempo: %d ms", end)
		//result.foreach(x => printf("\n%s", x._1))
  }

}
