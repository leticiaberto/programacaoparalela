#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TILE 16

__global__ void AddVet(int *d_a, int *d_b, int *d_c, int linhas, int colunas)
{
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = blockIdx.x * blockDim.x + threadIdx.x;

    int ind = i * colunas + j;

    if (i < linhas && j < colunas)
      d_c[ind] = d_a[ind] + d_b[ind];
}

int main()
{
    int *A, *B, *C;
    int *d_a, *d_b, *d_c;//para alocar no device
    int i, j;

    //Input
    int linhas, colunas;

    scanf("%d", &linhas);
    scanf("%d", &colunas);

    //Alocando memória na CPU
    A = (int *)malloc(sizeof(int)*linhas*colunas);
    B = (int *)malloc(sizeof(int)*linhas*colunas);
    C = (int *)malloc(sizeof(int)*linhas*colunas);

    //Declarando variáveis na memória da GPU
    cudaMalloc((void **)&d_a, linhas*colunas*sizeof(int));
    cudaMalloc((void **)&d_b, linhas*colunas*sizeof(int));
    cudaMalloc((void **)&d_c, linhas*colunas*sizeof(int));

    //Inicializar
    for(i = 0; i < linhas; i++){
        for(j = 0; j < colunas; j++){
            A[i*colunas+j] =  B[i*colunas+j] = i+j;
        }
    }

    cudaMemcpy(d_a, A, linhas*colunas*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, B, linhas*colunas*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_c, C, linhas*colunas*sizeof(int), cudaMemcpyHostToDevice);

    dim3 GRID(TILE, TILE);
    dim3 BLOCKS(ceil((float)colunas / TILE), ceil((float)linhas / TILE), 1);

    //Chama função no KERNEL
    AddVet<<<BLOCKS, GRID>>>(d_a, d_b, d_c, linhas, colunas);

    //cudaDeviceSynchronize();

    cudaMemcpy(C, d_c, linhas*colunas*sizeof(int), cudaMemcpyDeviceToHost);//copia a matriz resultante C da GPU para CPU

    //Computacao que deverá ser movida para a GPU (que no momento é executada na CPU)
    //Lembrar que é necessário usar mapeamento 2D (visto em aula)
    /*for(i=0; i < linhas; i++){
        for(j = 0; j < colunas; j++){
            C[i*colunas+j] = A[i*colunas+j] + B[i*colunas+j];
        }
    }*/

    long long int somador=0;
    //Manter esta computação na CPU
    for(i = 0; i < linhas; i++){
        for(j = 0; j < colunas; j++){
            somador+=C[i*colunas+j];
        }
    }

    printf("%lli\n", somador);

    free(A);
    free(B);
    free(C);

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
}
