import random
import sys

def multiplicaMatriz(matrixA, matrixB):
    sizeLin = len(matrixA)
    sizeCol = len(matrixB[0])
    matrizResult = []
    for i in range(sizeLin):
        linha = []
        for j in range(sizeCol):
            linha.append(0)
        matrizResult.append(linha)

    for i in range(sizeLin):
        for j in range(sizeCol):
            val = 0
            for k in range(len(matrixB[0])):
                val = val + matrixA[i][k] * matrixB[k][j]
                matrizResult[i][j] = val
    return matrizResult

def transposeMatrix(M):
    """Calcula a transposta de uma matriz."""
    aux=[]
    for j in range(len(M[0])):
        linha=[]
        for i in range(len(M)):
            linha.append(M[i][j])
        aux.append(linha)
    return aux

def imprimeMatriz(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            print '',round(matrix[i][j],2)
        print

def cria_matriz(lin,col):
    A=[]
    for i in range(lin):
        linha=[]
        for j in range(col):
            linha = linha + [random.uniform(1.0, 6.9)]
        A = A + [linha]
    return A

def main():
    #matrixA = cria_matriz(100, 100)
    matrixB = cria_matriz(2000, 2000)
    #print 'Matriz A:'
    #imprimeMatriz(matrixA)
    #print 'Transposta de A:'
    #trans = transposeMatrix(matrixA)
    #imprimeMatriz(trans)

    print 'Matriz B:'
    #imprimeMatriz(matrixB)
    #print 'Transposta de b:'
    #transB = transposeMatrix(matrixB)
    #imprimeMatriz(transB)
    output = sys.argv[1]
    outputFile = open(output, 'w')
    for i in range(len(matrixB)):
        for j in range(len(matrixB[0])):
            outputFile.write(' '+ str(round(matrixB[i][j],2)))
        outputFile.write('\n')

    outputFile.close()
    #matrixResult = multiplicaMatriz(matrixA, matrixB)
    #print 'Matriz resultado:'
    #imprimeMatriz(matrixResult)


if __name__ == '__main__':
    main()
