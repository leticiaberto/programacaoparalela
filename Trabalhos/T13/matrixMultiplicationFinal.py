import numpy as np
import sys
from pyspark.mllib.linalg.distributed import *
from pyspark.mllib.linalg.distributed import RowMatrix
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark import sql
import time
SLICES = 2000
COLS = 1024
ROWS = 1024

#método de conversão de RDD para a estrutura blockMatrix
def matrixStructure(rdd, rowsPerBlock=ROWS, colsPerBlock=COLS):
    return IndexedRowMatrix(rdd.zipWithIndex().map(lambda x: IndexedRow(x[1], x[0]))).toBlockMatrix(rowsPerBlock, colsPerBlock)


#configuração do spark 
conf = SparkConf().setAppName('Matrix Multiplication')
conf = (conf.setMaster('local[*]').set('spark.executor.memory', '4G').set('spark.driver.memory', '45G'))
sc = SparkContext(conf=conf)
sqlContext = sql.SQLContext(sc)

#leitura do tamanho da matriz
size = int(sys.argv[1])

#gera matrizes float64 quadradas de tamanho size
A = np.arange(size ** 2, dtype=np.float64).reshape(size, size)
B = np.arange(size ** 2, dtype=np.float64).reshape(size, size)

#tempo inicial
start_time = time.time()

#conversão dos RDDs correspondentes às matrizes A e B para a estrutura de matriz a ser trabalhada
matrixA = matrixStructure(sc.parallelize(A,numSlices = SLICES))
matrixB = matrixStructure(sc.parallelize(B,numSlices = SLICES))

#multiplicação distribuida
product = matrixA.multiply(matrixB)
 
#converte RDD para matriz local
finalMatrix = product.toLocalMatrix()

#tempo de processamento
print(" %s s" % (time.time() - start_time))

#impressao matriz final C
print(finalMatrix)
