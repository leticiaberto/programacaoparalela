name := "matrixMultiply"
version := "0.1"

scalaVersion := "2.11.11"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.1"

libraryDependencies += "org.apache.spark" % "spark-mllib_2.11" % "2.2.0"
