import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix
import org.apache.spark.mllib.linalg.distributed.MatrixEntry
import util.Random

object MatrixMultiply {

  def coordinateMatrixMultiply(leftMatrix: CoordinateMatrix, rightMatrix: CoordinateMatrix): CoordinateMatrix = {

    val M_ = leftMatrix.entries.map({ case MatrixEntry(i, j, v) => (j, (i, v)) })
    val N_ = rightMatrix.entries.map({ case MatrixEntry(j, k, w) => (j, (k, w)) })

    val productEntries = M_
      .join(N_)
      .map({ case (_, ((i, v), (k, w))) => ((i, k), (v * w)) })
      .reduceByKey(_ + _)
      .map({ case ((i, k), sum) => MatrixEntry(i, k, sum) })
    println("Take: "+productEntries.take(2))
    new CoordinateMatrix(productEntries)
    }


    def main(args: Array[String]){

      val start = System.currentTimeMillis

      val sc = new SparkContext(new SparkConf().setAppName("Matrix Multiplication").setMaster("local[*]"))
      val lin = args(0).toInt
      val col = args(1).toInt

      val numRecords = lin * col
      val partitions =  64
      val recordsPerPartition = numRecords / partitions // we are assuming here that numRecords is divisible by partitions, otherwise we need to compensate for the residual

      val seedRdd = sc.parallelize(Seq.fill(partitions)(recordsPerPartition),partitions)
      val randomNrs = seedRdd.flatMap(records => Seq.fill(records)(Random.nextFloat)).zipWithIndex()
                              .map({case (valor, index) => MatrixEntry(index/lin, index%col, valor)})
      val A = new CoordinateMatrix(randomNrs)
      coordinateMatrixMultiply(A,A)
      sc.stop()
      val end = System.currentTimeMillis - start
      printf("Tempo: %d ms", end)
      //{4,3,1,7} -> {(4,0), (3,1), (1,2), (7,3)} -> ((0,0),4), ((0,1),3), ((1,0),1), ((1,1),7)
    }
}
//https://www.balabit.com/blog/scalable-sparse-matrix-multiplication-in-apache-spark
//http://infolab.stanford.edu/~ullman/mmds/book.pdf - pagina 38
