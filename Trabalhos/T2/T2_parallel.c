#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

void producer_consumer(int *buffer, int size, int *vec, int n, int nt) {
	int i, j;
	long long unsigned int k = 0, sum = 0;

	#pragma omp parallel num_threads(nt) \
			default (none) reduction(+:sum) shared(vec, buffer, size, n) private (i, j)
	for(i=0;i<n;i++) {
		if(i % 2 == 0) {	// PRODUTOR
			#pragma omp for
			for(j=0;j<size;j++) {
				buffer[j] = vec[i] + j*vec[i+1];
			}
		}
		else {	// CONSUMIDOR
			#pragma omp for
			for(j=0;j<size;j++) {
				sum += buffer[j];
			}
		}
	}
	printf("%llu\n",sum);
}

int main(int argc, char * argv[]) {
	double start, end;
	//float d;
	int i, n, size, nt;
	int *buff;
	int *vec;

	scanf("%d %d %d",&nt,&n,&size);

	buff = (int *)malloc(size*sizeof(int));
	vec = (int *)malloc(n*sizeof(int));

	for(i=0;i<n;i++)
		scanf("%d",&vec[i]);

	start = omp_get_wtime();
	producer_consumer(buff, size, vec, n, nt);
	end = omp_get_wtime();

	printf("%lf\n",end-start);

	free(buff);
	free(vec);

	return 0;
}

/*

----------------lscpu------------------------------

Arquitetura:           x86_64
Modo(s) operacional da CPU:32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Thread(s) per núcleo  2
Núcleo(s) por soquete:2
Soquete(s):            1
Nó(s) de NUMA:        1
ID de fornecedor:      GenuineIntel
Família da CPU:       6
Modelo:                142
Model name:            Intel(R) Core(TM) i7-7500U CPU @ 2.70GHz
Step:                  9
CPU MHz:               2900.000
CPU max MHz:           3500,0000
CPU min MHz:           400,0000
BogoMIPS:              5808.00
Virtualização:       VT-x
cache de L1d:          32K
cache de L1i:          32K
cache de L2:           256K
cache de L3:           4096K
NUMA node0 CPU(s):     0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti retpoline intel_pt rsb_ctxsw tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp

--------------------------gprof-----------------------------
* arq1.in
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  ms/call  ms/call  name
100.75      0.01     0.01        1    10.08    10.08  producer_consumer

* arq2.in
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  ms/call  ms/call  name
100.75      0.25     0.25        1   251.88   251.88  producer_consumer

* arq3.in
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls   s/call   s/call  name
100.75      2.24     2.24        1     2.24     2.24  producer_consumer

-----------------------------Execução de flags e speedup-----------------
* arq1.in
sem flag: 0.013982
com flag -O0: 0.013334
com flag -O1: 0.002990
com flag -O2: 0.003657
com flag -O3: 0.000623
speedup flag -O0: 1,04859757
speedup flag -O1: 4,676254181
speedup flag -O2: 3,823352475
speedup flag -O3: 22,443017657


* arq2.in
sem flag: 0.243436
com flag -O0: 0.239099
com flag -O1: 0.071395
com flag -O2: 0.076041
com flag -O3: 0.107431
speedup flag -O0: 1,01813893
speedup flag -O1: 3,409706562
speedup flag -O2: 3,201378204
speedup flag -O3: 2,26597537

* arq3.in
sem flag: 2.209270
com flag -O0: 2.219818
com flag -O1: 0.468700
com flag -O2: 0.473172
com flag -O3: 0.497174
speedup flag -O0: 0,995248259
speedup flag -O1: 4,713612119
speedup flag -O2: 4,669063258
speedup flag -O3: 4,443655541
*/
